//
//  HotCell.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 8/11/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

// extends UITableViewCell
@interface HotCell : UITableViewCell

// now only showing one label, you can add more yourself
@property (nonatomic, strong) UILabel *descriptionLabel;

@end
