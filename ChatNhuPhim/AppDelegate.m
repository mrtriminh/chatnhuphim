//
//  AppDelegate.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/2/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "ViewController.h"
#import "UIApplication+Utils.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import "NSUserDefaults+Utils.h"
#import "NSString+Utils.h"
#import <StartApp/StartApp.h>
#import "Appirater.h"
#import "CNP_MyLanguage.h"


@interface AppDelegate ()<FBSDKMessengerURLHandlerDelegate>

@end

@implementation AppDelegate

FBSDKMessengerURLHandler *_messengerUrlHandler;


@synthesize window = _window;
@synthesize viewController = _viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[CNP_MyLanguage shared] setLanguage:@"vi"];
    
    //clear cache at Cached Directory
    [UIApplication clearCacheVideo];
    //clear cache at Document Directory
    [UIApplication clearCacheDocument];
    [UIApplication removeAllStoredCredentials];

    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    // Override point for customization after application launch.
    
    // Check for update
        NSString *appInfoUrl = @"http://itunes.apple.com/lookup?bundleId=com.chatnhuphim.ChatNhuPhim";
    
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:appInfoUrl]];
        [request setHTTPMethod:@"GET"];
    
        NSURLResponse *response;
        NSError *error;
        NSData *data = [NSURLConnection  sendSynchronousRequest:request returningResponse: &response error: &error];
        NSString *output = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSASCIIStringEncoding]; //[NSString stringWithCString:[data bytes] length:[data length]];
    
        NSError *e = nil;
        NSData *jsonData = [output dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
    
        NSString *lastVersion = [[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    if (lastVersion){
    
    if (![lastVersion isEqualToString:version]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:myLanguage(@"kUpdateTitle")
                                                        message:myLanguage(@"kUpdateMessage")
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Update now", nil];
        [alert show];
    }
    }
    [Appirater setAppId:@"1005212128"];
    [Appirater setCustomAlertTitle:@"Love Chat = Phim?"];
//    [Appirater setCustomAlertMessage:@"Hy vọng bạn thấy vui khi sử dụng app. Ghé App Store đánh giá 5 sao nhen"];
//    [Appirater setCustomAlertRateButtonTitle:@"OK! Qua luôn."];
//    [Appirater setCustomAlertRateLaterButtonTitle:@"Thôi để khi khác."];
//    [Appirater setCustomAlertCancelButtonTitle:@"Khó quá bỏ đi :("];
    [Appirater setDaysUntilPrompt:5];
    [Appirater setUsesUntilPrompt:20];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    
    // Initialize Reachability
    Reachability *reachability = [Reachability reachabilityWithHostName:@"http://www.google.com"];
    [reachability startNotifier];
    
    //startapp integration
    STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
    sdk.appID = @"210525733";
    [sdk disableReturnAd];
    //[sdk showSplashAd];//show splash ad
    
    //startapp optimised targeting
    int lowerBound = 17;
    int upperBound = 25;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);//don't know user's age YET, haha
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *gender = [userDefault valueForKey:kDictUserGender];
    if ([gender isEqualToString:@"male"]) {
    sdk.preferences = [STASDKPreferences prefrencesWithAge:rndValue andGender:STAGender_Male];
    } else if ([gender isEqualToString:@"female"]) {
        sdk.preferences = [STASDKPreferences prefrencesWithAge:rndValue andGender:STAGender_Female];
    }
    myLog(@"%@ in %d",gender,rndValue);
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    _messengerUrlHandler = [[FBSDKMessengerURLHandler alloc] init];
    _messengerUrlHandler.delegate = self;
    
    return YES;
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //Cancel local push notification
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  //  [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Check if the handler knows what to do with this url
    if ([_messengerUrlHandler canOpenURL:url sourceApplication:sourceApplication]) {
        // Handle the url
        [_messengerUrlHandler openURL:url sourceApplication:sourceApplication];
    }
    // This makes sure Facebook login return not cancelled
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                @"itms-apps://chat-phim/id1005212128?mt=8"]];
}

@end
