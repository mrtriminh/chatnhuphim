//
//  CheckInternetManager.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/5/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import "CheckInternetManager.h"

#import "Reachability.h"

@implementation CheckInternetManager

#pragma mark -
#pragma mark Default Manager
+ (CheckInternetManager *)sharedManager {
    static CheckInternetManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

#pragma mark -
#pragma mark Memory Management
- (void)dealloc {
    // Stop Notifier
    if (_reachability) {
        [_reachability stopNotifier];
    }
}

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable {
    return [[[CheckInternetManager sharedManager] reachability] isReachable];
}

+ (BOOL)isUnreachable {
    return ![[[CheckInternetManager sharedManager] reachability] isReachable];
}

+ (BOOL)isReachableViaWWAN {
    return [[[CheckInternetManager sharedManager] reachability] isReachableViaWWAN];
}

+ (BOOL)isReachableViaWiFi {
    return [[[CheckInternetManager sharedManager] reachability] isReachableViaWiFi];
}

#pragma mark -
#pragma mark Private Initialization
- (id)init {
    self = [super init];
    
    if (self) {
        // Initialize Reachability
        self.reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
        
        // Start Monitoring
        [self.reachability startNotifier];
    }
    
    return self;
}

@end