//
//  AppDelegate.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/2/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@class ViewController;
@protocol ChatNhuPhimDelegate <NSObject>
@required
-(void)makeClipsWithData:(id)data;
@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, weak) id<ChatNhuPhimDelegate> delegate;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;

@property (assign, nonatomic) HomeViewController *homeController;

@end

