//
//  SettingViewController.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/8/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import "SettingViewController.h"
#import "ViewController.h"

@interface SettingViewController ()

@property (weak, nonatomic) IBOutlet UIButton *upload;
@property (weak, nonatomic) IBOutlet UIButton *clear;
@property (weak, nonatomic) IBOutlet UIButton *term;
@property (weak, nonatomic) IBOutlet UIButton *about;
@property (weak, nonatomic) IBOutlet UIView *web;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *Close;
@property (weak, nonatomic) IBOutlet UIWebView *website;
@property (weak, nonatomic) IBOutlet UISegmentedControl *voice;
@property (weak, nonatomic) IBOutlet UIButton *feedback;

@property (weak, nonatomic) IBOutlet UITextView *feedText;
@property (weak, nonatomic) IBOutlet UIButton *feedSend;
@property (weak, nonatomic) IBOutlet UIButton *feedCancel;
@property (weak, nonatomic) IBOutlet UIView *feedContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedBottom;


@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.web.layer.cornerRadius = 8;
    self.web.layer.masksToBounds = YES;
    
    //self.feedBottom.constant = -(self.feedContent.frame.size.height);
    
    [_voice addTarget:self
               action:@selector(selectVoice)
     forControlEvents:UIControlEventValueChanged];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    selectedVoice = [defaults objectForKey:@"selectedVoice"];
    
    if ([selectedVoice isEqualToString:@"female"]) {
        [_voice setSelectedSegmentIndex:0];
    } else if ([selectedVoice isEqualToString:@"male"]) {
         [_voice setSelectedSegmentIndex:1];
    } else {
        [_voice setSelectedSegmentIndex:2];
    }
    myLog(@"%@",selectedVoice);
    //self.web.hidden = YES;
    // Do any additional setup after loading the view.
    //[self.clear setTitle:@"Làm sạch dữ liệu" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender {
    [self close];
}

-(void)close {
    _web.alpha = 1.0;
    [UIView animateWithDuration:1 animations:^() {
        _web.alpha = 0.0;
    }];
    self.webTopConstraint.constant = 750;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    myLog(@"tap close");
}

-(IBAction)up:(id)sender {
    [self upclip];
}

-(void)upclip {
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/852245118200611"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com"]];
    }
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.chatnhuphim.com/upload.php"]];
    /*[_website loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    NSString *urlString = @"http://www.chatnhuphim.com/upload.php";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_website loadRequest:urlRequest];
    
    _web.alpha = 0.0;
    [UIView animateWithDuration:1 animations:^() {
        _web.alpha = 1.0;
    }];
    self.webTopConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];*/
}

//clear cache
-(IBAction)clr:(id)sender {
    [self clearCache];
}

-(void)clearCache {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.clear setTitle:@"Dữ liệu sạch rồi!" forState:UIControlStateNormal];
}

-(IBAction)term:(id)sender {
    [self showTerm];
}

-(void)showTerm {
    [_website loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    NSString *urlString = @"http://www.chatnhuphim.com/term.php";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_website loadRequest:urlRequest];
    
    _web.alpha = 0.0;
    [UIView animateWithDuration:1 animations:^() {
        _web.alpha = 1.0;
    }];
    self.webTopConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}


-(IBAction)about:(id)sender
{
    [self showabout];
}

-(void)showabout {
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/852245118200611"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com"]];
    }
    /*[_website loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    NSString *urlString = @"http://www.chatnhuphim.com/about.php";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_website loadRequest:urlRequest];
    
    _web.alpha = 0.0;
    [UIView animateWithDuration:1 animations:^() {
        _web.alpha = 1.0;
    }];
    self.webTopConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];*/
}

-(IBAction)feed:(id)sender
{
    if ([_feedText.text length] != 0) {
        [_feedback setTitle:@"GÓP Ý                                        " forState:UIControlStateNormal];
    }
    self.feedBottom.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    [_feedText setText:@""];
    [_feedText becomeFirstResponder];
}

-(IBAction)feedCancel:(id)sender
{
    self.feedBottom.constant = -(self.feedContent.frame.size.height);
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    [_feedText setText:@""];
    [_feedText resignFirstResponder];
}

-(IBAction)feedSend:(id)sender
{
    NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%@",_feedText.text];
    NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    myLog(@"%@",url);
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
    //NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
   // [_website loadRequest:urlRequest];
    [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
    [serviceRequest setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
    NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
    myLog(@"requestReply: %@", requestReply);
    self.feedBottom.constant = -(self.feedContent.frame.size.height);
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    if ([_feedText.text length] != 0) {
    [_feedback setTitle:@"Cám ơn tình yêu" forState:UIControlStateNormal];
    }
     [_feedText resignFirstResponder];
   // [_website loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
}
    
//Pass voice variable
/*-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController * mainViewController = ( ViewController * )segue.destinationViewController;
    if ([_voice selectedSegmentIndex] == 0) {
        mainViewController.voice = @"female";
    } else if ([_voice selectedSegmentIndex] == 1) {
        mainViewController.voice = @"male";
    } else {
        mainViewController.voice = @"";
    }
    //  mainViewController.voice = ["%@",[_voice titleForSegmentAtIndex:_voice.selectedSegmentIndex]];
}*/

-(void)selectVoice{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([_voice selectedSegmentIndex] == 0) {
        selectedVoice = [NSString stringWithFormat:@"female"];
    } else if ([_voice selectedSegmentIndex] == 1) {
        selectedVoice = [NSString stringWithFormat:@"male"];
    } else {
        selectedVoice = [NSString stringWithFormat:@""];
    }
    [defaults setObject:selectedVoice forKey:@"selectedVoice"];
    [defaults synchronize];
   // myLog(@"%@",selectedVoice);
}

// dismiss view
- (IBAction)change:(id)sender
{
    self.parentViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
