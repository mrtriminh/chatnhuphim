//
//  UIImageView+Animation.h
//  Agento Floortime
//
//  Created by Hoat Ha Van on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Animation)
//Create a image view animation with a given frame
+(id)imageViewAnimation:(CGRect)frame duration:(CGFloat)duration;

+(id)realtimeLoaddingimageViewAnimation:(CGRect)frame duration:(CGFloat)duration;

@end
