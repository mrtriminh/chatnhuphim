//
//  UIColor+Utils.m
//  Dan
//
//  Created by Minh Đạt Giáp on 8/28/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

//RGB color with alpha
+(UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha{
    return [UIColor
            colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0
            green:((float)((hexValue & 0xFF00) >> 8))/255.0
            blue:((float)(hexValue & 0xFF))/255.0 alpha:alpha];
}


//RGB color
+(UIColor *)colorWithHex:(int)hexValue{
    return [UIColor colorWithHex:hexValue alpha:1.0];
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
//Color with default red, green, blue and alpha 1.0f
+(UIColor *)colorDefaultWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue{
    return [UIColor colorDefaultWithRed:red green:blue blue:green alpha:1.0f];
}

+(UIColor *)colorDefaultWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha{
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
}
@end
