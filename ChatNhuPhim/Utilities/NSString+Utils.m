//
//  NSString+Utils.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSString+Utils.h"


@import UIKit;
@import MobileCoreServices;
@implementation NSString (Utils)
+(BOOL)isEmptyString:(NSString*)string{

    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if ([string isKindOfClass:[NSString class]]) {
        
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([string isEqualToString:@"<null>"] ||[string isEqualToString:@"(null)"] || [string isEqualToString:@""]) {
            return YES;
        }
        
        if (string.length > 0) {
            return NO;
        }
    }
    
    if (!string) {
        return YES;
    }
    
    return NO;
}
+(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)isMale:(NSString*)value{
    return [[value lowercaseString] isEqualToString:@"male"]?YES:NO;
}

+(NSString*)encodeToBase64String:(UIImage*)image{
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (NSString*) mimeTypeFromString: (NSString *) path {
    path = [path lowercaseString];
    if([path hasSuffix:@".pdf"]){
        return @"application/pdf";
    }else if([path hasSuffix:@".txt"]){
        return @"text/plain";
    }else if([path hasSuffix:@".doc"] || [path hasSuffix:@".docx"]){
        return @"application/msword";
    }else if([path hasSuffix:@".mov"]){
        return @"video/quicktime";
    }else if([path hasSuffix:@".mp4"]){
        return @"video/mp4";
    }
    return @"";
}

+ (NSString *) encodeFormPostParameters: (NSDictionary *) parameters {
    NSMutableString *formPostParams = [[NSMutableString alloc] init];
    
    NSEnumerator *keys = [parameters keyEnumerator];
    
    NSString *name = [keys nextObject];
    while (nil != name) {
        NSString *encodedValue = ((NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef) [parameters objectForKey: name], NULL, CFSTR("=/:"), kCFStringEncodingUTF8)));
        
        [formPostParams appendString: [NSString stringWithFormat:@"\'%@\'", name]];
        [formPostParams appendString: @":"];
        [formPostParams appendString: encodedValue];
        
        name = [keys nextObject];
        
        if (nil != name) {
            [formPostParams appendString: @","];
        }
    }
    
    return formPostParams;
}

//Update format number
+ (NSString*)updateFormatString:(NSString*)string{
    NSString *stringAmount = string;
    
    
    NSNumberFormatter *numberFormatter =[[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setAlwaysShowsDecimalSeparator:YES];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    //for example: $123,456,789.00
    NSString *stringFormat = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[stringAmount integerValue]]];
    stringFormat = [stringFormat stringByReplacingOccurrencesOfString:@"$" withString:@"$ "];
    stringFormat = [stringFormat stringByReplacingOccurrencesOfString:@".00" withString:@""];
    stringFormat = [stringFormat stringByReplacingOccurrencesOfString:@"₫" withString:@"$"];
    return stringFormat;
}

+(id)jsonFormatFromData:(id)data{
    NSError *jsonError;
    NSData *objectData;// = [[NSData alloc] initWithData:data];
    if ([data isKindOfClass:[NSString class]]) {
        objectData = [data dataUsingEncoding:NSUTF8StringEncoding];
    }
    else{
        objectData = [[NSData alloc] initWithData:data];
    }
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    if (!jsonError) {
        return json;
    }
    return nil;
}

+ (NSString *)stringWithGenderType:(GenderTypeIcon)genderType {
    switch (genderType) {
        case GenderTypeIconNone:
            return @"";
            break;
        case GenderTypeIconMale:
            return @"male";
            break;
        case GenderTypeIconFemale:
            return @"female";
            break;
        default:
            return @"both";
            break;
    }
}

+ (NSString *)getMacAddress
{
    int mgmtInfoBase[6];
    char *msgBuffer = NULL;
    NSString *errorFlag = NULL;
    size_t length;
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET; // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE; // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK; // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST; // Request all configured interfaces
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    // Get the size of the data available (store in len)
    else if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
        errorFlag = @"sysctl mgmtInfoBase failure";
    // Alloc memory based on above call
    else if ((msgBuffer = malloc(length)) == NULL)
        errorFlag = @"buffer allocation failure";
    // Get system information, store in buffer
    else if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0) {
        free(msgBuffer);
        errorFlag = @"sysctl msgBuffer failure";
    } else {
        // Map msgbuffer to interface message structure
        struct if_msghdr *interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
        // Map to link-level socket structure
        struct sockaddr_dl *socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
        // Copy link layer address data in socket structure to an array
        unsigned char macAddress[6];
        memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
        // Read from char array into a string object, into traditional Mac address format
        NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                      macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5]];
        NSLog(@"Mac Address: %@", macAddressString);
        // Release the buffer memory
        free(msgBuffer);
        return macAddressString;
    }
    // Error...
    NSLog(@"Error: %@", errorFlag);
    return nil;
}
@end
