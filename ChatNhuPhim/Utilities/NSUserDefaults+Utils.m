//
//  NSUserDefaults+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSUserDefaults+Utils.h"
#import "Constants.h"

@implementation NSUserDefaults (Utils)

+(void)saveDeviceToken:(NSString*)deviceToken{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:deviceToken];
    [userDefault setValue:deviceToken forKey:kDictUUID];
    [userDefault synchronize];
}

+(void)saveUserGender: (NSString*)userGender{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:userGender forKey:kDictUserGender];
    [userDefault synchronize];
}

+(NSString*)getDeviceToken{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    if ([userDefault valueForKey:kDictUUID]) {
//        
//    }
//   return @"";
//    NSData *data = [userDefault valueForKey:kDictUUID];
//    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return [userDefault valueForKey:kDictUUID];
}

@end
