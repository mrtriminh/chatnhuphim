//
//  NSString+Utils.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@class UIImage;

@interface NSString (Utils)

+ (BOOL) isEmptyString:(NSString*)string;

+ (BOOL) isValidEmail:(NSString *)checkString;

+ (BOOL) isMale:(NSString*)value;

+ (NSString*) encodeToBase64String:(UIImage*)image;

+ (NSString*) mimeTypeFromString: (NSString *) path;

+ (NSString *) encodeFormPostParameters: (NSDictionary *) parameters;

+ (NSString*) updateFormatString:(NSString*)string;

+ (id)jsonFormatFromData:(id)data;

+ (NSString*)stringWithGenderType:(GenderTypeIcon)genderType;

+ (NSString *)getMacAddress;

@end
