//
//  UIImageView+Animation.m
//  Agento Floortime
//
//  Created by Hoat Ha Van on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIImageView+Animation.h"

@implementation UIImageView (Animation)

+(id)imageViewAnimation:(CGRect)frame duration:(CGFloat)duration{
    NSArray *imageNames = @[@"loading_1.png", @"loading_2.png", @"loading_3.png", @"loading_4.png",
                            @"loading_5.png", @"loading_7.png", @"loading_8.png", @"loading_1.png"];
    
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:imageNames.count];
    for (NSInteger index = 0; index < imageNames.count; index++)
    {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:index]]];
    }
    
    // Normal Animation
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.animationImages = images;
    imageView.animationDuration = duration;
    return imageView;
}

+(id)realtimeLoaddingimageViewAnimation:(CGRect)frame duration:(CGFloat)duration{
    NSArray *imageNames = @[@"loading_circle_gray_01.png", @"loading_circle_gray_02.png", @"loading_circle_gray_03.png", @"loading_circle_gray_04.png",
                            @"loading_circle_gray_05.png", @"loading_circle_gray_06.png", @"loading_circle_gray_07.png", @"loading_circle_gray_08.png", @"loading_circle_gray_01.png"];
    
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:imageNames.count];
    for (NSInteger index = 0; index < imageNames.count; index++)
    {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:index]]];
    }
    
    // Normal Animation
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.animationImages = images;
    imageView.animationDuration = duration;
    return imageView;
}
@end
