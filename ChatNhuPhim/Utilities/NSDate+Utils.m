//
//  NSDate+Utils.m
//  customer
//
//  Created by Minh Đạt Giáp on 5/14/15.
//  Copyright (c) 2015 TamHNM. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)
+ (NSDateComponents *)componentsOfDate:(NSDate *)date{
    return [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth| NSHourCalendarUnit |
            NSMinuteCalendarUnit fromDate:date];
}


+(NSString*) stringDateForConnectHistory:(NSDate*)date{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setDateFormat:@"MMM dd, yyyy hh:mma"];
    return  [dateFormat stringFromDate:date];
}

+(NSDate *) formatISO8601TimeStringAsNSDate:(NSString*)stringDate{
    NSDate * dateIsValid = nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZZ"];
    
    dateIsValid = [dateFormatter dateFromString:stringDate];
    if(!dateIsValid){
        NSLog(@"ERROR: [%s] Date oust is nil - cant pase date string:%@", __PRETTY_FUNCTION__, self);
    }
    return dateIsValid;
}

+(NSString*)stringDateForLicense:(NSDate*)licenseDate{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM/dd/yyyy"];
    return [format stringFromDate:licenseDate];
}
@end
