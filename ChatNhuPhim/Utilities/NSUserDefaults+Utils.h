//
//  NSUserDefaults+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Utils)

+(void)saveDeviceToken:(NSString*)deviceToken;

+(NSString*)getDeviceToken;

+(void)saveUserGender: (NSString*)userGender;

@end
