//
//  UIImage+Utils.m
//  klik
//
//  Created by Minh Dat Giap on 9/16/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIImage+Utils.h"

@implementation UIImage (Utils)
+(UIImage*)imageWithColor:(UIColor*)color{
    CGRect rect = CGRectMake(0.0, 0.0, 1.0, 1.0);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *imageResult = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return imageResult;
}
@end
