//
//  UIView+Utils.m

#import "UIView+Utils.h"

@implementation UIView (Utils)

+(UIView *)viewWithNibName:(NSString *)nibName
{
    NSString *name = (!nibName)? NSStringFromClass([self class]) : nibName;
    return [[[NSBundle mainBundle] loadNibNamed:name owner:nil options:nil] lastObject];
}

-(void)addSubviewWithFullSize:(UIView *)view
{
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleSize;
    [self addSubview:view];
}

- (CGFloat)left {
    return self.frame.origin.x;
}


- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}


- (CGFloat)top {
    return self.frame.origin.y;
}


- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}


- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}


- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right-self.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom-self.height;
    self.frame = frame;
}


- (CGFloat)width {
    return self.frame.size.width;
}


- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}


- (CGFloat)height {
    return self.frame.size.height;
}


- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

+(CGFloat)widthDevice
{
    return CGRectGetWidth([UIScreen mainScreen].bounds);
}

+(CGFloat)heightDevice
{
    return CGRectGetHeight([UIScreen mainScreen].bounds);
}
@end