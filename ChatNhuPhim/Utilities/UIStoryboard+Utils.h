//
//  UIStoryboard+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Utils)
//return a ViewController from StoryBoard Name and a given identifier of ViewController
+(id)viewControllerWithStoryBoardName:(NSString*)storyBoardName identifier:(NSString*)identifier;
+(instancetype)storyboardDefault;
@end
