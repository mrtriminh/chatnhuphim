//
//  NSDate+Utils.h
//  customer
//
//  Created by Minh Đạt Giáp on 5/14/15.
//  Copyright (c) 2015 TamHNM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)
+ (NSDateComponents *)componentsOfDate:(NSDate *)date;

+(NSString*) stringDateForConnectHistory:(NSDate*)date;

+(NSDate *) formatISO8601TimeStringAsNSDate:(NSString*)stringDate;

+(NSString*)stringDateForLicense:(NSDate*)licenseDate;
@end
