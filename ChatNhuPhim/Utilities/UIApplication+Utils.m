//
//  UIApplication+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIApplication+Utils.h"

@implementation UIApplication (Utils)

+(BOOL)canOpenURLAddress:(NSURL *)url{
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        return YES;
    }
    return NO;
}

+(void)openUrlAddress:(NSURL *)url{
    [[UIApplication sharedApplication] openURL:url];
}

+(void)registerAppPushNotification{
    // Let the device know we want to receive push notifications
    UIApplication *app = [UIApplication sharedApplication];
    if ([app respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [app registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [app registerForRemoteNotificationTypes:myTypes];
    }
}

+(void)clearCacheVideo {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [paths objectAtIndex:0];
    NSError * error;
    NSArray * directoryContents =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachesDirectory error:&error];
    for (NSString* stringPath in directoryContents) {
        myLog(@"File name: %@", stringPath);
        if ([stringPath hasSuffix:@".mp4"] || [stringPath hasSuffix:@".mov"]) {
            NSString *path = [NSString stringWithFormat:@"%@/%@", cachesDirectory, stringPath];
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            if (!error) {
                myLog(@"Delete file: %@", stringPath);
            }
        }
    }
    
    NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDownloadsDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory1 = [paths1 objectAtIndex:0];
    NSError * error1;
    NSArray * directoryContents1 =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachesDirectory1 error:&error1];
    for (NSString* stringPath in directoryContents1) {
        myLog(@"File name: %@", stringPath);
        if ([stringPath hasSuffix:@".mp4"] || [stringPath hasSuffix:@".mov"]) {
            NSString *path = [NSString stringWithFormat:@"%@/%@", cachesDirectory1, stringPath];
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error1];
            if (!error) {
                myLog(@"Delete file: %@", stringPath);
            }
        }
    }
}

+(void)clearCacheDocument {
    
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSError *error;
    
    NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirectory error:&error];
    if (!error) {
        for (NSString* stringPath in directoryContents) {
            myLog(@"File name: %@", stringPath);
            if ([stringPath hasSuffix:@".mp4"] || [stringPath hasSuffix:@".mov"]) {
                NSString *path = [NSString stringWithFormat:@"%@/%@", documentDirectory, stringPath];
                [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                if (!error) {
                    myLog(@"Delete file: %@", stringPath);
                }
            }
        }
    }
}

+ (void)removeAllStoredCredentials
{
    // Delete any cached URLrequests!
    NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    [sharedCache removeAllCachedResponses];
    
    // Also delete all stored cookies!
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookies];
    id cookie;
    for (cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
    
    NSDictionary *credentialsDict = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];
    if ([credentialsDict count] > 0) {
        // the credentialsDict has NSURLProtectionSpace objs as keys and dicts of userName => NSURLCredential
        NSEnumerator *protectionSpaceEnumerator = [credentialsDict keyEnumerator];
        id urlProtectionSpace;
        // iterate over all NSURLProtectionSpaces
        while (urlProtectionSpace = [protectionSpaceEnumerator nextObject]) {
            NSEnumerator *userNameEnumerator = [[credentialsDict objectForKey:urlProtectionSpace] keyEnumerator];
            id userName;
            // iterate over all usernames for this protectionspace, which are the keys for the actual NSURLCredentials
            while (userName = [userNameEnumerator nextObject]) {
                NSURLCredential *cred = [[credentialsDict objectForKey:urlProtectionSpace] objectForKey:userName];
                //NSLog(@"credentials to be removed: %@", cred);
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:cred forProtectionSpace:urlProtectionSpace];
            }
        }
    } 
}
@end
