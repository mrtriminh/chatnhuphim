//
//  UIColor+Utils.h
//  Dan
//
//  Created by Minh Đạt Giáp on 8/28/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+(UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha;

+(UIColor *)colorWithHex:(int)hexValue;

+(UIColor *)colorDefaultWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

+(UIColor *)colorDefaultWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString;
@end
