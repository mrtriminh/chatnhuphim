//
//  UIStoryboard+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIStoryboard+Utils.h"

@implementation UIStoryboard (Utils)

+(id)viewControllerWithStoryBoardName:(NSString *)storyBoardName identifier:(NSString *)identifier{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];
    return [storyBoard instantiateViewControllerWithIdentifier:identifier];
}

+(instancetype)storyboardDefault{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return storyBoard;
}
@end
