//
//  UIView+Utils.h


#import <UIKit/UIKit.h>
// view auto resizing
#define UIViewAutoresizingFlexibleSize (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)
#define isEqualAndGreaterThan_iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define isEqualAndGreaterThan_iOS8_1 ([UIDevice currentDevice].systemVersion.floatValue >= 8.1) 
#define isEqualAndGreaterThan_iOS8_0 ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) 
#define isDeviceiPhone35Inch  (([[UIScreen mainScreen] bounds].size.height < 568)?TRUE:FALSE)

typedef struct Margin
{
    CGFloat left;
    CGFloat top;
    CGFloat right;
    CGFloat bottom;
}Margin;

@interface UIView (Utils)
+(UIView *)viewWithNibName:(NSString *)nibName;
-(void)addSubviewWithFullSize:(UIView *)view;


/**
 This gives the top Frame coordinate of the receiver.
 This represent the x coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat left;

/**
 This gives the top Frame coordinate of the receiver.
 This represent the y coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat top;

/**
 This gives the right coordinate of the receiver.
 This represent the x + width coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat right;

/**
 This gives the bottom coordinate of the receiver.
 This represent the y + height coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat bottom;


/**
 This gives the width coordinate of the receiver.
 This represent the width coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat width;

/**
 This gives the height coordinate of the receiver.
 This represent the height coordinate of the receiver, this is the frame position.
 */
@property (nonatomic) CGFloat height;

+(CGFloat)widthDevice;
+(CGFloat)heightDevice;
@end
