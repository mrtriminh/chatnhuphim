//
//  UIImage+Utils.h
//  klik
//
//  Created by Minh Dat Giap on 9/16/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)
+(UIImage*)imageWithColor:(UIColor*)color;
@end
