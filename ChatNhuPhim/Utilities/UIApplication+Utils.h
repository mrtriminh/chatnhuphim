//
//  UIApplication+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Utils)

//can open url by UIApplication URL or not
+(BOOL)canOpenURLAddress:(NSURL*)url;

//open the url address by UIApplication
+(void)openUrlAddress:(NSURL*)url;

+(void)registerAppPushNotification;

+(void)clearCacheVideo;

+(void)clearCacheDocument;

+ (void)removeAllStoredCredentials;
@end
