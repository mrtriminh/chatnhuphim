//
//  ProgressEntity.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/5/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProgressEntity : NSObject
@property (strong, nonatomic) UIView *progressView;
@property (strong, nonatomic) NSDate *begin;
@property (strong, nonatomic) NSDate *end;
@property (assign, nonatomic) NSTimeInterval duration;
@property (assign, nonatomic) ProgressState state;
@end
