//
//  NSMutableDictionary+Clips_Utils.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "NSMutableDictionary+Clips_Utils.h"

@implementation NSMutableDictionary (Clips_Utils)
+(instancetype)clipsDictionaryWithHeaderInfo:(id)headerInfo contentInfo:(id)contentInfo isOpen:(BOOL)isOpen{
    if (!headerInfo) {
        headerInfo = [NSDictionary dictionary];
    }
    
    if (!contentInfo) {
        contentInfo = [NSDictionary dictionary];
    }
    return [NSMutableDictionary dictionaryWithObjects:@[headerInfo, contentInfo, @(isOpen)] forKeys:@[kDictHeaderInfo, kDictContentInfo, kDictIsOpen]];
}

-(void)setIsOpen:(BOOL)isOpen{
    [self setValue:@(isOpen) forKey:kDictIsOpen];
}

-(BOOL)getIsOpen{
    return [[self valueForKey:kDictIsOpen] isEqualToNumber:@1]?YES:NO;
}

-(void)setHeaderHeight:(NSNumber *)headerHeight{
    [self setValue:headerHeight forKey:kDictHeaderHeight];
}

-(float)getHeaderHeight{
    return [[self valueForKey:kDictHeaderHeight] floatValue];
}

-(void)setContentHeight:(NSNumber *)contentHeight{
    [self setValue:contentHeight forKey:kDictContentHeight];
}

-(float)getContentHeight{
    return [[self valueForKey:kDictContentHeight] floatValue];
}

-(void)setHeaderInfo:(id)headerInfo{
    if (!headerInfo) {
        headerInfo = [NSDictionary dictionary];
    }
    [self setValue:headerInfo forKey:kDictHeaderInfo];
}

-(void)setContentInfo:(id)contentInfo{
    if (!contentInfo) {
        contentInfo = [NSDictionary dictionary];
    }
    [self setValue:contentInfo forKey:kDictHeaderInfo];
}

-(id)getHeaderInfo{
    return [self valueForKey:kDictHeaderInfo];
}

-(id)getContentInfo{
    return [self valueForKey:kDictContentInfo];
}

@end
