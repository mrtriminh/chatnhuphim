//
//  ClipsBusinessManager.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/10/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "ClipsBusinessManager.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSString+Utils.h"
#import "PreviewClips.h"
#import "ScriptEntity.h"

@implementation ClipsBusinessManager

//Category for clips

+(void)getAllCategoryComplete:(NetworkResponse)completion {
    [[APIManager shared] methodGETdataWithParams:nil apiName:kAPIAllCategory completion:^(BOOL success, id data, NSString *error) {
        completion(success, data, error);
    }];
}


+(void)getClipsWithCategoryName:(NSString*)categoryName completion:(NetworkResponse)completion {
    id params = @{kDictCategory : categoryName};
    
    [[APIManager shared] methodGETdataWithParams:params apiName:kAPIExplore completion:^(BOOL success, id data, NSString *error) {
        
        if (success) {
            NSMutableArray *result = [NSMutableArray arrayWithCapacity:[data count]];
            for (NSDictionary *info in data) {
                NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[info valueForKey:kDictVideoName] videoURL:[info valueForKey:kDictVideoURL] thumbnailURL:[info valueForKey:kDictThumbnailURL]];
                [result addObject:dict];
            }
            completion(success, result, error);
        }else {
            completion(success, nil, error);
        }
    }];
}

//Search a clips
+(void)searchClipsWithKeyword:(NSString*)keyWord genderVoice:(GenderTypeIcon)genderType comletion:(NetworkResponse)completion {
    
    id param = @{kDictKeyword : keyWord, kDictVoice :[NSString stringWithGenderType:genderType]};
    
    [[APIManager shared] methodGETdataWithParams:param apiName:kAPISearchKeyword completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            id array = [data valueForKey:kDictSuggestion];
            id video = [data valueForKey:@"video"];
            NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[video valueForKey:kDictVideoName] videoURL:[video valueForKey:kDictVideoURL] thumbnailURL:[video valueForKey:kDictThumbnailURL]];
            
            NSMutableArray *result = [NSMutableArray arrayWithCapacity:[array count]];
            [result addObject:dict];
            
            for (NSDictionary *info in array) {
                NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[info valueForKey:kDictVideoName] videoURL:[info valueForKey:kDictVideoURL] thumbnailURL:[info valueForKey:kDictThumbnailURL]];
                [result addObject:dict];
            }
            completion(success, result, error);
        }else {
            completion(success, nil, error);
        }
    }];
}

//Favorite
+(void)getFavoriteClipsWithUUID:(NSString*)UUID completion:(NetworkResponse)completion {
    if ([NSString isEmptyString:UUID]) {
        UUID = @"";
    }
    id param = @{kDictUUID : UUID};
    
    [[APIManager shared] methodGETdataWithParams:param apiName:kAPIGetFavorite completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            NSMutableArray *result = [NSMutableArray arrayWithCapacity:[data count]];
            for (id info in data) {
                if ([info isKindOfClass:[NSDictionary class]]) {
                    NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[info valueForKey:kDictVideoName] videoURL:[info valueForKey:kDictVideoURL] thumbnailURL:[info valueForKey:kDictThumbnailURL]];
                    [result addObject:dict];
                }
            }
            completion(success, result, error);
        }else {
            completion(success, nil, error);
        }
    }];
}

+(void)addClipToFavoriteWithUUID:(NSString*)UUID urlVideo:(NSString*)URLVideo completion:(NetworkResponse)completion {
    if ([NSString isEmptyString:UUID]) {
        UUID = @"";
    }
    id param = @{kDictUUID: UUID, kDictVideoURL: URLVideo};
    
    [[APIManager shared] methodPOSTdataWithParams:param apiName:kAPIAddFavorite completion:^(BOOL success, id data, NSString *error) {
        completion(success, data, error);
    }];
}

+(void)deleteClipToFavoriteWithUUID:(NSString*)UUID urlVideo:(NSString*)URLVideo completion:(NetworkResponse)completion {
    if ([NSString isEmptyString:UUID]) {
        UUID = @"";
    }
    id param = @{kDictUUID: UUID, kDictVideoURL: URLVideo};
    
    [[APIManager shared] methodPOSTdataWithParams:param apiName:kAPIDeleteFavorite completion:^(BOOL success, id data, NSString *error) {
        completion(success, data, error);
    }];
}


+(void)getAllSequences:(id)params completion:(NetworkResponse)completion {
    [[APIManager shared] methodGETdataWithParams:params apiName:kAPIAllSequences completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            if ([data isKindOfClass:[NSArray class]]) {
                //id info = [[data firstObject] valueForKey:kDictItems];
                NSMutableArray *arrayResults = [NSMutableArray arrayWithCapacity:[data count]];
                for (NSDictionary *dict in data) {
                    
                    ScriptEntity *script = [[ScriptEntity alloc] init];
                    NSInteger ID = [[dict valueForKey:kDictID] integerValue];
                    script.ID = ID;
                    
                    NSString *message = [dict valueForKey:kDictMessage];
                    NSString *thumbnail = [dict valueForKey:kDictThumbnailURL];
                    thumbnail = [thumbnail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    script.message = message;
                    if(![NSString isEmptyString:thumbnail]) {
                        script.thumbnailURL = [NSURL URLWithString:thumbnail];
                    }
                    [arrayResults addObject:script];
                }
                completion(YES, arrayResults, nil);
            }
            else {
                completion(YES, nil, nil);
            }
        }else {
            completion(NO, nil, nil);
        }
    }];
}

+(void)listSequencesSuggestion:(NSString *)slideshowID completion:(NetworkResponse)completion {//kAPIAllSequences,kAPISlideshowDetail
    id param = @{kDictItemID: slideshowID};
    [[APIManager shared] methodGETdataWithParams:param apiName:kAPISlideshowDetail completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            if ([data isKindOfClass:[NSDictionary class]]) {
                
                NSArray *info = [data valueForKey:kDictItems];
                NSMutableArray *results = [NSMutableArray arrayWithCapacity:[info count]];
                for (NSDictionary *dict in info) {
                    
                    PreviewClips *clips = [[PreviewClips alloc] init];
                    
                    NSString *type = [dict valueForKey:kDictType];
                    clips.type = type;
                    if ([type isEqualToString:kDictTypeSelect]) {
                        id file = [dict valueForKey:kDictFile];
                        if ([file isKindOfClass:[NSDictionary class]]) {
                            NSString *thumbnail = [file valueForKey:kDictThumbnailURL];
                            thumbnail = [thumbnail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            clips.thumbnail = [NSURL URLWithString:thumbnail];
                            
                            NSString *videoUrl = [file valueForKey:kDictVideoURL];
                             NSLog(@"video path %@",videoUrl);
                            videoUrl = [videoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            clips.videoURL = [NSURL URLWithString:videoUrl];
                        }
                        clips.arrRecordFiles = nil;
                        clips.downloadIsCompleted = NO;
                    }else if ([type isEqualToString:kDictTypeUser]) {
                        clips.suggestionWords = [dict valueForKey:kDictMessage];
                        
                        NSString *audioUrl = [dict valueForKey:kDictAudioURL];
                        NSLog(@"audio path %@",audioUrl);
                        audioUrl = [audioUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        clips.audioURL = [NSURL URLWithString:audioUrl];
                    }
                    
                    [results addObject:clips];
                }
                
                completion(YES, results, nil);
            }
            else {
                myLog(@"This is a Dictionary:%@", data);
                completion(YES, nil, nil);
            }
        }else {
            completion(NO, nil, nil);
        }
    }];
}

+(void)searchClipsWithKeyword:(NSString*)keyWord categoryName:(NSString*)categoryName comletion:(NetworkResponse)completion {
    if ([NSString isEmptyString:categoryName] || [categoryName isEqualToString:@"by anyone"] || [categoryName isEqualToString:@"của ai cũng được"] ) {
        categoryName = @"";
    }
    id param = @{kDictKeyword : keyWord, kDictVoice :categoryName};
    
    [[APIManager shared] methodGETdataWithParams:param apiName:kAPISearchKeyword completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            id array = [data valueForKey:kDictSuggestion];
            id video = [data valueForKey:@"video"];
            NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[video valueForKey:kDictVideoName] videoURL:[video valueForKey:kDictVideoURL] thumbnailURL:[video valueForKey:kDictThumbnailURL]];
            
            NSMutableArray *result = [NSMutableArray arrayWithCapacity:[array count]];
            [result addObject:dict];
            
            for (NSDictionary *info in array) {
                NSMutableDictionary *dict = [NSMutableDictionary videoClipsDictWithVideoName:[info valueForKey:kDictVideoName] videoURL:[info valueForKey:kDictVideoURL] thumbnailURL:[info valueForKey:kDictThumbnailURL]];
                [result addObject:dict];
            }
            completion(success, result, error);
        }else {
            completion(success, nil, error);
        }
    }];
}

@end
