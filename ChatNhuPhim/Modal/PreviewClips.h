//
//  PreviewClips.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/27/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreviewClips : NSObject
@property (copy, nonatomic) NSURL *thumbnail;
@property (copy, nonatomic) NSURL *videoURL;
@property (copy, nonatomic) NSURL *audioURL;
@property (assign, nonatomic) NSInteger ID;
@property (copy, nonatomic) NSString *type;
@property (strong, nonatomic) NSURL *localVideoURL;
@property (strong, nonatomic) NSURL *localAudioURL;
@property (strong, nonatomic) NSMutableArray *arrRecordFiles;
@property (assign, nonatomic) long long totalBytesRead;
@property (assign, nonatomic) long long totalBytesExpectedToRead;
@property (assign, nonatomic) BOOL downloadIsCompleted;
@property (copy, nonatomic) NSString *suggestionWords;
@end
