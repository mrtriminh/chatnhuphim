//
//  NSMutableDictionary+Clips_Utils.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Clips_Utils)
+(instancetype)clipsDictionaryWithHeaderInfo:(id)headerInfo contentInfo:(id)contentInfo isOpen:(BOOL)isOpen;

-(void)setIsOpen:(BOOL)isOpen;

-(BOOL)getIsOpen;

-(void)setHeaderInfo:(id)headerInfo;

-(id)getHeaderInfo;

-(void)setContentInfo:(id)contentInfo;

-(id)getContentInfo;

-(void)setHeaderHeight:(NSNumber*)headerHeight;

-(float)getHeaderHeight;

-(void)setContentHeight:(NSNumber*)contentHeight;

-(float)getContentHeight;

@end
