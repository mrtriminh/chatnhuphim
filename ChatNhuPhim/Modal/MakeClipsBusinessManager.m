//
//  MakeClipsBusinessManager.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "MakeClipsBusinessManager.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "VideoPlayViewController.h"
#import "PreviewPlayViewController.h"
#import "UIStoryboard+Utils.h"
#import "UIView+Utils.h"
#import "PreviewClips.h"

#define MAX_WIDTH 850.0
#define MAX_HEIGHT 480.0
#define MAKE_CLIPS_WIDTH    (CGRectGetWidth([UIScreen mainScreen].bounds)*2.0)
#define MAKE_CLIPS_HEIGHT   (180*2.0)

enum MessengerShareMode {
    MessengerShareModeSend,
    MessengerShareModeComposer,
    MessengerShareModeReply
};
enum MessengerShareMode messengerShareMode;

@implementation MakeClipsBusinessManager

+(void)mergerAndSaveClipsToPhoto:(NSArray*)arrMakeDatas presentPlayBackController:(BOOL)flag completion:(void(^)(BOOL success, NSURL *fileURL))completion {
    [self mergerAndSaveClipsToPhoto:arrMakeDatas presentPlayBackController:flag isFromScriptClips:NO completion:completion];
}

+(void)mergerAndSaveClipsToPhoto:(NSArray*)arrMakeDatas presentPlayBackController:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion {
    
    NSMutableArray *tempArrayVideo = [NSMutableArray arrayWithCapacity:[arrMakeDatas count]];
    for (id object in arrMakeDatas) {
        BOOL isDictionary = [object isKindOfClass:[NSDictionary class]];
        if (isDictionary) {// Is a dictionay object
            if ([object getURLPath]) {
                [tempArrayVideo addObject:object];
            }
        }else {//Is Preview Clips object
            PreviewClips *clips = (PreviewClips*)object;
            if (clips.localVideoURL) {
                [tempArrayVideo addObject:object];
                //NSLog(@"url video in arrMakeDatas %@",clips.localVideoURL);
            }
        }
    }
    
    if (![tempArrayVideo count]) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kPlayAllNoVideo") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
        return;
    }
    
    //stop video if has any video was playbacked
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    __block AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __block UIView *rootView = delegate.window.rootViewController.view;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
    hud.labelText = myLanguage(@"kMergeClip");
    hud.minShowTime =1.5;
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    NSMutableArray *arrayInstruction = [[NSMutableArray alloc] init];
    AVMutableVideoCompositionInstruction *videoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    AVMutableCompositionTrack *audioTrack;
    audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                             preferredTrackID:kCMPersistentTrackID_Invalid];
    
    CMTime duration = kCMTimeZero;
    for(NSInteger i=0;i< [tempArrayVideo count]; i++) {
        
        //hud.labelText = [NSString stringWithFormat:@"%@ %d videos",myLanguage(@"kMergeClip1"),(i+1)];
        
        id object = [tempArrayVideo objectAtIndex:i];
        BOOL isDictionary = [object isKindOfClass:[NSDictionary class]];
        NSURL *videoUrl = nil;
        if (isDictionary) {
            videoUrl = [object getURLPath];
        }else {
            PreviewClips *clips = (PreviewClips*)object;
            videoUrl = clips.localVideoURL;
        }
        
        AVAsset *currentAsset = [AVAsset assetWithURL:videoUrl];
        
        
        AVMutableCompositionTrack *currentTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [currentTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:duration error:nil];
        
        [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:duration error:nil];
        
        AVMutableVideoCompositionLayerInstruction *currentAssetLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:currentTrack];
        AVAssetTrack *currentAssetTrack = [[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        UIImageOrientation currentAssetOrientation  = UIImageOrientationUp;
        BOOL  isCurrentAssetPortrait  = NO;
        CGAffineTransform currentTransform = currentAssetTrack.preferredTransform;
        
        if(currentTransform.a == 0 && currentTransform.b == 1.0 && currentTransform.c == -1.0 && currentTransform.d == 0) {
            currentAssetOrientation= UIImageOrientationRight;
            isCurrentAssetPortrait = YES;
        }
        if(currentTransform.a == 0 && currentTransform.b == -1.0 && currentTransform.c == 1.0 && currentTransform.d == 0) {
            currentAssetOrientation =  UIImageOrientationLeft; isCurrentAssetPortrait = YES;
        }
        if(currentTransform.a == 1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == 1.0) {
            currentAssetOrientation =  UIImageOrientationUp;
        }
        if(currentTransform.a == -1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == -1.0) {currentAssetOrientation = UIImageOrientationDown;
        }
        myLog(@"Current size: %f, %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height);
        
        CGFloat assetScaleToFitRatio;
        if (!isDictionary) {
            assetScaleToFitRatio = [self getScaleToFitRatioCurrentTrack:currentTrack];
        }else {
            assetScaleToFitRatio = [self getScaleToFitRatio:[tempArrayVideo objectAtIndex:i] currentTrack:currentTrack];//makeInfo
        }
        
        //Get screensize to calculate crop area
//        CGRect screenBound = [[UIScreen mainScreen] bounds];
//        CGSize screenSize = screenBound.size;
//        CGFloat screenHeight = screenSize.height;
//        CGFloat cropArea = 90/screenHeight;
//        myLog(@"percent to crop %f",cropArea);
        
        if(isCurrentAssetPortrait){
            CGFloat assetScaleToFitPortraitRatio = (MAX_WIDTH/(currentTrack.naturalSize.height));
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitPortraitRatio,assetScaleToFitPortraitRatio);
            
            //            int ratio = (int) roundf(assetScaleToFitPortraitRatio);
            //            myLog(@"ratio :%d", ratio);
            CGFloat translateAxisX = (0.0);
            myLog(@"W,H:(%f, %f), ratio: %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height, assetScaleToFitPortraitRatio);
            CGFloat translateAxisY = (((currentTrack.naturalSize.height/1.75)*assetScaleToFitPortraitRatio));
            myLog(@"width :%f", currentTrack.naturalSize.width);
            myLog(@"y :%f", translateAxisY);
            [currentAssetLayerInstruction setTransform:
             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX,-translateAxisY)) atTime:duration];
        }else{
            CGFloat translateAxisX = (currentTrack.naturalSize.width > MAX_WIDTH )?(0.0):0.0;// if use <, 640 video will be moved left by 10px. (float)(MAX_WIDTH - currentTrack.naturalSize.width)/(float)4.0
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitRatio,assetScaleToFitRatio);
            [currentAssetLayerInstruction setTransform:
             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX, 0)) atTime:duration];
        }
        
        duration=CMTimeAdd(duration, currentAsset.duration);
        
        [currentAssetLayerInstruction setOpacity:0.0 atTime:duration];
        [arrayInstruction addObject:currentAssetLayerInstruction];
    }
    
    videoCompositionInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, duration);
    videoCompositionInstruction.layerInstructions = arrayInstruction;
    
    
    CALayer *aLayer;
    CALayer *parentLayer;
    CALayer *videoLayer;
    UILabel *labelLogo = [[UILabel alloc] init];
    labelLogo.text = @"App Chat=Phim";
    labelLogo.textColor = [UIColor whiteColor];
    labelLogo.font = [UIFont systemFontOfSize:40.0];
    [labelLogo sizeToFit];
    
    
    aLayer  = [CALayer layer];
    [aLayer addSublayer:labelLogo.layer];
    aLayer.frame = CGRectMake(MAX_WIDTH- labelLogo.width - 10.0, MAX_HEIGHT-50.0, 20.0, 20.0);
    aLayer.opacity = 1;
    
    parentLayer = [CALayer layer];
    videoLayer  = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, MAX_WIDTH,MAX_HEIGHT);
    videoLayer.frame = CGRectMake(0, 0, MAX_WIDTH,MAX_HEIGHT);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:aLayer];
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    videoComposition.instructions = [NSArray arrayWithObject:videoCompositionInstruction];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    videoComposition.renderSize = CGSizeMake(MAX_WIDTH, MAX_HEIGHT);
    
    NSString *myPathDocs =  [[self applicationCacheDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo%-dtemp.mp4",arc4random() % 10000]];
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    myLog(@"Path: %@", myPathDocs);
    
    NSArray *array = [myLanguage(@"kMergeClipArray") componentsSeparatedByString:@","];
    int count = (int)[array count];
    NSInteger rand = (int)arc4random_uniform(count);
    hud.labelText = [array objectAtIndex:rand];
    hud.minShowTime =1.0;
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL = url;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.videoComposition = videoComposition;
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^ {
        [MBProgressHUD hideAllHUDsForView:rootView animated:YES];
        switch (exporter.status) {
            case AVAssetExportSessionStatusCompleted: {
                NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                if (flag) {
                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    VideoPlayViewController *videoController = [storyBoard instantiateViewControllerWithIdentifier:kVideoPlayViewController];
                    videoController.URLVideo = url;
                    videoController.arrScriptsClip = nil;
                    if (isFromScriptClips) {
                        videoController.arrScriptsClip = arrMakeDatas;
                        videoController.isExportToMake = YES;
                    }
                    [delegate.window.rootViewController presentViewController:videoController animated:YES completion:nil];
                    //callback
                    if (completion) {
                        completion(YES, url);
                    }
                }else{
                    //callback
                    if (completion) {
                        completion(YES, url);
                    }
                }
            }
                break;
            case AVAssetExportSessionStatusFailed:
                myLog(@"Failed:%@", exporter.error.description);
                break;
            case AVAssetExportSessionStatusCancelled: {
                myLog(@"Canceled:%@", exporter.error);
                //callback
                if (completion) {
                    completion(NO, nil);
                }
            }
                break;
            case AVAssetExportSessionStatusExporting:
                myLog(@"Exporting!");
                break;
            case AVAssetExportSessionStatusWaiting:
                myLog(@"Waiting");
                break;
            default:
                break;
        }
    }];
}

//Merge user clips with audio background
+(void)mergerUserClipWithAudio:(NSArray*)arrMakeDatas audioURL:(NSURL*)audioFile showHUD:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion {
    
    NSMutableArray *tempArrayVideo = [NSMutableArray arrayWithCapacity:[arrMakeDatas count]];
    for (id object in arrMakeDatas) {
        BOOL isDictionary = [object isKindOfClass:[NSDictionary class]];
        if (isDictionary) {// Is a dictionay object
            if ([object getURLPath]) {
                [tempArrayVideo addObject:object];
            }
        }else {//Is Preview Clips object
            PreviewClips *clips = (PreviewClips*)object;
            if (clips.localVideoURL) {
                [tempArrayVideo addObject:object];
            }
        }
    }
    
    if (![tempArrayVideo count]) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kPlayAllNoVideo") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
        return;
    }
    
    //stop video if has any video was playbacked
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    __block AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __block UIView *rootView = delegate.window.rootViewController.view;
    
    if (flag) {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
    hud.labelText = myLanguage(@"kMergeClip");
    hud.minShowTime =1.0;
    }
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    NSMutableArray *arrayInstruction = [[NSMutableArray alloc] init];
    AVMutableVideoCompositionInstruction *videoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    //Load test audio file
    //NSURL *audioFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"applause" ofType:@"mp3"]];
    AVURLAsset  *audioAsset = [[AVURLAsset alloc]initWithURL:audioFile options:nil];
    NSLog(@"audio File %@",audioFile);
    AVMutableCompositionTrack *audioTrack;
    //audioTrack.preferredVolume = 1;
    audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                             preferredTrackID:kCMPersistentTrackID_Invalid];
    //audioTrack.preferredVolume = 1;
    CMTime duration = kCMTimeZero;
   
    for(NSInteger i=0;i< [tempArrayVideo count]; i++) {
        
        id object = [tempArrayVideo objectAtIndex:i];
        BOOL isDictionary = [object isKindOfClass:[NSDictionary class]];
        NSURL *videoUrl = nil;
        if (isDictionary) {
            videoUrl = [object getURLPath];
        }else {
            PreviewClips *clips = (PreviewClips*)object;
            videoUrl = clips.localVideoURL;
        }
        
        AVAsset *currentAsset = [AVAsset assetWithURL:videoUrl];
        
        AVMutableCompositionTrack *currentTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [currentTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:duration error:nil];
        
        [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:duration error:nil];
        
        AVMutableVideoCompositionLayerInstruction *currentAssetLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:currentTrack];
        AVAssetTrack *currentAssetTrack = [[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        UIImageOrientation currentAssetOrientation  = UIImageOrientationUp;
        BOOL  isCurrentAssetPortrait  = NO;
        CGAffineTransform currentTransform = currentAssetTrack.preferredTransform;
        
        if(currentTransform.a == 0 && currentTransform.b == 1.0 && currentTransform.c == -1.0 && currentTransform.d == 0) {
            currentAssetOrientation= UIImageOrientationRight;
            isCurrentAssetPortrait = YES;
        }
        if(currentTransform.a == 0 && currentTransform.b == -1.0 && currentTransform.c == 1.0 && currentTransform.d == 0) {
            currentAssetOrientation =  UIImageOrientationLeft; isCurrentAssetPortrait = YES;
        }
        if(currentTransform.a == 1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == 1.0) {
            currentAssetOrientation =  UIImageOrientationUp;
        }
        if(currentTransform.a == -1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == -1.0) {currentAssetOrientation = UIImageOrientationDown;
        }
        myLog(@"Current size: %f, %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height);
        
        CGFloat assetScaleToFitRatio;
        if (!isDictionary) {
            assetScaleToFitRatio = [self getScaleToFitRatioCurrentTrack:currentTrack];
        }else {
            assetScaleToFitRatio = [self getScaleToFitRatio:[tempArrayVideo objectAtIndex:i] currentTrack:currentTrack];//makeInfo
        }
        
        //Get screensize to calculate crop area
        //        CGRect screenBound = [[UIScreen mainScreen] bounds];
        //        CGSize screenSize = screenBound.size;
        //        CGFloat screenHeight = screenSize.height;
        //        CGFloat cropArea = 90/screenHeight;
        //        myLog(@"percent to crop %f",cropArea);
        
        if(isCurrentAssetPortrait){
            CGFloat assetScaleToFitPortraitRatio = (MAX_WIDTH/(currentTrack.naturalSize.height));
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitPortraitRatio,assetScaleToFitPortraitRatio);
            
            //            int ratio = (int) roundf(assetScaleToFitPortraitRatio);
            //            myLog(@"ratio :%d", ratio);
            CGFloat translateAxisX = (0.0);
            myLog(@"W,H:(%f, %f), ratio: %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height, assetScaleToFitPortraitRatio);
            CGFloat translateAxisY = (((currentTrack.naturalSize.height/1.75)*assetScaleToFitPortraitRatio));
            myLog(@"width :%f", currentTrack.naturalSize.width);
            myLog(@"y :%f", translateAxisY);
            [currentAssetLayerInstruction setTransform:
             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX,-translateAxisY)) atTime:duration];
        }else{
            CGFloat translateAxisX = (currentTrack.naturalSize.width > MAX_WIDTH )?(0.0):0.0;// if use <, 640 video will be moved left by 10px. (float)(MAX_WIDTH - currentTrack.naturalSize.width)/(float)4.0
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitRatio,assetScaleToFitRatio);
            [currentAssetLayerInstruction setTransform:
             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX, 0)) atTime:duration];
        }
        
        duration=CMTimeAdd(duration, currentAsset.duration);
        
        [currentAssetLayerInstruction setOpacity:0.0 atTime:duration];
        [arrayInstruction addObject:currentAssetLayerInstruction];
    }
    
    AVMutableCompositionTrack *AudioBGTrack;
    //AudioBGTrack.preferredVolume = 0.1;
    AudioBGTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    [AudioBGTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    
    videoCompositionInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, duration);
    videoCompositionInstruction.layerInstructions = arrayInstruction;
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.instructions = [NSArray arrayWithObject:videoCompositionInstruction];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    videoComposition.renderSize = CGSizeMake(MAX_WIDTH, MAX_HEIGHT);
    
    //Save to tmp folder to avoid cache cleared
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tmpDir = [[documentsDirectory stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"tmp"];
    NSString *myPathDocs =  [tmpDir stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo%-dtemp.mp4",arc4random() % 10000]];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    myLog(@"Path: %@", myPathDocs);
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL = url;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.videoComposition = videoComposition;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^ {
        if (flag) {
            [MBProgressHUD hideAllHUDsForView:rootView animated:YES];
        }
        switch (exporter.status) {
            case AVAssetExportSessionStatusCompleted: {
                NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                NSLog(@"export user clips");
                    //callback
                    if (completion) {
                        completion(YES, url);
                    }
            }
                break;
            case AVAssetExportSessionStatusFailed:
                myLog(@"Failed:%@", exporter.error.description);
                break;
            case AVAssetExportSessionStatusCancelled: {
                myLog(@"Canceled:%@", exporter.error);
                //callback
                if (completion) {
                    completion(NO, nil);
                }
            }
                break;
            case AVAssetExportSessionStatusExporting:
                myLog(@"Exporting!");
                break;
            case AVAssetExportSessionStatusWaiting:
                myLog(@"Waiting");
                break;
            default:
                break;
        }
    }];
}

//Merge clips with user recorded audio
+(void)mergerUserAudio:(NSURL*)audioFile withClip:(NSURL*)videoFile showHUD:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion {
    
    //stop video if has any video was playbacked
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    __block AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __block UIView *rootView = delegate.window.rootViewController.view;
    
    if (flag) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
        hud.labelText = myLanguage(@"kMergeAudio");
        hud.minShowTime =1.0;
    }
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    AVAsset *firstAsset = [AVAsset assetWithURL:videoFile];
    AVAsset *audioAsset = [AVAsset assetWithURL:audioFile];
    
    //VIDEO TRACK
    AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    
    //AUDIO TRACK
    if(audioAsset!=nil){
        AVMutableCompositionTrack *AudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
        AudioTrack.preferredVolume = 2.0f;
    }
    
    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, firstAsset.duration);
    
    //FIXING ORIENTATION//
    AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
    AVAssetTrack *FirstAssetTrack = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
    BOOL  isFirstAssetPortrait_  = NO;
    CGAffineTransform firstTransform = FirstAssetTrack.preferredTransform;
    if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)  {FirstAssetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;}
    if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {FirstAssetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;}
    if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {FirstAssetOrientation_ =  UIImageOrientationUp;}
    if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {FirstAssetOrientation_ = UIImageOrientationDown;}
//    CGFloat FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.width;
//    if(isFirstAssetPortrait_){
//        FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.height;
//        CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
//        [FirstlayerInstruction setTransform:CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor) atTime:kCMTimeZero];
//    }else{
//        CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
//        [FirstlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 160)) atTime:kCMTimeZero];
//    }
    CGFloat assetScaleToFitRatio;
    assetScaleToFitRatio = [self getScaleToFitRatioCurrentTrack:firstTrack];
    
    if(isFirstAssetPortrait_){
        CGFloat FirstAssetScaleToFitRatio = (MAX_WIDTH/(FirstAssetTrack.naturalSize.height));
        CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
        
        //            int ratio = (int) roundf(assetScaleToFitPortraitRatio);
        //            myLog(@"ratio :%d", ratio);
        CGFloat translateAxisX = (0.0);
        CGFloat translateAxisY = (((firstTrack.naturalSize.height/1.75)* FirstAssetScaleToFitRatio));
        [FirstlayerInstruction setTransform:
         CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX,-translateAxisY)) atTime:firstAsset.duration];
    }else{
        CGFloat translateAxisX = (firstTrack.naturalSize.width > MAX_WIDTH )?(0.0):0.0;// if use <, 640 video will be moved left by 10px. (float)(MAX_WIDTH - currentTrack.naturalSize.width)/(float)4.0
        CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitRatio,assetScaleToFitRatio);
        [FirstlayerInstruction setTransform:
         CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX, 0)) atTime:firstAsset.duration];
    }
    [FirstlayerInstruction setOpacity:0.0 atTime:firstAsset.duration];
  
    MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,nil];;
    
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    MainCompositionInst.renderSize = CGSizeMake(MAX_WIDTH, MAX_HEIGHT);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = MainCompositionInst;
    exporter.shouldOptimizeForNetworkUse = YES;
    [MBProgressHUD hideAllHUDsForView:rootView animated:YES];
//    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
//    NSMutableArray *arrayInstruction = [[NSMutableArray alloc] init];
//   
//    AVMutableVideoCompositionInstruction *videoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//    
//    AVAsset *currentAsset = [AVAsset assetWithURL:videoFile];
//    
//    AVURLAsset  *audioAsset = [[AVURLAsset alloc]initWithURL:audioFile options:nil];
//    NSLog(@"audio file %@",audioFile);
//    NSLog(@"audio duration %f vs video duration %f",CMTimeGetSeconds(audioAsset.duration),CMTimeGetSeconds(currentAsset.duration));
//    AVMutableCompositionTrack *AudioBGTrack;
//    //AudioBGTrack.preferredVolume = 0.1;
//    AudioBGTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
//    [AudioBGTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
//    
//    AVMutableCompositionTrack *audioTrack;
//    //audioTrack.preferredVolume = 1;
////    audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
////                                             preferredTrackID:kCMPersistentTrackID_Invalid];
//    CMTime duration = kCMTimeZero;
//    
//       NSError *error = nil;
//        AVMutableCompositionTrack *currentTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
//        [currentTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:duration error:&error];
//        
//        [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentAsset.duration) ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:duration error:nil];
//    if (error) {
//        NSLog(@"Insertion error: %@", error);
//    }
//        AVMutableVideoCompositionLayerInstruction *currentAssetLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:currentTrack];
//        AVAssetTrack *currentAssetTrack = [[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//        UIImageOrientation currentAssetOrientation  = UIImageOrientationUp;
//        BOOL  isCurrentAssetPortrait  = NO;
//        CGAffineTransform currentTransform = currentAssetTrack.preferredTransform;
//        
//        if(currentTransform.a == 0 && currentTransform.b == 1.0 && currentTransform.c == -1.0 && currentTransform.d == 0) {
//            currentAssetOrientation= UIImageOrientationRight;
//            isCurrentAssetPortrait = YES;
//        }
//        if(currentTransform.a == 0 && currentTransform.b == -1.0 && currentTransform.c == 1.0 && currentTransform.d == 0) {
//            currentAssetOrientation =  UIImageOrientationLeft; isCurrentAssetPortrait = YES;
//        }
//        if(currentTransform.a == 1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == 1.0) {
//            currentAssetOrientation =  UIImageOrientationUp;
//        }
//        if(currentTransform.a == -1.0 && currentTransform.b == 0 && currentTransform.c == 0 && currentTransform.d == -1.0) {currentAssetOrientation = UIImageOrientationDown;
//        }
//        myLog(@"Current size: %f, %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height);
//        
//        CGFloat assetScaleToFitRatio;
//        assetScaleToFitRatio = [self getScaleToFitRatioCurrentTrack:currentTrack];
//    
//        if(isCurrentAssetPortrait){
//            CGFloat assetScaleToFitPortraitRatio = (MAX_WIDTH/(currentTrack.naturalSize.height));
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitPortraitRatio,assetScaleToFitPortraitRatio);
//            
//            //            int ratio = (int) roundf(assetScaleToFitPortraitRatio);
//            //            myLog(@"ratio :%d", ratio);
//            CGFloat translateAxisX = (0.0);
//            myLog(@"W,H:(%f, %f), ratio: %f", currentTrack.naturalSize.width, currentTrack.naturalSize.height, assetScaleToFitPortraitRatio);
//            CGFloat translateAxisY = (((currentTrack.naturalSize.height/1.75)*assetScaleToFitPortraitRatio));
//            myLog(@"width :%f", currentTrack.naturalSize.width);
//            myLog(@"y :%f", translateAxisY);
//            [currentAssetLayerInstruction setTransform:
//             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX,-translateAxisY)) atTime:duration];
//        }else{
//            CGFloat translateAxisX = (currentTrack.naturalSize.width > MAX_WIDTH )?(0.0):0.0;// if use <, 640 video will be moved left by 10px. (float)(MAX_WIDTH - currentTrack.naturalSize.width)/(float)4.0
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(assetScaleToFitRatio,assetScaleToFitRatio);
//            [currentAssetLayerInstruction setTransform:
//             CGAffineTransformConcat(CGAffineTransformConcat(currentAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(translateAxisX, 0)) atTime:duration];
//        }
//
//    //duration=CMTimeAdd(duration, currentAsset.duration);
//        
//    [currentAssetLayerInstruction setOpacity:0.0 atTime:currentAsset.duration];
//    [arrayInstruction addObject:currentAssetLayerInstruction];
//    
//    videoCompositionInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, duration);
//    videoCompositionInstruction.layerInstructions = arrayInstruction;
//    
//    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
//    videoComposition.instructions = [NSArray arrayWithObject:videoCompositionInstruction];
//    videoComposition.frameDuration = CMTimeMake(1, 30);
//    videoComposition.renderSize = CGSizeMake(MAX_WIDTH, MAX_HEIGHT);
//    
//    NSString *myPathDocs =  [[self applicationCacheDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo%-dtemp.mp4",arc4random() % 10000]];
//    
//    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
//    myLog(@"Path: %@", myPathDocs);
//    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
//    exporter.outputURL = url;
//    exporter.outputFileType = AVFileTypeMPEG4;
//    exporter.videoComposition = videoComposition;
//    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^ {
        //if (flag) {
        //}
        switch (exporter.status) {
            case AVAssetExportSessionStatusCompleted: {
                NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                VideoPlayViewController *videoController = [storyBoard instantiateViewControllerWithIdentifier:kVideoPlayViewController];
                videoController.URLVideo = url;
                videoController.arrScriptsClip = nil;
                videoController.isExportRecord = YES;
                [delegate.window.rootViewController presentViewController:videoController animated:YES completion:nil];
                //callback
                if (completion) {
                    completion(YES, url);
                }
            }
                break;
            case AVAssetExportSessionStatusFailed:
                myLog(@"Failed:%@", exporter.error.description);
                break;
            case AVAssetExportSessionStatusCancelled: {
                myLog(@"Canceled:%@", exporter.error);
                //callback
                if (completion) {
                    completion(NO, nil);
                }
            }
                break;
            case AVAssetExportSessionStatusExporting:
                myLog(@"Exporting!");
                break;
            case AVAssetExportSessionStatusWaiting:
                myLog(@"Waiting");
                break;
            default:
                break;
        }
    }];
}

//save clip to Photos
+(void)saveClipsFileToPhotos: (NSURL*)fileURL {
    [self saveClipsFileToPhotos:fileURL alertShow:YES completion:^(BOOL success, NSURL *assetURL) {
        
    }];
}
//save clip to Photos before editing
+(void)saveEditClipsFileToPhotos: (NSURL*)fileURL {
    [self saveClipsFileToPhotos:fileURL alertShow:NO completion:^(BOOL success, NSURL *assetURL) {
        myLog(@"%@",assetURL);
        if (success) {
            // [MakeViewController startEditClips];
        }
    }];
}

//get Cache Directory
+(NSString *)applicationCacheDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

//calculate the scale correctly.
+(CGFloat)getScaleToFitRatio:(NSMutableDictionary*)makeInfo currentTrack:(AVMutableCompositionTrack*)currentTrack{
    CGFloat assetScaleToFitRatio = 1.0/1.0;
    
    if ([makeInfo getMakeClipsIsLocal]) {
        if (currentTrack.naturalSize.width >= 1280 || currentTrack.naturalSize.width >= 1920 || currentTrack.naturalSize.width >= 3840 || currentTrack.naturalSize.height >= 720 || currentTrack.naturalSize.height >= 1080 || currentTrack.naturalSize.height >= 2160) {
            assetScaleToFitRatio = 0.7/1.0;
        }else if (currentTrack.naturalSize.width < MAX_WIDTH) {
            assetScaleToFitRatio = 1.35/1.0;
        }else {
            assetScaleToFitRatio = 1.0/1.0;
        }
    }
    return assetScaleToFitRatio;
}

+(CGFloat)getScaleToFitRatioCurrentTrack:(AVMutableCompositionTrack*)currentTrack{
    CGFloat assetScaleToFitRatio = 1.0/1.0;
    
    if (currentTrack.naturalSize.width >= 1280 || currentTrack.naturalSize.width >= 1920 || currentTrack.naturalSize.width >= 3840 || currentTrack.naturalSize.height >= 720 || currentTrack.naturalSize.height >= 1080 || currentTrack.naturalSize.height >= 2160) {
        assetScaleToFitRatio = 0.7/1.0;
    }else if (currentTrack.naturalSize.width < MAX_WIDTH) {
        assetScaleToFitRatio = 1.35/1.0;
    }else {
        assetScaleToFitRatio = 1.0/1.0;
    }
    return assetScaleToFitRatio;
}


#pragma mark - Pick item from Photos
+ (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.allowsEditing = YES;
    mediaUI.videoMaximumDuration = 10.0f;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    mediaUI.videoQuality = UIImagePickerControllerQualityTypeIFrame960x540;
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:YES completion:nil];
    return YES;
}

#pragma mark - Record Camera Video
+ (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    cameraUI.allowsEditing = YES;
    cameraUI.delegate = delegate;
    cameraUI.videoMaximumDuration = 10.0f;
    cameraUI.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
    cameraUI.videoQuality = UIImagePickerControllerQualityTypeIFrame960x540;
    cameraUI.modalPresentationStyle = UIModalPresentationFullScreen;
    [controller presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}

#pragma mark - FB
+(void)shareToMessengerWithURL:(NSURL*)fileURL {
    if ([FBSDKMessengerSharer messengerPlatformCapabilities] & FBSDKMessengerPlatformCapabilityVideo) {
        
        FBSDKMessengerShareOptions *options = [[FBSDKMessengerShareOptions alloc] init];
        
        options.contextOverride = [self getContextForShareMode];
        
        NSData *videoData = [NSData dataWithContentsOfURL:fileURL];
        [FBSDKMessengerSharer shareVideo:videoData withOptions:options];
    } else {
        
        // Messenger isn't installed. Redirect the person to the App Store.
        NSString *appStoreLink = @"https:// itunes.apple.com/us/app/facebook-messenger/id454638411?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
    }
}

+ (FBSDKMessengerContext *) getContextForShareMode {
    
    if (messengerShareMode == MessengerShareModeSend) {
        // Force a send flow by returning a broadcast context.
        return [[FBSDKMessengerBroadcastContext alloc] init];
        
    } else if (messengerShareMode == MessengerShareModeComposer) {
        // Force the composer flow by returning the composer context.
        return [[FBSDKMessengerURLHandlerReplyContext alloc] init];
        
    } else if (messengerShareMode == MessengerShareModeReply) {
        // Force the reply flow by returning the reply context.
        return [[FBSDKMessengerURLHandlerReplyContext alloc] init];
    }
    
    
    return nil;
}

+ (void) sharetoFBWithURL:(NSURL *)fileURL viewController:(UIViewController*)viewController{
    
    [self saveClipsFileToPhotos:fileURL alertShow:NO completion:^(BOOL success, NSURL *assetURL) {
        if (success) {
            FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
            video.videoURL = assetURL;
            FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
            content.video = video;
            //    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            [FBSDKShareDialog showFromViewController:viewController
                                         withContent:content
                                            delegate:nil];
        }
    }];
}

+(void)saveClipsFileToPhotos: (NSURL*)fileURL alertShow:(BOOL)alertShow completion:(void(^)(BOOL success, NSURL *assetURL))completion {
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    
    if (status != ALAuthorizationStatusAuthorized) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Cannot access Photo library. Please go to setting > find app Chat=Phim > turn on Photo :)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 40)];
        //        UIImage *alertImg = [UIImage imageNamed:@"alertImg"];
        //        [imageView setImage:alertImg];
        //        [alert setValue:imageView forKey:@"accessoryView"];
        //        [alert show];
        //        completion(NO, nil);
        
        ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
        ALAssetsLibraryGroupsEnumerationResultsBlock assetGroupEnumerator =
        ^(ALAssetsGroup *assetGroup, BOOL *stop) {
            if (assetGroup != nil) {
                // do somthing
            }
        };
        
        ALAssetsLibraryAccessFailureBlock assetFailureBlock = ^(NSError *error) {
            myLog(@"Error enumerating photos: %@",[error description]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kErrorMessageNotUseAlbum") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 40)];
            UIImage *alertImg = [UIImage imageNamed:@"alertImg"];
            [imageView setImage:alertImg];
            [alert setValue:imageView forKey:@"accessoryView"];
            [alert show];
            completion(NO, nil);
        };
        
        NSUInteger groupTypes = ALAssetsGroupAll;
        
        [library enumerateGroupsWithTypes:groupTypes usingBlock:assetGroupEnumerator failureBlock:assetFailureBlock];
        
    }else {
        
        __block AppDelegate *delegate = [UIApplication sharedApplication].delegate;
       //UIView *videoVCview = [delegate.window.rootViewController.childViewControllers objectAtIndex:1].view;
        __block UIView *rootView = delegate.window.rootViewController.presentedViewController.view;
        //[MBProgressHUD hideAllHUDsForView:rootView animated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
        hud.labelText = myLanguage(@"kSaveClip");
        hud.minShowTime =1.0;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:fileURL]) {
            
            ALAssetsLibrary* library = [[ALAssetsLibrary alloc]init];
            [library writeVideoAtPathToSavedPhotosAlbum:fileURL completionBlock:^(NSURL *assetURL, NSError *error) {
                
                myLog(@"ASSET URL %@",assetURL);
                if (error) {
                    if (alertShow) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kSaveClipError") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        });
                        
                    }
                    completion (NO, nil);
                }else {
                    if (alertShow) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //[MBProgressHUD hideAllHUDsForView:rootView animated:YES];
                            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
                            hud.labelText = myLanguage(@"kSaveClipSuccess");
                            hud.minShowTime =1;
                            [MBProgressHUD hideAllHUDsForView:rootView animated:YES];
//                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kSaveClipSuccess") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                            [alert show];
                        });
                    }
                    myLog(@"VIDEO SAVED");
                    completion(YES, assetURL);
                }
                
            }];
        }
        [MBProgressHUD hideAllHUDsForView:rootView animated:YES];
    }
}

+(BOOL)checkHasExistentMakeClips:(NSArray*)arrMakeDatas {
    
    NSMutableArray *temps = [NSMutableArray arrayWithCapacity:[arrMakeDatas count]];
    for (NSMutableDictionary *dict in arrMakeDatas) {
        if ([dict getURLPath]) {
            [temps addObject:dict];
        }
    }
    if (![temps count]) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kPlayAllNoVideo") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
        return NO;
    }
    return YES;
}
@end
