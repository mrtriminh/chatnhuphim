//
//  NSMutableDictionary+VideoClips_Utils.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (VideoClips_Utils)

+(id)videoClipsDictWithVideoName:(id)videoName videoURL:(id)videoURL thumbnailURL:(id)thumbnailURL;

-(void)setVideoName:(id)videoName;

-(id)getVideoName;

-(void)setVideoURL:(id)videoURL;

-(id)getVideoURL;

-(void)setVideoThumbnailURL:(id)thumbnailURL;

-(id)getVideoThumbnailURL;

@end
