//
//  AdsEntity.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/13/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdsEntity : NSObject
@property (assign, nonatomic) NSInteger beginAds;
@property (assign, nonatomic) NSInteger endAds;
@property (assign, nonatomic) NSInteger positionAds;
@end
