//
//  ClipsBusinessManager.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/10/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIManager.h"

@interface ClipsBusinessManager : NSObject

//Category for clips
+(void)getAllCategoryComplete:(NetworkResponse)completion;

+(void)getClipsWithCategoryName:(NSString*)categoryName completion:(NetworkResponse)completion;

//Search a clips
+(void)searchClipsWithKeyword:(NSString*)keyWord genderVoice:(GenderTypeIcon)genderType comletion:(NetworkResponse)completion;

//Favorite
+(void)getFavoriteClipsWithUUID:(NSString*)UUID completion:(NetworkResponse)completion;

+(void)addClipToFavoriteWithUUID:(NSString*)UUID urlVideo:(NSString*)URLVideo completion:(NetworkResponse)completion;

+(void)deleteClipToFavoriteWithUUID:(NSString*)UUID urlVideo:(NSString*)URLVideo completion:(NetworkResponse)completion;

+(void)getAllSequences:(id)params completion:(NetworkResponse)completion;

+(void)listSequencesSuggestion:(id)param completion:(NetworkResponse)completion;

+(void)searchClipsWithKeyword:(NSString*)keyWord categoryName:(NSString*)categoryName comletion:(NetworkResponse)completion;
@end
