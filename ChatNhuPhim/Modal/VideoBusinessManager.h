//
//  VideoBusinessManager.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "APIManager.h"

@interface VideoBusinessManager : NSObject

+(instancetype)manager;

-(void)getListRandomsWithParams:(id)params completion:(NetworkResponse)completion;

-(void)downloadVideoClips:(NSURL*)urlRequest informationDownload:(id)informationDownload isFromClips:(BOOL)flag;
@end
