//
//  NSMutableDictionary+VideoClips_Utils.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "NSMutableDictionary+VideoClips_Utils.h"

@implementation NSMutableDictionary (VideoClips_Utils)
+(id)videoClipsDictWithVideoName:(id)videoName videoURL:(id)videoURL thumbnailURL:(id)thumbnailURL{
    if (videoName == nil) {
        videoName = [NSNull new];
    }
    return [NSMutableDictionary dictionaryWithObjects:@[videoName, videoURL, thumbnailURL] forKeys:@[kDictVideoName, kDictVideoURL, kDictThumbnailURL]];
}

-(void)setVideoName:(id)videoName{
    if (videoName == nil) {
        videoName = [NSNull new];
    }
    [self setValue:videoName forKey:kDictVideoName];
}

-(id)getVideoName{
    return [self valueForKey:kDictVideoName];
}

-(void)setVideoThumbnailURL:(id)thumbnailURL{
    if (thumbnailURL == nil) {
        thumbnailURL = [NSNull new];
    }
    [self setValue:thumbnailURL forKey:kDictThumbnailURL];
}

-(id)getVideoThumbnailURL{
    return [self valueForKey:kDictThumbnailURL];
}

-(void)setVideoURL:(id)videoURL{
    if (videoURL == nil) {
        videoURL = [NSNull new];
    }
    [self setValue:videoURL forKey:kDictVideoURL];
}

-(id)getVideoURL{
    return [self valueForKey:kDictVideoURL];
}

@end
