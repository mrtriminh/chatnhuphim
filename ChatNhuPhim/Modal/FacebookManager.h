//
//  FacebookManager.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface FacebookManager : NSObject<FBSDKGraphRequestConnectionDelegate>

+(instancetype)sharedInstance;


@property(nonatomic, strong)    FBSDKLoginManager       *loginManager;
@property(nonatomic, strong)    NSMutableDictionary     *fbInfo;
@property (nonatomic, strong)   NSString *avartaURL;

-(NSString*)facebook_email;

-(NSString*)facebook_id;

-(NSString*)fullname;

-(void)logInWithReadPermissions:(NSArray *)permissions viewController:(id)viewController completion:(void(^)(BOOL success, NSString*error))completion;

@end
