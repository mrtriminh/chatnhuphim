//
//  NSMutableDictionary+Make_Utils.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/3/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Make_Utils)

+(id)makeClipsDictionaryWithNumber:(id)number;

-(void)setANumber:(id)number;

-(NSNumber*)getANumber;

-(void)setThumbnailImage:(id)thumbnailImage;

-(id)getThumbnailImage;

-(void)setURLPath:(NSURL*)aURLPath;

-(id)getURLPath;

-(void)setMakeClipIsLocal:(BOOL)isLocal;

-(BOOL)getMakeClipsIsLocal;

-(void)setMakeClipThumbnailURL:(id)thumbnailURL;

-(id)getMakeClipsThumbnailURL;

-(void)setMakeClipVideoURL:(id)videoURL;

-(id)getMakeClipVideoURL;

-(void)setTotalBytesRead:(long long)totalBytesRead;

-(long long)getTotalBytesRead;

-(void)setTotalBytesExpectedToRead:(long long)totalBytesExpectedToRead;

-(long long)getTotalBytesExpectedToRead;

-(void)setDownloadIsCompleted:(BOOL)isCompleted;

-(BOOL)getDownloadIsCompleted;

@end
