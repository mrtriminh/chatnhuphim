//
//  ScriptEntity.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/28/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScriptEntity : NSObject
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSURL *thumbnailURL;
@property (copy, nonatomic) NSURL *videoURL;
@property (assign, nonatomic) NSInteger ID;

@end
