//
//  MakeClipsBusinessManager.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
@import UIKit;
@import AssetsLibrary;
@import AVFoundation;
@import MobileCoreServices;
@import QuartzCore;


@interface MakeClipsBusinessManager : NSObject

+(void)mergerAndSaveClipsToPhoto:(NSArray*)arrMakeDatas presentPlayBackController:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion;

+(void)mergerAndSaveClipsToPhoto:(NSArray*)arrMakeDatas presentPlayBackController:(BOOL)flag completion:(void(^)(BOOL success, NSURL *fileURL))completion;

+(void)mergerUserClipWithAudio:(NSArray*)arrMakeDatas audioURL:(NSURL*)audioFile showHUD:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion;

+(void)mergerUserAudio:(NSURL*)audioFile withClip:(NSURL*)videoFile showHUD:(BOOL)flag isFromScriptClips:(BOOL)isFromScriptClips completion:(void(^)(BOOL success, NSURL *fileURL))completion;

+ (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller usingDelegate: (id <UIImagePickerControllerDelegate,
                                                UINavigationControllerDelegate>) delegate;
+ (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate;
+(void)saveClipsFileToPhotos: (NSURL*)fileURL;

+(void)saveEditClipsFileToPhotos: (NSURL*)fileURL;

+(void)saveClipsFileToPhotos: (NSURL*)fileURL alertShow:(BOOL)alertShow completion:(void(^)(BOOL success, NSURL *assetURL))completion;

+(void)shareToMessengerWithURL:(NSURL*)fileURL;

+(void)sharetoFBWithURL:(NSURL *)fileURL viewController:(UIViewController*)viewController;

+(BOOL)checkHasExistentMakeClips:(NSArray*)arrMakeDatas;
@end
