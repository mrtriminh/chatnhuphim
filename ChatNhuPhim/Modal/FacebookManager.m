
//
//  FacebookManager.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//
#import "FacebookManager.h"
#import "Constants.h"
#import "NSUserDefaults+Utils.h"

@implementation FacebookManager

+(instancetype)sharedInstance
{
    static dispatch_once_t  predecate;
    static FacebookManager* obj;
    
    dispatch_once(&predecate, ^(){
        obj = [[FacebookManager alloc] init];
    });
    
    return obj;
}

- (FBSDKLoginManager *)loginManager
{
    if (_loginManager) {
        return _loginManager;
    }
    _loginManager = [[FBSDKLoginManager alloc] init];
    
    return _loginManager;
}

- (NSMutableDictionary *)fbInfo
{
    if (!_fbInfo) {
        _fbInfo = [[NSMutableDictionary alloc] init];
    }
    
    return _fbInfo;
}


-(void)logInWithReadPermissions:(NSArray *)permissions viewController:(id)viewController completion:(void(^)(BOOL success, NSString* error))completion{
    __weak FBSDKLoginManager *login = self.loginManager;
    
//    [login logInWithReadPermissions:permissions fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//        if (error) {
//            // Process error
//            completion(NO, error.description);
//            
//        } else if (result.isCancelled) {
//            // Handle cancellations
//            completion(NO, nil);
//        } else {
//            // If you ask for multiple permissions at once, you
//            // should check if specific permissions missing
//            if ([result.grantedPermissions containsObject:@"email"]) {
//                myLog(@"Granted Permission: %@", result.grantedPermissions);
//                // Do work
//                [self.fbInfo removeAllObjects];
//                [self updateProfilePictureUsingBlock:^(NSError *error) {
//                    if (error) {
//                        completion(NO, error.description);
//                    }else{
//                        completion(YES, nil);
//                    }
//                }];
//            }
//        }
//    }];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            completion(NO, error.description);
        } else if (result.isCancelled) {
            // Handle cancellations
            completion(NO, nil);
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                myLog(@"Granted Permission: %@", result.grantedPermissions);
                // Do work
                //[self.fbInfo removeAllObjects];
                [self updateProfilePictureUsingBlock:^(NSError *error) {
                    if (error) {
                        completion(NO, error.description);
                    }else{
                        completion(YES, nil);
                    }
                }];
            }
        }

    }];
}

- (void)updateProfilePictureUsingBlock:(void(^)(NSError * error)) callback
{
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email,id,name"}]
        startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary * result, NSError *error) {
            if (!error) {
                myLog(@"Face book info: %@", result);
                if (result[@"email"]) {
                    //Using ID of FB to make an identifier for favorite
                    [NSUserDefaults saveDeviceToken:result[@"email"]];
                    callback(nil);
                }else {
                    callback(error);
                }
            }
        }];
    }
}

#pragma mark - Facebook Parsing

-(NSString*)facebook_email{
    return [self.fbInfo valueForKey:@"email"];
}

-(NSString*)facebook_id{
    return [self.fbInfo valueForKey:@"facebook_id"];
}

-(NSString*)fullname{
    return [self.fbInfo valueForKey:@"fullname"];
}

- (NSString *)serializeArrayToJsonString:(NSMutableArray*)array
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}
@end
