//
//  RecordEntity.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/9/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordEntity : NSObject
@property (strong, nonatomic) NSURL *recordURL;
@property (strong, nonatomic) NSDate *recordBegin;
@property (strong, nonatomic) NSDate *recordEnd;
@end
