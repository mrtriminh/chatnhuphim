//
//  NSMutableDictionary+Make_Utils.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/3/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "NSMutableDictionary+Make_Utils.h"

@implementation NSMutableDictionary (Make_Utils)
+(id)makeClipsDictionaryWithNumber:(id)number {
    if (!number) {
        number = @-1;
    }
    return [NSMutableDictionary dictionaryWithObjects:@[number, @(YES)] forKeys:@[kDictData, kDictIsLocal]];
}

-(void)setANumber:(id)number {
    [self setValue:number forKey:kDictData];
}

-(NSNumber*)getANumber {
    return [self valueForKey:kDictData];
}

-(void)setData:(id)data {
    [self setValue:data forKey:kDictData];
}

-(NSString*)getFileName {
    return [self valueForKey:kDictFileName];
}

-(void)setFileName:(NSString*)fileName {
    [self setValue:fileName forKey:kDictFileName];
}

-(void)setThumbnailImage:(id)thumbnailImage {
    [self setValue:thumbnailImage forKey:kDictThumbnailImage];
}

-(id)getThumbnailImage {
    return [self valueForKey:kDictThumbnailImage];
}

-(void)setURLPath:(NSURL *)aURLPath {
    [self setValue:aURLPath forKey:kDictURLPath];
}

-(id)getURLPath {
    return [self valueForKey:kDictURLPath];
}

-(void)setMakeClipIsLocal:(BOOL)isLocal {
    [self setValue:@(isLocal) forKey:kDictIsLocal];
}

-(BOOL)getMakeClipsIsLocal {
    return [[self valueForKey:kDictIsLocal] isEqual:@1]?YES:NO;
}

-(void)setMakeClipThumbnailURL:(id)thumbnailURL {
    [self setValue:thumbnailURL forKey:kDictThumbnailURL];
}

-(id)getMakeClipsThumbnailURL{
    return [self valueForKey:kDictThumbnailURL];
}

-(void)setMakeClipVideoURL:(id)videoURL{
    if (videoURL == nil) {
        videoURL = [NSNull new];
    }
    [self setValue:[videoURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:kDictVideoURL];
}

-(id)getMakeClipVideoURL{
    return [self valueForKey:kDictVideoURL];
}

-(void)setTotalBytesRead:(long long)totalBytesRead{
    [self setValue:[NSNumber numberWithLongLong:totalBytesRead] forKey:kDictTotalBytesRead];
}

-(long long)getTotalBytesRead{
    return [[self valueForKey:kDictTotalBytesRead] longLongValue];
}

-(void)setTotalBytesExpectedToRead:(long long)totalBytesExpectedToRead{
    [self setValue:[NSNumber numberWithLongLong:totalBytesExpectedToRead] forKey:kDictTotalBytesExpectedToRead];
}

-(long long)getTotalBytesExpectedToRead{
    return [[self valueForKey:kDictTotalBytesExpectedToRead] longLongValue];
}

-(void)setDownloadIsCompleted:(BOOL)isCompleted{
    [self setValue:@(isCompleted) forKey:kDictDownloadIsCompleted];
}

-(BOOL)getDownloadIsCompleted{
    return [[self valueForKey:kDictDownloadIsCompleted] isEqual:@1]?YES:NO;
}
@end
