//
//  VideoBusinessManager.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/6/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "VideoBusinessManager.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "AppDelegate.h"

@implementation VideoBusinessManager
+(instancetype)manager {
    static VideoBusinessManager *obj = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        obj = [self alloc];
    });
    return obj;
}

-(void)getListRandomsWithParams:(id)params completion:(NetworkResponse)completion {

    [[APIManager shared] methodGETdataWithParams:params apiName:kAPIRandom completion:^(BOOL success, id data, NSString *error) {
        
        if (success) {
            if ([data isKindOfClass:[NSArray class]]) {
                id info = [data firstObject];
                completion(YES, [NSMutableDictionary videoClipsDictWithVideoName:[info valueForKey:kDictVideoName] videoURL:[info valueForKey:kDictVideoURL] thumbnailURL:[info valueForKey:kDictThumbnailURL]], nil);
            }
            else {
                myLog(@"This is a Dictionary:%@", data);
                completion(YES, nil, nil);
            }
        }else {
            completion(NO, nil, nil);
        }
    }];
}

-(void)downloadVideoClips:(NSURL*)urlRequest informationDownload:(id)informationDownload isFromClips:(BOOL)flag {
    [[APIManager shared] downloadFileWithURL:urlRequest informationDownload:informationDownload isFromClips:flag];
}
@end
