// 
// ViewController.m
// ChatNhuPhim
// 
// Created by Tri Minh on 6/2/15.
// Copyright (c) 2015 Tri Minh. All rights reserved.
// 

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Reachability.h"
#import "SettingViewController.h"
#import "HotCell.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@import CoreGraphics;

@interface ViewController ()
- (IBAction)play:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *inputTxt;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UIView *Notification;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UILabel *NotiMessage;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *remkBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareMesBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareFbBtn;
@property (nonatomic, strong) AVPlayer *player;
@property (weak, nonatomic) IBOutlet UIView *videolayer;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *bgLogo;
@property (weak, nonatomic) IBOutlet UIButton *tipBtn;
@property (assign) BOOL keyboardIsShown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardBottom;
@property (nonatomic, strong) NSURLSession *session;

@property (weak, nonatomic) IBOutlet UIView *hotPhrase;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTop;
@property (weak, nonatomic) IBOutlet UITextView *spam;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *tableImage;
@property (weak, nonatomic) IBOutlet UIButton *voiceBtn;
@property (weak, nonatomic) IBOutlet UIButton *creatBtn;

@property (nonatomic, retain) FBSDKMessengerURLHandlerReplyContext *replyContext;
@property (nonatomic, retain) FBSDKMessengerURLHandlerOpenFromComposerContext *composerContext;

@end
@implementation ViewController

@synthesize voice;

- (void)viewDidLoad {
   
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    voice = @"";
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/voice-tuy.png"];
    UIImage *voiceBG = [UIImage imageWithContentsOfFile:path];
    [_voiceBtn setBackgroundImage:voiceBG forState:UIControlStateNormal];
    _saved = YES;
    _loaded = NO;
    self.bgLogo.hidden = NO;
    [self.view bringSubviewToFront:_Notification];
    
    NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/hot-bg.png"];
    [_bgImage setImage:[UIImage imageWithContentsOfFile:fullpath]];
    
    NSString *fullpath2 = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/hot-title.png"];
    [_tableImage setImage:[UIImage imageWithContentsOfFile:fullpath2]];
    
    // Check internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityDidChange:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    self.inputTxt.delegate = self;

    _playBtn.hidden = YES;
    _remkBtn.hidden = YES;
    _thumbnail.hidden = NO;
    
    // tap to dismiss keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    
    [self.view addGestureRecognizer:tap];
    
    // tap to stop video
    UITapGestureRecognizer *tapstop = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(stopVideo)];
    [tapstop setCancelsTouchesInView:NO];
    
    [self.videoView addGestureRecognizer:tapstop];
    
    // register for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    self.keyboardIsShown = NO;
    
    // add fb button
    [_shareMesBtn addTarget:self action:@selector(shareToMess:) forControlEvents:UIControlEventTouchUpInside];
    [_shareFbBtn addTarget:self action:@selector(sharetoFB) forControlEvents:UIControlEventTouchUpInside];

    // config HotTable
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    self.tableViewTop.constant = -650;
    [self updateTrend];
    
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [_inputTxt resignFirstResponder];
}

-(void)updateTrend {
    // download trend
    NSString *urlString = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=trend"];
    NSURL *urlTrend = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlTrend];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             NSMutableArray *testFeeds = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:NSJSONReadingMutableContainers
                                                                           error:nil];
             trend = [testFeeds copy];
             [[self tableView] performSelectorOnMainThread:@selector(reloadData)
                                                withObject:nil
                                             waitUntilDone:NO];
             NSLog(@"array: %@", trend);
         }
     }];
}

// Check internet connection method
- (void)reachabilityDidChange:(NSNotification *)notification {
//    Reachability *reachability = (Reachability *)[notification object];
//    
//    if ([reachability isReachable]) {
//        [self.inputTxt setEnabled:YES];
//        [self.NotiMessage setText:@"Đã kết nối Internet"];
//        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/ /*#99ff99*/];
//        self.NotiMessage.hidden = NO;
//        [UIView animateWithDuration:2 animations:^() {
//            _NotiMessage.alpha = 0.0;
//        }];
//        [self.view bringSubviewToFront:_Notification];
//        [self.view bringSubviewToFront:_panelView];
//        for (CALayer *layer in self.videolayer.layer.sublayers) {
//            [layer removeFromSuperlayer];
//        }
//        NSLog(@"Reachable");
//        [self.view sendSubviewToBack:_Notification];
//    
//    } else {
//        [self.inputTxt setEnabled:NO];
//        // show noti
//        int i = arc4random() % 2;
//        NSLog(@"%d",i);
//        if (i == 0) {
//            NSBundle *bundle = [NSBundle mainBundle];
//            NSString *moviePath = [bundle pathForResource:@"disconnect-1" ofType:@"mov"];
//            NSURL *movie = [NSURL fileURLWithPath:moviePath];
//            AVPlayerItem *playDisconnect = [AVPlayerItem playerItemWithURL:movie];
//            _player = [[AVPlayer alloc] initWithPlayerItem:playDisconnect];
//        } else {
//            NSBundle *bundle = [NSBundle mainBundle];
//            NSString *moviePath = [bundle pathForResource:@"disconnect-2" ofType:@"mov"];
//            NSURL *movie = [NSURL fileURLWithPath:moviePath];
//            AVPlayerItem *playDisconnect = [AVPlayerItem playerItemWithURL:movie];
//            _player = [[AVPlayer alloc] initWithPlayerItem:playDisconnect];
//        }
//        
//        // AVPlayer *player = [AVPlayer playerWithURL:movieURL]; // 
//        // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
//        // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(replayDisconnect:) name:AVPlayerItemDidPlayToEndTimeNotification object:playDisconnect];
//        // Pass the AVPlayerItem to a new player
//        
//        AVPlayerLayer *layer = [AVPlayerLayer layer];
//        
//        [layer setPlayer:_player];
//        [layer setFrame:_videolayer.bounds/*CGRectMake(0, 60, 320, 320)*/];
//        [layer setBackgroundColor:[UIColor clearColor].CGColor];
//        [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
//        
//        [self.view sendSubviewToBack:_videoView];
//        // [self.view bringSubviewToFront:_videolayer];
//        // [self.view bringSubviewToFront:_Notification];
//        [self.videolayer.layer addSublayer:layer];
//        
//        // [self.view bringSubviewToFront:_videoView];
//        // [self.view bringSubviewToFront:_inputView];
//        // [self.inputTxt setEnabled:NO];
//        _playBtn.hidden = NO;
//        _remkBtn.hidden = YES;
//        [_player play];
//    
//        // [self.view bringSubviewToFront:_Notification];
//        [self.NotiMessage setText:@"Vui lòng kết nối Internet"];
//        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:1 green:0.914 blue:0.694 alpha:1] /*yellow*/];
//        self.NotiMessage.hidden = NO;
//        [UIView animateWithDuration:2 animations:^() {
//            _NotiMessage.alpha = 1.0;
//        }];
//        
//        NSLog(@"Unreachable");
//    }
}

-(IBAction)Save:(id)sender
{
    // NSString *moviePath = [[NSUserDefaults standardUserDefaults]
                          // stringForKey:@"movie"];
    // NSURL *movieURL = [NSURL URLWithString:moviePath];
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    
    if (status != ALAuthorizationStatusAuthorized) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lưu ý tí xíu" message:@"Bạn vui lòng vào Cài đặt/Settings. \nKéo xuống dưới tìm app Chat=Phim, \nVà cho phép app lưu vào album nhé" delegate:nil cancelButtonTitle:@"Ok con dê" otherButtonTitles:nil, nil];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 40)];
        NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/alertImg.png"];
        UIImage *alertImg = [UIImage imageWithContentsOfFile:fullpath];
        [imageView setImage:alertImg];
        
        // [alert addSubview:imageView];
        [alert setValue:imageView forKey:@"accessoryView"];
        [alert show];
    } else {
    if ((_saved == NO) && (_loaded =YES)) {
        _saved = YES;
        [self saveToCameraRoll:movieURL];
        // noti saved
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Đã lưu vào máy"];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
        [UIView animateWithDuration:2 animations:^() {
            _NotiMessage.alpha = 0.0;
        }];
    } else {
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Nãy mới Lưu rồi mà"];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
        [UIView animateWithDuration:2 animations:^() {
            _NotiMessage.alpha = 0.0;
        }];
    }
    }
}

- (void) saveToCameraRoll:(NSURL *)moviesaveURL
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    /*ALAssetsLibraryWriteVideoCompletionBlock videoWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image with metadata to Photo Library %@", newURL.absoluteString);
            assetURL = [NSURL URLWithString:newURL.absoluteString];
            NSLog(@"shareURL: %@", assetURL);
        }
    };*/
    
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:movieURL])
    {
        [library writeVideoAtPathToSavedPhotosAlbum:movieURL completionBlock:^(NSURL *assetAlbumURL, NSError *error) {
            if (error) {
                NSLog(@"error while saving video");
            } else{
                [self addAssetURL:assetAlbumURL toAlbum:@"Chat Như Phim"];
            }
        }];
    }
}

- (void)addAssetURL:(NSURL*)assetAlbumURL toAlbum:(NSString*)albumName
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
            // If album found
            [library assetForURL:assetAlbumURL resultBlock:^(ALAsset *asset) {
                // add asset to album
                [group addAsset:asset];
            } failureBlock:nil];
        }
        else {
            // if album not found create an album
            [library addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group)     {
                [self addAssetURL:assetAlbumURL toAlbum:albumName];
            } failureBlock:nil];
        }
    } failureBlock: nil];
}

// select voice male/female
-(IBAction)selectVoice:(id)sender{
    // NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([voice  isEqual: @"male"]) {
        NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/voice-nu.png"];
        UIImage *voiceBG = [UIImage imageWithContentsOfFile:fullpath];
        [_voiceBtn setBackgroundImage:voiceBG forState:UIControlStateNormal];
        voice = [NSString stringWithFormat:@"female"];
    } else if ([voice  isEqual: @"female"]) {
        NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/voice-tuy.png"];
        UIImage *voiceBG = [UIImage imageWithContentsOfFile:fullpath];
        [_voiceBtn setBackgroundImage:voiceBG forState:UIControlStateNormal];
        voice = [NSString stringWithFormat:@""];
    } else {
        NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/voice-nam.png"];
        UIImage *voiceBG = [UIImage imageWithContentsOfFile:fullpath];
        [_voiceBtn setBackgroundImage:voiceBG forState:UIControlStateNormal];
        voice = [NSString stringWithFormat:@"male"];
    }
    NSLog(@"%@",voice);
}

// Character filter
-(void)chaFilter:(NSString*)string
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    NSCharacterSet *notAllowedChars = [NSCharacterSet characterSetWithCharactersInString:@".,/?!+{}£$%&*"];
    
    // return [_inputTxt stringByReplacingStringsFromDictionary:replacements];
    NSString *spaceTxt = [[[string componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@" "] lowercaseString];
    
    prepareTxt = [[regex stringByReplacingMatchesInString:spaceTxt options:0 range:NSMakeRange(0, [spaceTxt length]) withTemplate:@" "] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // stringByReplacingOccurrencesOfString:@"  " withString:@" "]
    // NSLog (@"Result: %@", prepareTxt);
}
-(IBAction)creat:(id)sender
{
    [_inputTxt resignFirstResponder];
    _remkBtn.hidden = YES;
    _playBtn.hidden = YES;
    [_inputTxt setEnabled:NO];
    _loaded = NO;
    
    [self.view bringSubviewToFront:_Notification];
    [self.NotiMessage setText:@"Tuyển diễn viên..."];
    [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
    self.NotiMessage.hidden = NO;
    _NotiMessage.alpha = 1.0;
    // prepare request link
    // NSString* prepareTxt = [[_inputTxt.text stringByReplacingOccurrencesOfString:@"  " withString:@" "]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [self chaFilter:_inputTxt.text];
    NSLog (@"Result: %@", prepareTxt);
    
    words = [[prepareTxt componentsSeparatedByString:@" "] count];
    NSLog(@"%lu",(unsigned long)words);
    
    // voice = [[NSUserDefaults standardUserDefaults]
    // stringForKey:@"selectedVoice"];
    if (words<16) {
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                       {
                           // Background work
                           NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=make&text=%@&voice=%@",prepareTxt,voice];
                           NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                           NSLog(@"%@",url);
                           NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
                           [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
                           [serviceRequest setHTTPMethod:@"GET"];
                           
                           NSURLResponse *response;
                           NSError *error;
                           NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
                           
                           // download in background thread to allow indicator
                           strData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                           NSLog(@"stringURL %@",strData);
                           saveURL = [NSURL URLWithString:strData];
                           NSLog(@"send url %@",saveURL);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^(void)
                                          {
                                              // Main thread work (UI usually)
                                              NSURLRequest *request = [NSURLRequest requestWithURL:saveURL];
                                              NSURLConnection *movieDownload = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                              
                                              if (movieDownload) {
                                                  
                                                  receivedMovie=[NSMutableData data];
                                                  movieURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                                                  movieURL = [movieURL URLByAppendingPathComponent:@"movie.mp4"];
                                                  
                                                  // save the file
                                                  NSString *moviePath = [NSString stringWithFormat:@"%@",movieURL];
                                                  [[NSUserDefaults standardUserDefaults] setObject:moviePath forKey:@"movie"];
                                                  [[NSUserDefaults standardUserDefaults] synchronize];
                                              } else {
                                                  // inform the user that the movie download could not be made
                                                  NSLog(@"failed to get movie");
                                              }
                                          });
                       });
        
    } else {
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Hiện tại chỉ được 15 từ thôi ;)"];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:1 green:0.914 blue:0.694 alpha:1] /*Yellow #ffe9b1*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
        [UIView animateWithDuration:2 animations:^() {
            _NotiMessage.alpha = 0.0;
        }];
        [_inputTxt setEnabled:YES];
        prepareTxt = [NSString stringWithFormat:@""];
        NSLog(@"Nhieu chu qua");
    }
}
// input video name and play with return key
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.inputTxt) {
        [textField resignFirstResponder];
        _remkBtn.hidden = YES;
        _playBtn.hidden = YES;
        [_inputTxt setEnabled:NO];
        _loaded = NO;
        
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Tuyển diễn viên..."];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
       // prepare request link
       // NSString* prepareTxt = [[_inputTxt.text stringByReplacingOccurrencesOfString:@"  " withString:@" "]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self chaFilter:_inputTxt.text];
        NSLog (@"Result: %@", prepareTxt);
        
        words = [[prepareTxt componentsSeparatedByString:@" "] count];
        NSLog(@"%lu",(unsigned long)words);
        
        // voice = [[NSUserDefaults standardUserDefaults]
                            // stringForKey:@"selectedVoice"];
        if (words<16) {
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                           {
                               // Background work
                               NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=make&text=%@&voice=%@",prepareTxt,voice];
                               NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                               NSLog(@"%@",url);
                               NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
                               [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
                               [serviceRequest setHTTPMethod:@"GET"];
                               
                               NSURLResponse *response;
                               NSError *error;
                               NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest
                                                                     returningResponse:&response
                                                                                 error:&error];
                               
                               // download in background thread to allow indicator
                               strData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                               NSLog(@"stringURL %@",strData);
                               saveURL = [NSURL URLWithString:strData];
                               NSLog(@"send url %@",saveURL);
                               

                               dispatch_async(dispatch_get_main_queue(), ^(void)
                                              {
                                                  // Main thread work (UI usually)
                                                  NSURLRequest *request = [NSURLRequest requestWithURL:saveURL];
                                                  NSURLConnection *movieDownload = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                                  
                                                  if (movieDownload) {
                                                      
                                                      receivedMovie=[NSMutableData data];
                                                      movieURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                                                      movieURL = [movieURL URLByAppendingPathComponent:@"movie.mp4"];
                                                      
                                                      // save the file
                                                      NSString *moviePath = [NSString stringWithFormat:@"%@",movieURL];
                                                      [[NSUserDefaults standardUserDefaults] setObject:moviePath forKey:@"movie"];
                                                      [[NSUserDefaults standardUserDefaults] synchronize];
                                                  } else {
                                                      // inform the user that the movie download could not be made
                                                      NSLog(@"failed to get movie");
                                                  }
                                              });
                           });
            
            
            return NO;
        } else {
            [self.view bringSubviewToFront:_Notification];
            [self.NotiMessage setText:@"Hiện tại chỉ được 15 từ thôi ;)"];
            [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:1 green:0.914 blue:0.694 alpha:1] /*Yellow #ffe9b1*/];
            self.NotiMessage.hidden = NO;
            _NotiMessage.alpha = 1.0;
            [UIView animateWithDuration:2 animations:^() {
                _NotiMessage.alpha = 0.0;
            }];
            [_inputTxt setEnabled:YES];
            prepareTxt = [NSString stringWithFormat:@""];
            NSLog(@"Nhieu chu qua");
        }
        
    }
    return YES;
}

-(IBAction)remake:(id)sender
{
    _remkBtn.hidden = YES;
    _playBtn.hidden = YES;
    [_inputTxt setEnabled:NO];
    // Prevent share old clips or without data
    _loaded = NO;
    [self.view bringSubviewToFront:_Notification];
    [self.NotiMessage setText:@"Tuyển diễn viên..."];
    [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
    self.NotiMessage.hidden = NO;
    _NotiMessage.alpha = 1.0;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       // Background work
                       NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=remake&text=%@&voice=%@",prepareTxt,voice];
                       NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                       NSLog(@"%@",url);
                       NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
                       [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
                       [serviceRequest setHTTPMethod:@"GET"];
                       
                       NSURLResponse *response;
                       NSError *error;
                       NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest
                                                             returningResponse:&response
                                                                         error:&error];
                       
                       // download in background thread to allow indicator
                       strData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                       NSLog(@"stringURL %@",strData);
                       saveURL = [NSURL URLWithString:strData];
                       NSLog(@"send url %@",saveURL);
                       
                       
                       dispatch_async(dispatch_get_main_queue(), ^(void)
                                      {
                                          // Main thread work (UI usually)
                                          NSURLRequest *request = [NSURLRequest requestWithURL:saveURL];
                                          NSURLConnection *movieDownload = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                          
                                          if (movieDownload) {
                                              
                                              receivedMovie=[NSMutableData data];
                                              movieURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                                              movieURL = [movieURL URLByAppendingPathComponent:@"movie.mp4"];
                                              
                                              // save the file
                                              NSString *moviePath = [NSString stringWithFormat:@"%@",movieURL];
                                              [[NSUserDefaults standardUserDefaults] setObject:moviePath forKey:@"movie"];
                                              [[NSUserDefaults standardUserDefaults] synchronize];
                                          } else {
                                              // inform the user that the movie download could not be made
                                              NSLog(@"failed to get movie");
                                          }
                                      });
                   });
}

-(void)play:(id)sender
{
    NSString *moviePath = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"movie"];
    // NSURL *movieURL = [NSURL URLWithString:moviePath];
   // NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    NSLog(@"%@",moviePath);
    
    // AVPlayer *player = [AVPlayer playerWithURL:movieURL]; // 
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:movieURL];
    
    // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    // Pass the AVPlayerItem to a new player
    
    _player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    
    AVPlayerLayer *layer = [AVPlayerLayer layer];
    
    [layer setPlayer:_player];
    [layer setFrame:_videolayer.bounds];
    [layer setBackgroundColor:[UIColor whiteColor].CGColor];
    [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [self.videolayer.layer addSublayer:layer];
    
    // [self.view bringSubviewToFront:_videoView];
    [self.view bringSubviewToFront:_panelView];
    [self.inputTxt setEnabled:NO];
    
    _playBtn.hidden = YES;
    _remkBtn.hidden = YES;
    _thumbnail.hidden = YES;
    [_player play];
    _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
}

-(void)stopVideo{
    NSLog (@"stop");
    
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self.view bringSubviewToFront:_videoView];
    [self.view bringSubviewToFront:_inputView];
    [self.inputTxt setEnabled:YES];
    _playBtn.hidden = NO;
    _saveBtn.hidden = NO;
    _remkBtn.hidden = NO;
    _thumbnail.hidden = NO;
    for (CALayer *layer in self.videolayer.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
}

/****/
// Download method
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // append the new data to the receivedData
    // receivedData is declared as a method instance elsewhere
    if(!receivedMovie)
    {
        receivedMovie = [NSMutableData data];
    }
    [receivedMovie appendData:data];
    [self.view bringSubviewToFront:_Notification];
    self.NotiMessage.hidden = NO;
    _NotiMessage.alpha = 1.0;
    [self.NotiMessage setText:@"Đang đóng phim, đợi chút..."];
    [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
    [UIView animateWithDuration:0.5 animations:^() {
        _NotiMessage.alpha = 0.0;
    }];
    // self.NotiMessage.hidden = NO;
    _remkBtn.hidden = YES;
    _playBtn.hidden = YES;
    self.tableViewTop.constant = -650;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    _playBtn.enabled = NO;
    _saveBtn.enabled = NO;
    _shareFbBtn.enabled = NO;
    _shareMesBtn.enabled = NO;

}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // inform the user
    NSLog(@"Connection failed! Error - %@",
          [error localizedDescription]
         // [[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
          );
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    // NSLog(@"Succeeded! Received %d bytes of data",[receivedMovie length]);

    if ([receivedMovie length] > 500) {
        [receivedMovie writeToURL:movieURL atomically:YES];
        
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:movieURL];
        NSLog(@"%@",movieURL);
        //add subtitles here
        /*
        // 1 - Set up the text layer
        CATextLayer *subtitle1Text = [[CATextLayer alloc] init];
        [subtitle1Text setFont:@"Helvetica"];
        [subtitle1Text setFontSize:18];
        [subtitle1Text setFrame:CGRectMake(0, _videolayer.frame.size.height - 50, _videolayer.frame.size.width, 100)];
        [subtitle1Text setString:_inputTxt.text];
        [subtitle1Text setAlignmentMode:kCAAlignmentCenter];
        [subtitle1Text setForegroundColor:[[UIColor whiteColor] CGColor]];
        
        // 2 - The usual overlay
        CALayer *overlayLayer = [CALayer layer];
        [overlayLayer addSublayer:subtitle1Text];
        overlayLayer.frame = _videolayer.bounds;
        [overlayLayer setMasksToBounds:YES];
        */
        // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        // Pass the AVPlayerItem to a new player
        _player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
        
        AVPlayerLayer *layer = [AVPlayerLayer layer];
        
        [layer setPlayer:_player];
        [layer setFrame:_videolayer.bounds/*CGRectMake(0, 60, 320, 320)*/];
        [layer setBackgroundColor:[UIColor whiteColor].CGColor];
        [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        [self.videolayer.layer addSublayer:layer];
        // [self.videolayer.layer addSublayer:overlayLayer];
        
        // [self.view bringSubviewToFront:_videoView];
        // [self.view bringSubviewToFront:_inputView];
        [self.inputTxt setEnabled:NO];
        
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Xong, xin mời xem"];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:0.6 green:1 blue:0.6 alpha:1] /*green*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
        [UIView animateWithDuration:1 animations:^() {
            _NotiMessage.alpha = 0.0;
        }];
        
        _playBtn.hidden = YES;
        _remkBtn.hidden = YES;
        _thumbnail.hidden = YES;
        self.bgLogo.hidden = YES;
        [_player play];
        [_inputTxt setEnabled:YES];
        
        _saved = NO;
        _loaded = YES;
        
        AVAsset *asset = [AVAsset assetWithURL:movieURL];
        
        // Get thumbnail at the start of the video
        CMTime thumbnailTime = [asset duration];
        thumbnailTime.value = 0;
        
        // Get image from the video at the given time
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        [_thumbnail setImage:thumbnail];
        // [_thumbnail setFrame:_videolayer.bounds];
        
        _playBtn.enabled = YES;
        _saveBtn.enabled = YES;
        _shareFbBtn.enabled = YES;
        _shareMesBtn.enabled = YES;
        
    } else {
        [self.inputTxt becomeFirstResponder];
        [self.view bringSubviewToFront:_Notification];
        [self.NotiMessage setText:@"Hic, ko có ai hết. Thử lại nè."];
        [self.NotiMessage setBackgroundColor:[UIColor colorWithRed:1 green:0.914 blue:0.694 alpha:1] /*yellow*/];
        self.NotiMessage.hidden = NO;
        _NotiMessage.alpha = 1.0;
        [UIView animateWithDuration:3 animations:^() {
            _NotiMessage.alpha = 0.0;
        }];
        [_inputTxt setEnabled:YES];
        _playBtn.enabled = YES;
        _saveBtn.enabled = YES;
        _shareFbBtn.enabled = YES;
        _shareMesBtn.enabled = YES;
        NSLog(@"Khong co chu");
    }
    
}

// keyboard handle (move up down)
// need to adapt to iphone 6 and 6+

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary *info = [n userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // self.keyboardBottom.constant = 0;
    self.keyboardTop.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
    // NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    // CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // CGRect viewframe = self.inputView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    // self.inputView.CGpointY = CGPointAdd(0, -200);
    // viewframe.size.height += (keyboardSize.height);
    
    /*self.inputView.center = CGPointMake(160, 410);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
   // [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    [self.view bringSubviewToFront:_inputView];
    self.keyboardIsShown = NO;*/
}

- (void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* info = [n userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // CGSize contronpanel = [[info objectForKey:self.controlpanel] CGRectValue].size;
    // NSLog(@"%f",keyboardSize.height);
    // NSLog(@"%f",((CGRectGetHeight(self.Notification.frame) + CGRectGetHeight(self.videoView.frame)) - keyboardSize.height));
    
    self.keyboardTop.constant = ((CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.Notification.frame) - CGRectGetHeight(self.videoView.frame)) - keyboardSize.height - (CGRectGetHeight(self.inputView.frame))-13);
    // self.keyboardBottom.constant = keyboardSize.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
    
   // CGPoint viewOrigin = self.inputView.frame.origin;
    
   // CGFloat viewHeight = 60;
    
    // self.inputView.center = CGPointMake(160, 568-keyboardSize.height-30);
    [self.view bringSubviewToFront:_panelView];
    
    /*
     CGRect visibleRect = self.inputView.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, viewOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, viewOrigin.y - visibleRect.size.height + viewHeight);
        
        self.inputView.center = CGPointMake(0, scrollPoint.y);
    }*/
}

// ***FB integration***

// define the state
enum MessengerShareMode {
    MessengerShareModeSend,
    MessengerShareModeComposer,
    MessengerShareModeReply
};

// shareMode holds state indicating which flow the user is in.
// Return the corresponding FBSDKMessengerContext based on that state.
enum MessengerShareMode shareMode;


-(void)shareToMess:(id)sender {
    if ([FBSDKMessengerSharer messengerPlatformCapabilities] & FBSDKMessengerPlatformCapabilityVideo) {
        if (_loaded == YES) {
            // getContextForShareMode is a helper method
            FBSDKMessengerShareOptions *options = [[FBSDKMessengerShareOptions alloc] init];
            // options.metadata = metadata;
            options.contextOverride = [self getContextForShareMode];
            
            // NSString *moviePath = [NSString stringWithFormat:@"%@",movieURL];
            NSData *videoData = [NSData dataWithContentsOfURL:movieURL];
            [FBSDKMessengerSharer shareVideo:videoData withOptions:options];
            // NSLog(@"share file with path %@",moviePath);
        }
    } else {
        
        // Messenger isn't installed. Redirect the person to the App Store.
        NSString *appStoreLink = @"https:// itunes.apple.com/us/app/facebook-messenger/id454638411?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
    }
    // if ([FBSDKMessengerSharer messengerPlatformCapabilities] & FBSDKMessengerPlatformCapabilityOpen) {
      // [FBSDKMessengerSharer openMessenger];
    // }
}

- (FBSDKMessengerContext *) getContextForShareMode
{
    // shareMode holds state indicating which flow the user is in.
    // Return the corresponding FBSDKMessengerContext based on that state.
    
    if (shareMode == MessengerShareModeSend) {
        // Force a send flow by returning a broadcast context.
        return [[FBSDKMessengerBroadcastContext alloc] init];
        
    } else if (shareMode == MessengerShareModeComposer) {
        // Force the composer flow by returning the composer context.
        return _composerContext;
        
    } else if (shareMode == MessengerShareModeReply) {
        // Force the reply flow by returning the reply context.
        return _replyContext;
    }
    
    
    return nil;
}

- (void) sharetoFB:(NSURL *)moviesaveURL
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    ALAssetsLibraryWriteVideoCompletionBlock videoWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image with metadata to Photo Library %@", newURL.absoluteString);
            assetURL = [NSURL URLWithString:newURL.absoluteString];
            NSLog(@"shareURL: %@", assetURL);
            FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
            video.videoURL = assetURL;
            FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
            content.video = video;
            
            [FBSDKShareDialog showFromViewController:self
                                         withContent:content
                                            delegate:nil];
        }
    };
    
    if (([library videoAtPathIsCompatibleWithSavedPhotosAlbum:movieURL]) && (_saved == NO))
    {
        [library writeVideoAtPathToSavedPhotosAlbum:movieURL
                                    completionBlock:videoWriteCompletionBlock];
        _saved = YES;
    }
}


- (void)sharetoFB{
    if (_loaded == YES) {
    [self sharetoFB:movieURL];
    }
}

//HotTable(trend) show and hide
-(IBAction)tip:(id)sender {
    [self tipShowHide];
}

-(void)tipShowHide {
    if (self.tableViewTop.constant == 0) {
        self.tableViewTop.constant = -650;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
            }];
        [_tipBtn setTitle:@"hot" forState:UIControlStateNormal];
        /*NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/hotbtn.png"];
        UIImage *tipbg = [UIImage imageWithContentsOfFile:fullpath];
        [_tipBtn setBackgroundImage:tipbg forState:UIControlStateNormal];*/
    } else {
        [_tipBtn setTitle:@"X thoát" forState:UIControlStateNormal];
        // [_tipBtn setBackgroundImage:nil forState:UIControlStateNormal];
        _spam.editable = YES;
   [_spam setFont:[UIFont fontWithName:@"Courier" size:12]];
        _spam.editable = NO;
    NSArray *spamtxt = [[NSArray alloc] initWithObjects:
                     @"Nói câu này đi mấy chế ơi",
                     @"Bấm đại cái gì đi nè",
                     @"Nt chọc người ấy đi chế",
                     @"Đời có gì vui?",
                     @"Embịlíulưỡinóikhónghehok",
                     @"Kể chuyện hài đi chế",
                     @"Chế nghĩ sao về em?",
                     @"Mấy câu này em nghĩ ra đó",
                                                    nil];
    int i = arc4random() % 7;
    _spam.text = [spamtxt objectAtIndex:i];
    self.tableViewTop.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    [self.view sendSubviewToBack:_videoView];
    [self.view sendSubviewToBack:_videolayer];
    [self.view sendSubviewToBack:_Notification];
    }
   
}

// clear memory
- (IBAction)change:(id)sender
{
    [self.storyboard instantiateViewControllerWithIdentifier:@"settingview"];
    [self performSegueWithIdentifier:@"maintosetting" sender:self];
    // [self presentViewController:viewController animated:YES completion:nil];
    // [self.ViewController dismissViewControllerAnimated:YES completion:nil];
}

// trending hot table
#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [trend count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    // static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"trend cell"];
    
    
    // Set up the cell...
   cell.textLabel.text = [trend objectAtIndex:indexPath.row];
    
 // cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *cellBgView =[[UIImageView alloc]init];
    cellBgView.alpha = 0.5;
    NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/hot-cell.png"];
    [cellBgView setImage:[UIImage imageWithContentsOfFile:fullpath]];
    [cell setBackgroundView:cellBgView];
    
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    // HotCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // voice = [[NSUserDefaults standardUserDefaults]
            // stringForKey:@"selectedVoice"];
    [self chaFilter:cell.textLabel.text];
    self.tableViewTop.constant = -650;
    [_tipBtn setTitle:@"hot" forState:UIControlStateNormal];
    /*NSString *fullpath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/hotbtn.png"];
    UIImage *tipbg = [UIImage imageWithContentsOfFile:fullpath];
    [_tipBtn setBackgroundImage:tipbg forState:UIControlStateNormal];*/
    // voice = @"";
    _inputTxt.text = cell.textLabel.text;
    [self creat:_creatBtn];
}

@end