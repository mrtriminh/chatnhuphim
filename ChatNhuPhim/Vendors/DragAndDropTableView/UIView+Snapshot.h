#import <UIKit/UIKit.h>

@interface UIView (Snapshot)
-(UIImage *)snapshotImageWithClearedRect:(CGRect)rect;
-(UIImage *)snapshotImage;
@end
