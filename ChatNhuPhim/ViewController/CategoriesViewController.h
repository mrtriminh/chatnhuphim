//
//  CategoriesViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/10/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CategoriesViewDelegate<NSObject>
-(void)willSearchWithCategoryName:(NSString*)title;
@end

@interface CategoriesViewController : UIViewController
@property (copy, nonatomic) NSArray *arrCategories;
@property (weak, nonatomic) IBOutlet UITableView *ibTableView;
@property (weak, nonatomic) id<CategoriesViewDelegate>delegate;
@end
