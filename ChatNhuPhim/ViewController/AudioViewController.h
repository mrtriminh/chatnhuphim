//
//  AudioViewController.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 1/18/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@import AVFoundation;
@import AssetsLibrary;
@import QuartzCore;
@import MediaPlayer;

@interface AudioViewController : UIViewController <AVAudioRecorderDelegate>
@property (strong, nonatomic) NSURL *URLVideo;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (strong, nonatomic) IBOutlet UIView *ibPlayerView;
@property (weak, nonatomic) IBOutlet UIButton *ibSaveButton;
@property (weak, nonatomic) IBOutlet UIButton *ibRecordPauseButton;
@property (strong, nonatomic) AVAudioRecorder *recorder;
@property (weak, nonatomic) IBOutlet UIProgressView *ibDownloadProgress;
@end
