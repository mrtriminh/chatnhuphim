//
//  WebViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/14/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (strong, nonatomic) NSString *addressURL;
@end
