

//
//  WebViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/14/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>
@property (weak,nonatomic) IBOutlet UIWebView *ibWebViewInApp;
@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _ibWebViewInApp.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSURL *URL = [NSURL URLWithString:_addressURL];
    [_ibWebViewInApp loadRequest:[NSURLRequest requestWithURL:URL]];
}

-(IBAction)backSetting:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
