//
//  CustomActionSheetViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/9/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "CustomActionSheetViewController.h"
#import "NSUserDefaults+Utils.h"

@interface CustomActionSheetViewController ()<UIGestureRecognizerDelegate>
@property(nonatomic) IBOutlet UIView *actionSheetView;
@end

@implementation CustomActionSheetViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(handleTapFromView:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    _actionSheetView.clipsToBounds = YES;
    _actionSheetView.layer.cornerRadius = 10;
    
    [self slideIn];
}


#pragma mark - Custom ActionSheet Methods

- (void)slideOut {
    
    [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
    
    // Set delegate and selector to remove from superview when animation completes
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    
    // Move this view to bottom of superview
    CGRect frame = self.actionSheetView.frame;
    frame.origin = CGPointMake(0.5, self.view.bounds.size.height);
    self.actionSheetView.frame = frame;
   
    [UIView commitAnimations];
    
    if (_delegate && [_delegate respondsToSelector:@selector(customerActionSheetSelectType:)]) {
        [_delegate customerActionSheetSelectType:CustomActionSheetNone];
    }
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([animationID isEqualToString:@"removeFromSuperviewWithAnimation"]) {
        [self.view removeFromSuperview];
    }
}

- (void)slideIn {
    
    self.view.frame = [[UIScreen mainScreen] bounds];
    [self.actionSheetView setBounds:CGRectMake(0, [[UIScreen mainScreen] bounds].origin.y, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    
    //set initial location at bottom of view
    CGRect frame = self.actionSheetView.frame;
    frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
    self.actionSheetView.frame = frame;
    [self.view addSubview:self.actionSheetView];
    
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    self.view.alpha = 1.0f;
    [[self.actionSheetView layer] addAnimation:animation forKey:@"TransitionToActionSheet"];
}


#pragma mark - Tap Gesture Recognizer Method

- (void) handleTapFromView: (UITapGestureRecognizer *)recognizer {
    [self slideOut];
}

-(IBAction)cancel:(id)sender {
    [self slideOut];
}

-(IBAction)shareFB:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(customerActionSheetSelectType:)]) {
        [_delegate customerActionSheetSelectType:CustomActionSheetFB];
    }
    NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] shareFB",[NSUserDefaults getDeviceToken]];
    NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    myLog(@"%@",url);
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
    [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
    [serviceRequest setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
    NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
    myLog(@"track cell usage: %@", requestReply);
}

-(IBAction)shareMessenger:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(customerActionSheetSelectType:)]) {
        [_delegate customerActionSheetSelectType:CustomActionSheetMessenger];
    }
    NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] sendFBMess",[NSUserDefaults getDeviceToken]];
    NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    myLog(@"%@",url);
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
    [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
    [serviceRequest setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
    NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
    myLog(@"track cell usage: %@", requestReply);
}

-(IBAction)saveFileToPhotos:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(customerActionSheetSelectType:)]) {
        [_delegate customerActionSheetSelectType:CustomActionSheetSaveFile];
    }
    NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] save",[NSUserDefaults getDeviceToken]];
    NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    myLog(@"%@",url);
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
    [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
    [serviceRequest setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
    NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
    myLog(@"track cell usage: %@", requestReply);
}

@end
