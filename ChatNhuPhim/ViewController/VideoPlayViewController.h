//
//  VideoPlayViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/5/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@import AVFoundation;
@import AssetsLibrary;
@import QuartzCore;
@import MediaPlayer;

@interface VideoPlayViewController : UIViewController<UIGestureRecognizerDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) NSURL *URLVideo;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (strong, nonatomic) IBOutlet UIView *ibPlayerView;
@property (weak, nonatomic) IBOutlet UIButton *ibSaveButton;
@property (weak, nonatomic) IBOutlet UIButton *ibExportToMake;
@property (weak, nonatomic) IBOutlet UIButton *ibExportRecord;
//@property (assign) BOOL madeVideo; not use in version 2.32
@property (assign, nonatomic) BOOL isExportRecord;
@property (assign, nonatomic) BOOL isExportToMake;
@property (copy, nonatomic) NSArray *arrScriptsClip;
@property (weak, nonatomic) IBOutlet UILabel *labelShareMessage;
@end
