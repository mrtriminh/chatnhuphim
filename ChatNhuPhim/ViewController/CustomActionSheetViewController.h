//
//  CustomActionSheetViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/9/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, CustomActionSheetType) {
    CustomActionSheetNone,
    CustomActionSheetFB,
    CustomActionSheetSaveFile,
    CustomActionSheetMessenger
};
@protocol CustomActionSheetDelegate<NSObject>
-(void)customerActionSheetSelectType:(CustomActionSheetType)sheetType;
@end

@interface CustomActionSheetViewController : UIViewController
- (void)slideOut;
@property (weak, nonatomic) id <CustomActionSheetDelegate>delegate;
@end
