//
//  CategoriesViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/10/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import "CategoriesViewController.h"
#import "UIColor+Utils.h"
#import "ClipsBusinessManager.h"

@interface CategoriesViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [ClipsBusinessManager getAllCategoryComplete:^(BOOL success, id data, NSString *error) {
        if (success) {
            NSMutableArray *results = [NSMutableArray arrayWithArray:data];
            [results insertObject:myLanguage(@"kDefaultCategory") atIndex:0];
            _arrCategories = [NSArray arrayWithArray:results];
            [_ibTableView reloadData];
            
        }
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrCategories count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
//    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:22];
    
    cell.textLabel.text = [_arrCategories objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *title =[_arrCategories objectAtIndex:indexPath.row];
    if (_delegate && [_delegate respondsToSelector:@selector(willSearchWithCategoryName:)]) {
        [_delegate willSearchWithCategoryName:title];
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}
-(void)viewDidLayoutSubviews {
    if ([_ibTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_ibTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_ibTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_ibTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
