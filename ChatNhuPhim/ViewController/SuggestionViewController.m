//
//  SuggestionViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/14/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "SuggestionViewController.h"
#import "MBProgressHUD.h"
@interface SuggestionViewController ()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *ibTextView;
@property (weak, nonatomic) IBOutlet UILabel *ibTitle;

@end

@implementation SuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _ibTitle.text = myLanguage(@"kGopYTitle");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)send:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = myLanguage(@"kSendGopy");
    hud.minShowTime =1.5;
    
    NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%@",_ibTextView.text];
    NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    myLog(@"%@",url);
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
    [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
    [serviceRequest setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
    NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
    myLog(@"requestReply: %@", requestReply);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kSendGopy") delegate:self cancelButtonTitle:@"Ok..." otherButtonTitles:nil, nil] show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
