//
//  MoreViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "MoreViewController.h"
#import "FacebookManager.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSString+Utils.h"
#import "WebViewController.h"
#import "SuggestionViewController.h"
#import "UIColor+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "CNP_MyLanguage.h"


@interface MoreViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ibTableView;
@property (weak, nonatomic) NSString *version;
@property (weak, nonatomic) NSString *lastVersion;
@property (nonatomic, assign)  unsigned long long int totalSize;
@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _ibTableView.tableFooterView = [UIView new];
    _ibTableView.tableHeaderView = [UIView new];
//    _ibTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    _ibTableView.separatorInset =  UIEdgeInsetsZero;
//    _ibTableView.layoutMargins = UIEdgeInsetsZero;
    _totalSize = 0;
    //[self checkClearSize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
    
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (![_version isEqualToString:_lastVersion]) {
        NSArray *array = [myLanguage(@"kArraySettings") componentsSeparatedByString:@","];
        return [array count];
//        return [kArrayUpdateSettings count];
    } else {
//    return [kArraySettings count];
        NSArray *array = [myLanguage(@"kArraySettings") componentsSeparatedByString:@","];
        return [array count];

    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:22];
    NSArray *array = [myLanguage(@"kArraySettings") componentsSeparatedByString:@","];
    if (indexPath.row == 2) {
        [self checkClearSize];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %lluMB",myLanguage(@"kClearCacheTitle"),_totalSize/1024/1024];
    } else {
    cell.textLabel.text = [array objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor colorWithHexString:@"#010101"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (indexPath.row) {
        case 0: {//Login FB
            [FBSDKAccessToken setCurrentAccessToken:nil];
            [FBSDKProfile setCurrentProfile:nil];
            NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"http://login.facebook.com"]];
            for (NSHTTPCookie* cookie in facebookCookies) {
                [cookies deleteCookie:cookie];
            }
            
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            __block UIView *rootView = delegate.window.rootViewController.view;
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
            hud.labelText = myLanguage(@"kFBConnect");
            hud.minShowTime =0.5;
            
            [[FacebookManager sharedInstance] logInWithReadPermissions: @[@"email", @"public_profile"] viewController:nil completion:^(BOOL success, NSString *error) {
                
                [MBProgressHUD hideHUDForView:rootView animated:YES];
                
                if (success) {
                    [self getFacebookProfileInfos];
                    //Show popup
                    [[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kFBLoginSuccess") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }else {
                    if (![NSString isEmptyString:error]) {
                        [[[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:kMessageAgreed otherButtonTitles:nil, nil] show];
                    }
                }
            }];
        }
            break;
        case 1: {//Add more clips
            WebViewController *webController = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
            webController.addressURL = @"http://www.chatnhuphim.com";
            [self.navigationController pushViewController:webController animated:YES];
        }
            break;
        case 2: {//Clear cache
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kCacheClearAlert") delegate:self cancelButtonTitle:myLanguage(@"kCacheClearCancel") otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case 3: {//term and condition
            WebViewController *webController = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
            webController.addressURL = @"http://www.chatnhuphim.com/term.php";
            [self.navigationController pushViewController:webController animated:YES];
        }
            break;
        case 4: {//Suggestion
            SuggestionViewController *sugestionController = [self.storyboard instantiateViewControllerWithIdentifier:kSuggestionViewController];
            [self.navigationController pushViewController:sugestionController animated:YES];
        }
            break;
        case 5: {//About
            NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/852245118200611"];
            if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
                [[UIApplication sharedApplication] openURL:facebookURL];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com"]];
            }
        }
            break;
        case 6: {//LANGUAGE
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:myLanguage(@"kSelectLanguage") delegate:self cancelButtonTitle:myLanguage(@"kCancelTitle") destructiveButtonTitle:nil otherButtonTitles:myLanguage(@"kLanguageEnglish"),myLanguage(@"kLanguageVietnam"), nil];
            [actionSheet showInView:self.view];
        }
            break;
        case 7: {//Update
            NSString *iTunesLink = @"itms://itunes.apple.com/us/app/chat-phim/id1005212128?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
            break;
        default:
            break;
    }
    
}

-(void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

-(void)clearVideoInDocument
{
    NSString *extension = @"mp4";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        
        if ([[filename pathExtension] isEqualToString:extension]) {
            
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
}

-(void)getFacebookProfileInfos {
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:nil];
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(result)
        {
            if ([result objectForKey:@"gender"]) {
                [NSUserDefaults saveUserGender:[result objectForKey:@"gender"]];
                //myLog(@"gender: %@",result);
            }
        }
    }];
    
    [connection start];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}
-(void)viewDidLayoutSubviews {
    if ([_ibTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_ibTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_ibTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_ibTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UIActionsheet 
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //myLog(@"Index: %ld", buttonIndex);
    if (buttonIndex == 0) {//select english
        [[CNP_MyLanguage shared] setLanguage:@"en"];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *chosenLang = [NSString stringWithFormat:@"en"];
        [userDefaults setObject:chosenLang forKey:kChosenLang];
        
    }else if(buttonIndex ==1) {//Select vietnamese
         [[CNP_MyLanguage shared] setLanguage:@"vi"];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *chosenLang = [NSString stringWithFormat:@"vi"];
        [userDefaults setObject:chosenLang forKey:kChosenLang];
    }
    
    if (buttonIndex != 2) {//It isnot Cancel event
        [[NSNotificationCenter defaultCenter] postNotificationName:kPushChangeLanguage object:nil];
        [_ibTableView reloadData];
    }
}

#pragma mark - UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        //            int cacheSize = [[NSURLCache sharedURLCache] currentDiskUsage];
        //            NSString *message = [NSString stringWithFormat:@"%@ %dMB",myLanguage(@"kCacheClear"),(cacheSize/1024)];
        [[[UIAlertView alloc] initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:myLanguage(@"kCacheClear"), nil] show];
        [self clearTmpDirectory];
        [self clearVideoInDocument];
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate.homeController.makeController clearAllMakeClipsForClearCache];
        NSLog(@"clear");
    }
}

//Check tmp and cache size
- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

-(void)checkClearSize {
    _totalSize = 0;
    [self checkDocumentSize];
    [self checkTmpSize];
    NSLog(@"clear size %llu",_totalSize/1024/1024);
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell *cell = [_ibTableView cellForRowAtIndexPath:indexpath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %lluMB",myLanguage(@"kClearCacheTitle"),_totalSize/1024/1024];
}

-(void)checkDocumentSize
{
    NSString *extension = @"mp4";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    unsigned long long int fileSize = 0;
    while ((filename = [e nextObject])) {
        
        if ([[filename pathExtension] isEqualToString:extension]) {
            
            fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:filename error:nil] fileSize];
            fileSize += fileSize;
        }
    }
    _totalSize += fileSize;
    //NSLog(@"doc %llu",fileSize);
}

-(void)checkTmpSize {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tmpDir = [[documentsDirectory stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"tmp"];
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:tmpDir error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[tmpDir stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    _totalSize += fileSize;
    //NSLog(@"%llu",fileSize);
}
    
-(void)dealloc {
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
