//
//  VideoPlayViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/5/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "VideoPlayViewController.h"
#import "UIView+Utils.h"
#import "UIApplication+Utils.h"
#import "MakeClipsBusinessManager.h"
#import "NSString+Utils.h"
#import "FacebookManager.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSUserDefaults+Utils.h"

@implementation VideoPlayViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_moviePlayerController == nil) {
        
        _moviePlayerController = [[MPMoviePlayerController alloc] init];//]WithContentURL:_URLVideo];
        [_moviePlayerController prepareToPlay];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayerController];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pauseVideo) name:kPushStopPlayVideo object:nil];
        _moviePlayerController.view.frame = _ibPlayerView.frame;
        _moviePlayerController.movieSourceType = MPMovieSourceTypeFile;
        _moviePlayerController.repeatMode = MPMovieRepeatModeNone;
        _moviePlayerController.controlStyle = MPMovieControlStyleNone;
        _moviePlayerController.view.backgroundColor = [UIColor lightGrayColor];
        _moviePlayerController.shouldAutoplay = NO;
        _moviePlayerController.contentURL = _URLVideo;
        [_ibPlayerView addSubviewWithFullSize:_moviePlayerController.view];
        //Add Tap gesture for Movie Player
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pauseVideo)];
        tap.delegate = self;
        tap.numberOfTapsRequired = 1;
        [_moviePlayerController.view addGestureRecognizer:tap];
    }
    
    [_moviePlayerController play];
//    self.madeVideo = NO;
    
    //check that can export script to make
    _ibExportToMake.hidden = _isExportToMake?NO:YES;
    _ibExportRecord.hidden = _isExportRecord?NO:YES;
    
    //Update language
    NSArray *array = [myLanguage(@"kVideoShareMessage") componentsSeparatedByString:@","];
    NSString *message = [array objectAtIndex: arc4random() % [array count]];
    _labelShareMessage.text = message;
    [_ibExportToMake setTitle:myLanguage(@"kVideoExportMake") forState:UIControlStateNormal];
}

-(void)moviePlayBackDidFinish:(NSNotification*)aNotify{
    
    myLog(@"Finish Playback");
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
}

-(void)pauseVideo{
    
    if(_moviePlayerController.playbackState == MPMoviePlaybackStatePlaying) {
        [_moviePlayerController pause];
    } else {
        [_moviePlayerController play];
         myLog(@"tap");
    }
}
#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kPushStopPlayVideo
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_moviePlayerController.view setHidden:YES];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
    _arrScriptsClip = nil;
}

- (IBAction)closeVideoController:(id)sender {
    if ([_arrScriptsClip count]) {
        _arrScriptsClip = nil;
    }
    [_moviePlayerController stop];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
    
    [self dismissViewControllerAnimated:YES completion:^{
        [UIApplication clearCacheVideo];
//        if (self.madeVideo == YES) {
//            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
//            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
//            [delegate.homeController.makeController showAd];
//        } else
        if (_isExportToMake) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kPushClosePreviewVideo object:nil];
        }
//        else {
//            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
//            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
//            [delegate.homeController.makeController hideAd];
//        }
    }];
    
}

- (IBAction)shareMessenger:(id)sender {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefault valueForKey:kDictUUID];
    
    if ([NSString isEmptyString:token]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please log in Facebook"
                                                        message:@"to send videos"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        alert.tag = 1;
        [alert show];
        
    } else {
        [MakeClipsBusinessManager shareToMessengerWithURL:_URLVideo];
        //self.madeVideo = YES;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
    if (buttonIndex == 1) {
        [self loginFB];
        }
    }
    if (alertView.tag == 2) {
        if (buttonIndex == 1) {
        [self loginFB];;
        }
    }
}

-(void)loginFB{
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"http://login.facebook.com"]];
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __block UIView *rootView = delegate.window.rootViewController.view;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
    hud.labelText = myLanguage(@"kFBConnect");
    hud.minShowTime =0.5;
    
    [[FacebookManager sharedInstance] logInWithReadPermissions: @[@"email", @"public_profile"] viewController:nil completion:^(BOOL success, NSString *error) {
        
        [MBProgressHUD hideHUDForView:rootView animated:YES];
        
        if (success) {
            [self getFacebookProfileInfos];
            //Show popup
            [[[UIAlertView alloc] initWithTitle:nil message:@"Nice. Login Facebook successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }else {
            if (![NSString isEmptyString:error]) {
                [[[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:kMessageAgreed otherButtonTitles:nil, nil] show];
            }
        }
    }];
}

-(void)getFacebookProfileInfos {
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:nil];
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(result)
        {
            if ([result objectForKey:@"gender"]) {
                [NSUserDefaults saveUserGender:[result objectForKey:@"gender"]];
                myLog(@"gender: %@",result);
            }
        }
    }];
    
    [connection start];
}

- (IBAction)shareFB:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefault valueForKey:kDictUUID];
    
    if ([NSString isEmptyString:token]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please log in Facebook"
                                                        message:@"to share videos"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
         alert.tag = 2;
        [alert show];
        
    } else {
    [MakeClipsBusinessManager sharetoFBWithURL:_URLVideo viewController:self];
    //self.madeVideo = YES;
    }
}

- (IBAction)saveToPhotos:(id)sender {
    _ibSaveButton.userInteractionEnabled = NO;
    [MakeClipsBusinessManager saveClipsFileToPhotos:_URLVideo alertShow:YES completion:^(BOOL success, NSURL *assetURL) {
        _ibSaveButton.userInteractionEnabled = YES;
        //self.madeVideo = YES;
    }];
}

//export video from script to MAKE Clips
- (IBAction)exportScriptClipsToMake:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kScriptExportToMake") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    NSLog(@"export");
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushScriptsClipToMake object:_arrScriptsClip];
    
    if ([_arrScriptsClip count]) {
        _arrScriptsClip = nil;
    }
    [_moviePlayerController stop];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
    
    [self dismissViewControllerAnimated:YES completion:^{
        //[UIApplication clearCacheVideo];
        //        if (self.madeVideo == YES) {
        //            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
        //            [delegate.homeController.makeController showAd];
        //        } else
        if (_isExportToMake) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kPushClosePreviewVideo object:nil];
        }
        //        else {
        //            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
        //            [delegate.homeController.makeController hideAd];
        //        }
    }];
    
    //Move to MAKE tap
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.homeController selectTabBarItem:1];
}

//export record file to MAKE
-(IBAction)exportRecordToMake:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushRecordFileToMake object:_URLVideo];
    [self dismissViewControllerAnimated:YES completion:^{
        [UIApplication clearCacheVideo];
        //        if (self.madeVideo == YES) {
        //            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
        //            [delegate.homeController.makeController showAd];
        //        } else
        if (_isExportRecord) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kPushCloseAudio object:nil];
        }
        //        else {
        //            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //            [delegate.homeController selectTabBarItem:1];//move to MAKE tab
        //            [delegate.homeController.makeController hideAd];
        //        }
    }];    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.homeController selectTabBarItem:1];
}

@end
