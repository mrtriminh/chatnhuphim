//
//  PreviewPlayViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/27/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewPlayViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *arrScripts;
@property (strong, nonatomic) NSString *scriptTitle;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (strong, nonatomic) IBOutlet UIView *ibParentProgressView;
@property (strong, nonatomic) NSMutableArray *arrProgressViews;
@property (weak, nonatomic) NSURL *userTempClipURL;
@end
