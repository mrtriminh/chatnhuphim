//
//  HomeViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/29/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeViewController.h"
#import "ClipsViewController.h"
#import "MakeViewController.h"
#import "MoreViewController.h"

@interface HomeViewController : UITabBarController
@property (strong, nonatomic) NSMutableArray *arrBarItems;
@property (strong, nonatomic) IBOutlet UIView *ibViewTabBar;
@property (strong, nonatomic) MakeViewController *makeController;
@property (strong, nonatomic) ClipsViewController *clipController;
@property (strong, nonatomic) MoreViewController *moreController;
-(void)selectTabBarItem:(NSInteger)index;
@end
