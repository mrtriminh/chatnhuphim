//
//  ClipsViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/29/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClipsViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *arrIBButtons;
@property (weak, nonatomic) IBOutlet UIView *ibViewContents;

-(void)selectTabSearchTabSearchWithKeyWord:(NSString*)keyword;

-(void)selectTabSearchTabSearch;
@end
