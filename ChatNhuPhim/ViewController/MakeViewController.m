//
//  MakeViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "MakeViewController.h"
#import "MakeClipsTableViewCell.h"
#import "DragAndDropTableView.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "VideoBusinessManager.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "NSMutableDictionary+Clips_Utils.h"
#import "MakeClipsBusinessManager.h"
#import "CustomActionSheetViewController.h"
#import "AppDelegate.h"
#import "UIApplication+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "PreviewClips.h"
#import "AudioViewController.h"

#define MAKE_CLIPS_WIDTH    (CGRectGetWidth([UIScreen mainScreen].bounds)*2.0)
#define MAKE_CLIPS_HEIGHT   (180*2.0)

#ifndef __IPHONE_7_0
typedef void (^PermissionBlock)(BOOL granted);
#endif

@import AssetsLibrary;
@import MobileCoreServices;
@import AVFoundation;
@import AudioToolbox;

@interface MakeViewController ()<UITableViewDataSource, UITableViewDelegate, DragAndDropTableViewDataSource,DragAndDropTableViewDelegate,
MakeClipsTableViewCellDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CustomActionSheetDelegate>{
    NSInteger CP_NowNumberOfAddClips;
    NSInteger CP_RowToAddRecordFile;
    BOOL CP_IsRecordVideo;
    BOOL CP_IsAddingAudio;
    __block NSURL *CP_FileURL;
    
    __weak IBOutlet UIView *CP_ibViewBottom;
}

@property (weak, nonatomic) IBOutlet DragAndDropTableView *ibMakeClipsTableView;
@property (strong, nonatomic) NSMutableArray *arrMakeDatas;
@property (strong, nonatomic) CustomActionSheetViewController * customAlert;
@property (strong, nonatomic) IBOutlet UITextView *contentTips;
@property (weak, nonatomic) IBOutlet UIButton *ibClearAll;
@end

@implementation MakeViewController
//@synthesize contentTips;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLanguage) name:kPushChangeLanguage object:nil];
    
    self.madeVideo = NO;
    CP_IsAddingAudio = NO;
    startAppAd = [[STAStartAppAd alloc] init]; //Startapp initiate
    CP_NowNumberOfAddClips = -1;
//    CP_ibViewBottom.layer.borderWidth = 0.3;
//    CP_ibViewBottom.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self.ibMakeClipsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.ibMakeClipsTableView.separatorColor = [UIColor clearColor];
    
    _ibMakeClipsTableView.separatorInset = UIEdgeInsetsZero;
    //_ibMakeClipsTableView.layoutMargins = UIEdgeInsetsZero;
    _ibMakeClipsTableView.tableFooterView = [UIView new];
    _ibMakeClipsTableView.tableHeaderView = [UIView new];
    
    //default items
    if ([_arrMakeDatas count]==0) {
         _arrMakeDatas = [NSMutableArray arrayWithCapacity:20];
        [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:@1]];
    }
    
    //Add observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedScriptVideo:) name:kPushScriptsClipToMake object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedRecordFile:) name:kPushRecordFileToMake object:nil];
    
    //set tips
    //NSString *tip = [NSString stringWithFormat:@"%@",myLanguage(@"kContentTips")];
    self.contentTips.text = myLanguage(@"kContentTips");
    [self.ibClearAll setTitle:myLanguage(@"kClearAllBtn") forState:UIControlStateNormal];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedScriptVideo:) name:kPushScriptsClipToMake object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedRecordFile:) name:kPushRecordFileToMake object:nil];
//    [startAppAd loadAd];  // Add this line
//    if (self.madeVideo) {
//        [startAppAd showAd];
//    }
}

- (void) updateLanguage {
     self.contentTips.text = myLanguage(@"kContentTips");
    [self.ibClearAll setTitle:myLanguage(@"kClearAllBtn") forState:UIControlStateNormal];
}

#pragma mark - UITableView's Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrMakeDatas count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 210.0f;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MakeClipsTableViewCell *cell = [[[NSBundle mainBundle]loadNibNamed:kMakeClipsTableViewCell owner:nil options:0] firstObject];
    [cell setCellInfo:[_arrMakeDatas objectAtIndex:indexPath.row] indexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.delegate = self;
    return cell;
}


-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    [_arrMakeDatas exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
}

#pragma mark DragAndDropTableViewDelegate

-(void)tableView:(UITableView *)tableView willBeginDraggingCellAtIndexPath:(NSIndexPath *)indexPath placeholderImageView:(UIImageView *)placeHolderImageView{
    
    // this is the place to edit the snapshot of the moving cell
    // add a shadow
    placeHolderImageView.layer.shadowOpacity = .3;
    placeHolderImageView.layer.shadowRadius = 1;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
}

-(void)tableView:(DragAndDropTableView *)tableView didEndDraggingCellAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)toIndexPath placeHolderView:(UIImageView *)placeholderImageView{
    [self resetLabelNumber];
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}
#pragma mark - MakeClipsTableViewCell
-(void)resetLabelNumber{
    //reset number
//    NSInteger number = 1;
//    for (id makeInfo in _arrMakeDatas) {
//        [makeInfo setANumber:[NSNumber numberWithInteger:number++]];
//    }

    //reload info cell
    [_ibMakeClipsTableView reloadData];
}

-(void)makeClipseTableViewCellAddClips:(MakeClipsTableViewCell *)cell{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    NSArray *titles = [myLanguage(@"kMakeActionSheetTitles") componentsSeparatedByString:@","];
    for( NSString *title in titles)  {
        [actionSheet addButtonWithTitle:title];
    }
    [actionSheet addButtonWithTitle:@"cancel"];
    actionSheet.cancelButtonIndex = [titles count];
    [actionSheet showInView:self.view];
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_NowNumberOfAddClips = indexPath.row;
    
}

//Trim clip and export to MAKE
-(void)makeClipseTableViewCellEditClips:(MakeClipsTableViewCell *)cell passVideoURL:(NSString *)url {
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_NowNumberOfAddClips = indexPath.row;

    //Using UIVideoEditor
    NSString *urlstring = [NSString stringWithFormat:@"%@",url];
    NSURL* myurl = [NSURL URLWithString:urlstring];
    NSString *path = [[myurl absoluteURL] path];
    BOOL can = [UIVideoEditorController canEditVideoAtPath:path];
    if (!can) {
        myLog(@"can't edit this video");
        return;
    }
    UIVideoEditorController* vc = [UIVideoEditorController new];
    vc.delegate = self;
    vc.videoPath = path;
    vc.videoQuality = UIImagePickerControllerQualityTypeIFrame960x540;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void) videoEditorController: (UIVideoEditorController*) editor
      didSaveEditedVideoToPath: (NSString*) editedVideoPath {
    myLog(@"path for edited video %@",editedVideoPath);
    //NSString *urlstring = [NSString stringWithFormat:editedVideoPath];
    NSURL *aPath = [NSURL fileURLWithPath:editedVideoPath];
    [self processLocalMovieAtPath:aPath];
    //    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(editedVideoPath)) {
    //        UISaveVideoAtPathToSavedPhotosAlbum(editedVideoPath, self,
    //                                            @selector(video:savedWithError:ci:), nil);
    //    // need to think of something else to do with it
    //    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)video:(NSString*)path savedWithError:(NSError*)err ci:(void*)ci {
    
}

//Replace audio and export to MAKE (use notification)
-(void)makeClipseTableViewCellEditAudio:(MakeClipsTableViewCell *)cell passVideoURL:(NSString *)url {
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_RowToAddRecordFile = indexPath.row;
    CP_IsAddingAudio = YES;
    NSString *urlstring = [NSString stringWithFormat:@"%@",url];
    NSURL *videoURL = [NSURL URLWithString:urlstring];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AudioViewController *audioController = [storyBoard instantiateViewControllerWithIdentifier:kAudioViewController];
    audioController.URLVideo = videoURL;
    [self.navigationController pushViewController:audioController animated:YES];
    
}

//add more cell
-(void)makeClipseTableViewCellAddMoreCell:(MakeClipsTableViewCell *)cell {
    
    if ([_arrMakeDatas count] == 6) {
        NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] cell 7",[NSUserDefaults getDeviceToken]];
        NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        myLog(@"%@",url);
        NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
        [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
        [serviceRequest setHTTPMethod:@"GET"];
        NSURLResponse *response;
        NSError *error;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
        NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
        myLog(@"track cell usage: %@", requestReply);

    } else if ([_arrMakeDatas count] == 10)  {
        NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] cell 10",[NSUserDefaults getDeviceToken]];
        NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        myLog(@"%@",url);
        NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
        [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
        [serviceRequest setHTTPMethod:@"GET"];
        NSURLResponse *response;
        NSError *error;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
        NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
        myLog(@"track cell usage: %@", requestReply);
    } else if ([_arrMakeDatas count] == 15) {
        NSString *inputURL = [NSString stringWithFormat:@"http://chatnhuphim.com/api.php?action=gopy&content=%s %@","[tracking] cell 15",[NSUserDefaults getDeviceToken]];
        NSURL *url = [NSURL URLWithString:[inputURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        myLog(@"%@",url);
        NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:url];
        [serviceRequest setValue:@"text" forHTTPHeaderField:@"Content-type"];
        [serviceRequest setHTTPMethod:@"GET"];
        NSURLResponse *response;
        NSError *error;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:&error];
        NSString *requestReply = [[NSString alloc] initWithBytes:[urlData bytes] length:[urlData length] encoding:NSASCIIStringEncoding];
        myLog(@"track cell usage: %@", requestReply);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    if ((indexPath.row == 0 && _arrMakeDatas.count == 1) || (indexPath.row == _arrMakeDatas.count-1)) {
        [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:nil]];
    }else {
        [_arrMakeDatas insertObject:[NSMutableDictionary makeClipsDictionaryWithNumber:nil] atIndex:indexPath.row+1];
    }
    
    [self resetLabelNumber];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_ibMakeClipsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
}

//add random cell
-(void)makeClipseTableViewCellAddRandomClips:(MakeClipsTableViewCell *)cell {
    _ibPlayAll.enabled = NO;
    __block NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    [[VideoBusinessManager manager] getListRandomsWithParams:nil completion:^(BOOL success, id data, NSString *error) {
        
        if (success) {
            NSMutableDictionary *makeInfo = [_arrMakeDatas objectAtIndex:indexPath.row];
            [makeInfo setMakeClipIsLocal:NO];
            [makeInfo setMakeClipThumbnailURL:[data getVideoThumbnailURL]];
            [makeInfo setMakeClipVideoURL:[data getVideoURL]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_ibMakeClipsTableView reloadData];
                cell.ibAddCellBtn.enabled = YES;
            });
        }
    }];
    
}

//Add random cell again (in action sheet)
-(void)makeClipseTableViewCellAddRandomClipsActionSheet:(NSInteger)row {
    _ibPlayAll.enabled = NO;
    //__block NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    [[VideoBusinessManager manager] getListRandomsWithParams:nil completion:^(BOOL success, id data, NSString *error) {
        
        if (success) {
//            NSMutableDictionary *makeInfo = [_arrMakeDatas objectAtIndex:CP_NowNumberOfAddClips];
//            [makeInfo setMakeClipIsLocal:NO];
//            [makeInfo setMakeClipThumbnailURL:[data getVideoThumbnailURL]];
//            [makeInfo setMakeClipVideoURL:[data getVideoURL]];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [_ibMakeClipsTableView reloadData];
//            });
            [self addClipToMakeWithData:data atCell:row];
        }
    }];
}

-(void)deleteMakeClipsOfMakeClipsTableViewCell:(MakeClipsTableViewCell *)cell{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    if ([_arrMakeDatas count] == 1) {
        [_arrMakeDatas removeAllObjects];
        [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:nil]];
    }else {
        [_arrMakeDatas removeObjectAtIndex:indexPath.row];
    }
    
    [self resetLabelNumber];
}

//Breakdown functions in actionsheet into separate buttons
-(void)makeClipseTableViewCellRecord:(MakeClipsTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_NowNumberOfAddClips = indexPath.row;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        BOOL flag = [MakeClipsBusinessManager startCameraControllerFromViewController:self usingDelegate:self];
        if (!flag) {
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:myLanguage(@"kErrorMessageNotUseCamera") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:alertAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else{
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
    }];
}

-(void)makeClipseTableViewCellAlbum:(MakeClipsTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_NowNumberOfAddClips = indexPath.row;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        BOOL flag = [MakeClipsBusinessManager startMediaBrowserFromViewController:self usingDelegate:self];
        if (!flag) {
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:alertAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else{
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
    }];
}

-(void)makeClipseTableViewCellSearch:(MakeClipsTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_ibMakeClipsTableView indexPathForCell:cell];
    CP_NowNumberOfAddClips = indexPath.row;
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.homeController selectTabBarItem:0];//move to Clips tab
    [delegate.homeController.clipController selectTabSearchTabSearch];
}

#pragma mark - UIActionSheet's Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    myLog(@"The index is selected: %ld", (long)buttonIndex);
    if (buttonIndex == 3) {//Select open the library photo albumn
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            BOOL flag = [MakeClipsBusinessManager startMediaBrowserFromViewController:self usingDelegate:self];
            if (!flag) {
                if ([UIAlertController class]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else{
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
        }];
        
    }else if (buttonIndex ==2) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            BOOL flag = [MakeClipsBusinessManager startCameraControllerFromViewController:self usingDelegate:self];
            if (!flag) {
                if ([UIAlertController class]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:myLanguage(@"kErrorMessageNotUseCamera") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else{
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:myLanguage(@"kErrorMessageNotAccessPhoto") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
        }];
    }else if (buttonIndex == 1){
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate.homeController selectTabBarItem:0];//move to Clips tab
        [delegate.homeController.clipController selectTabSearchTabSearch];
    } else if (buttonIndex == 0){
        [self makeClipseTableViewCellAddRandomClipsActionSheet:CP_NowNumberOfAddClips];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare((CFStringRef)mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSURL *moviePath = [info objectForKey:UIImagePickerControllerMediaURL] ;
        [self processLocalMovieAtPath:moviePath];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    myLog(@"User cancel selected albumn");
    CP_NowNumberOfAddClips = -1;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)processLocalMovieAtPath:(NSURL*)aPath{
    myLog(@"Media Path: %@", aPath);
    
    AVURLAsset *urlAsset = [[AVURLAsset alloc] initWithURL:aPath options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:urlAsset];
    generator.appliesPreferredTrackTransform = YES;
    
    //Set the time and size of thumbnail for image
    NSError *err = NULL;
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    CGSize maxSize = CGSizeMake(MAKE_CLIPS_WIDTH, MAKE_CLIPS_HEIGHT);
    generator.maximumSize = maxSize;
    
    CGImageRef imgRef = [generator copyCGImageAtTime:thumbTime actualTime:NULL error:&err];
    UIImage *thumbnail = [[UIImage alloc] initWithCGImage:imgRef];
   
    NSMutableDictionary *makeInfo = [_arrMakeDatas objectAtIndex:CP_NowNumberOfAddClips];
    [makeInfo setMakeClipIsLocal:YES];
    [makeInfo setURLPath:aPath];
    [makeInfo setThumbnailImage:thumbnail];
    [_ibMakeClipsTableView reloadData];
        NSLog(@"process local");

    CP_NowNumberOfAddClips = -1;
}

-(void)processRecordFileAtPath:(NSURL*)aPath{
    myLog(@"Media Path: %@", aPath);
    
    AVURLAsset *urlAsset = [[AVURLAsset alloc] initWithURL:aPath options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:urlAsset];
    generator.appliesPreferredTrackTransform = YES;
    
    //Set the time and size of thumbnail for image
    NSError *err = NULL;
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    CGSize maxSize = CGSizeMake(MAKE_CLIPS_WIDTH, MAKE_CLIPS_HEIGHT);
    generator.maximumSize = maxSize;
    
    CGImageRef imgRef = [generator copyCGImageAtTime:thumbTime actualTime:NULL error:&err];
    UIImage *thumbnail = [[UIImage alloc] initWithCGImage:imgRef];
    
    NSMutableDictionary *makeInfo = [_arrMakeDatas objectAtIndex:CP_RowToAddRecordFile];
    [makeInfo setMakeClipIsLocal:YES];
    [makeInfo setURLPath:aPath];
    [makeInfo setThumbnailImage:thumbnail];
    [_ibMakeClipsTableView reloadData];
    NSLog(@"process record");
    
}

#pragma mark - View All Videos
-(IBAction)viewallVideo:(id)sender {
    
    [MakeClipsBusinessManager mergerAndSaveClipsToPhoto:_arrMakeDatas presentPlayBackController:YES completion:nil];
  
}

#pragma mark - Share Popup
-(IBAction)sharePopup:(id)sender {
    if ([MakeClipsBusinessManager checkHasExistentMakeClips:_arrMakeDatas]) {
        
        if (!_customAlert) {
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            _customAlert = [[CustomActionSheetViewController alloc] init];
            [delegate.window.rootViewController.view addSubview:_customAlert.view];
            [delegate.window.rootViewController addChildViewController:_customAlert];
            [_customAlert didMoveToParentViewController:delegate.window.rootViewController];
            [_customAlert viewWillAppear:YES];
            _customAlert.delegate = self;
        }

    }
}
#pragma mark - Clear All Make Clip
-(IBAction)clearAllMakeClips:(id)sender{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kClearAllClip") delegate:self cancelButtonTitle:myLanguage(@"kClearAllClipOK") otherButtonTitles:myLanguage(@"kClearAllClipCancel"), nil];
    [alertView show];
}

-(IBAction)new:(id)sender{
    [startAppAd showAd];
}

-(void)clearAllMakeClipsForClearCache{
    
        [_arrMakeDatas removeAllObjects];
        [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:@1]];
        [_ibMakeClipsTableView reloadData];
    NSLog(@"clear cache called");
}
//-(void)showAd{
//    self.madeVideo = NO;
//    myLog(@"showed ad");
//}
//
//-(void)hideAd{
//    self.madeVideo = NO;
//}

#pragma mark - UIAlertView
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    if (buttonIndex == 0) {
        [UIApplication clearCacheDocument];
        [UIApplication clearCacheVideo];
        [_arrMakeDatas removeAllObjects];
        [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:@1]];
        [_ibMakeClipsTableView reloadData];
    }
}
#pragma mark - CustomActionSheet Delegate
-(void)customerActionSheetSelectType:(CustomActionSheetType)sheetType{
    myLog(@"AA");
    
    switch (sheetType) {
        case CustomActionSheetNone: {
            [_customAlert removeFromParentViewController];
            _customAlert = nil;
        }
            break;
        case CustomActionSheetFB: {
            [MakeClipsBusinessManager mergerAndSaveClipsToPhoto:_arrMakeDatas presentPlayBackController:NO completion:^(BOOL success, NSURL *fileURL) {
                //show alert
                if (success) {
                    [MakeClipsBusinessManager sharetoFBWithURL:fileURL viewController:self];
                }
            }];
            
        }
            break;
        case CustomActionSheetSaveFile: {
            [MakeClipsBusinessManager mergerAndSaveClipsToPhoto:_arrMakeDatas presentPlayBackController:NO completion:^(BOOL success, NSURL *fileURL) {
                //show alert
                if (success) {
                    [MakeClipsBusinessManager saveClipsFileToPhotos:fileURL];
                }
            }];
            
        }
            break;
        case CustomActionSheetMessenger: {
            [MakeClipsBusinessManager mergerAndSaveClipsToPhoto:_arrMakeDatas presentPlayBackController:NO completion:^(BOOL success, NSURL *fileURL) {
                //show alert
                if (success) {
                    [MakeClipsBusinessManager shareToMessengerWithURL:fileURL];
                }
            }];
        }
            break;
        default:
            break;
    }
}

-(void)addClipToMakeWithData:(id)data {
    myLog(@"Received the video to make clips");
    
    NSMutableDictionary *makeInfo = [NSMutableDictionary makeClipsDictionaryWithNumber:@([_arrMakeDatas count]+1)];
    [makeInfo setMakeClipIsLocal:NO];
    [makeInfo setMakeClipThumbnailURL:[data getVideoThumbnailURL]];
    [makeInfo setMakeClipVideoURL:[data getVideoURL]];
    
    [_arrMakeDatas addObject:makeInfo];
    [_ibMakeClipsTableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
         [_ibMakeClipsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_arrMakeDatas count]-1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
    
}

-(void)addClipToMakeWithData:(id)data atCell:(NSInteger)cellNumber {
    myLog(@"Received the video to make clips");
    
    NSMutableDictionary *makeInfo = [NSMutableDictionary makeClipsDictionaryWithNumber:@([_arrMakeDatas count]+1)];
    [makeInfo setMakeClipIsLocal:NO];
    [makeInfo setMakeClipThumbnailURL:[data getVideoThumbnailURL]];
    [makeInfo setMakeClipVideoURL:[data getVideoURL]];
    
    [_arrMakeDatas removeObjectAtIndex:cellNumber];
    [_arrMakeDatas insertObject:makeInfo atIndex:cellNumber];
    [_ibMakeClipsTableView reloadData];
    
}

#pragma mark - NSNotification Center
-(void)receivedScriptVideo:(NSNotification*)aNotif{
    NSArray *arrayScriptsVideo =  aNotif.object;
    if ([arrayScriptsVideo count]) {
        NSLog(@"video number %d",[arrayScriptsVideo count]);
        //do some things
        if (![_arrMakeDatas count]) {
            _arrMakeDatas = [NSMutableArray arrayWithCapacity:20];
            [_arrMakeDatas addObject:[NSMutableDictionary makeClipsDictionaryWithNumber:@1]];
        }
        for (PreviewClips *clips in arrayScriptsVideo) {
            
            NSMutableDictionary *makeInfo = [NSMutableDictionary makeClipsDictionaryWithNumber:@([_arrMakeDatas count]+1)];
            //Get image from video
            AVAsset *asset = [AVAsset assetWithURL:clips.localVideoURL];
            AVAssetImageGenerator *assetImageGene = [[AVAssetImageGenerator alloc] initWithAsset:asset];
            assetImageGene.appliesPreferredTrackTransform = YES;
            CMTime time = CMTimeMake(0, 1);
            NSError *error = nil;
            CMTime actualTime;
            UIImage *thumbnail;
            CGImageRef image = [assetImageGene copyCGImageAtTime:time actualTime:&actualTime error:&error];
            if (!error) {
                thumbnail = [[UIImage alloc] initWithCGImage:image];
            }
            CGImageRelease(image);
            
            [makeInfo setMakeClipIsLocal:YES];
            [makeInfo setURLPath:clips.localVideoURL];
            NSLog(@"added %@",clips.localVideoURL);
            if (!error) {
                [makeInfo setThumbnailImage:thumbnail];
            }
            [_arrMakeDatas addObject:makeInfo];
        }
        //reload all data
        [_ibMakeClipsTableView reloadData];
    }
}

-(void)receivedRecordFile:(NSNotification*)aNotif{
    NSURL *aPath = aNotif.object;//[NSURL fileURLWithPath:aNotif.object];
   // NSLog(@"row before process %d",CP_RowToAddRecordFile);
    [self processRecordFileAtPath:aPath];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_ibMakeClipsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(CP_RowToAddRecordFile) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });

}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
