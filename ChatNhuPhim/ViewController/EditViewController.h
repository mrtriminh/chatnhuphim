//
//  EditViewController.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 11/25/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreMedia/CoreMedia.h>
#import <QuartzCore/QuartzCore.h>

#import "AVSECommand.h"
#import "AVSETrimCommand.h"
#import "AVSEExportCommand.h"
#import "MakeClipsTableViewCell.h"
#import "MakeViewController.h"

@interface EditViewController : UIViewController
{
    AVSEExportCommand *exportCommand;
    MakeClipsTableViewCell *myCell;
    MakeViewController *myMakeController;
}

@property AVPlayer *player;
@property AVPlayerLayer *playerLayer;
@property double currentTime;
@property (readonly) double duration;

@property AVMutableComposition *composition;
@property AVMutableVideoComposition *videoComposition;
@property AVMutableAudioMix *audioMix;
@property AVAsset *inputAsset;
@property CALayer *watermarkLayer;

@property IBOutlet UIActivityIndicatorView *loadingSpinner;
@property IBOutlet UILabel *unplayableLabel;
@property IBOutlet UILabel *noVideoLabel;
@property IBOutlet UILabel *protectedVideoLabel;

@property IBOutlet UIButton *playPauseButton;
@property IBOutlet UIButton *exportButton;
@property IBOutlet UIView *playerView;
@property IBOutlet UIProgressView *exportProgressView;

@property (weak, nonatomic) NSString *editVideoPath;

- (void)reloadPlayerView;
- (void)exportWillBegin;
- (void)exportDidEnd;
- (void)editCommandCompletionNotificationReceiver:(NSNotification*)notification;
- (void)exportCommandCompletionNotificationReceiver:(NSNotification*)notification;

- (IBAction)playPauseToggle:(id)sender;
//- (IBAction)edit:(id)sender;
- (IBAction)exportToMovie:(id)sender;

@end
