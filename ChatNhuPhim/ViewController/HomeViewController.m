//
//  HomeViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/29/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "HomeViewController.h"
#import "TabBarItemView.h"
#import "NSString+Utils.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "AppDelegate.h"

#define TAB_BAR_HEIGHT 49.0
#define TAB_BAR_COLOR @"#ece9f0"


@interface HomeViewController ()
@property (assign, nonatomic) NSInteger numberTabBar;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLanguage) name:kPushChangeLanguage object:nil];
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.homeController = self;
    
    self.numberTabBar = 3;
    
    self.arrBarItems = [NSMutableArray arrayWithCapacity:self.numberTabBar];
    
    self.tabBar.hidden = YES;
    [self initViewForTabBar];
    [self initViewControllerForTabBar];
    [self selectTabBarItem:0];
    
    //Set language to chosen language
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *chosenLang = [userDefaults objectForKey:kChosenLang];
    if ([chosenLang length] == 0) {
        [[CNP_MyLanguage shared] setLanguage:@"vi"];
    } else {
    [[CNP_MyLanguage shared] setLanguage:chosenLang];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushChangeLanguage object:nil];
    }
}


-(IBAction)eventClick:(UIButton*)sender{
    if (sender.tag != self.selectedIndex) {
        [self selectTabBarItem:sender.tag];
    }
}

-(void)selectTabBarItem:(NSInteger)index{
    for (TabBarItemView *tabBarItem in self.arrBarItems) {
        BOOL isSelected = (tabBarItem.ibButton.tag == index);
        tabBarItem.ibButton.selected = isSelected? YES: NO;
        
        tabBarItem.ibButton.titleLabel.font = isSelected?[UIFont fontWithName:@"Helvetica-Medium" size:15]:[UIFont fontWithName:@"Helvetica" size:15];
    }
    self.selectedIndex = index;
    if (index == 2) {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate.homeController.moreController checkClearSize];
    }
}

//init tab bar item
-(void)initViewForTabBar{
    self.view.backgroundColor = [UIColor blackColor];
    
    self.ibViewTabBar = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds)-TAB_BAR_HEIGHT, CGRectGetWidth([UIScreen mainScreen].bounds), TAB_BAR_HEIGHT)];
    [self.ibViewTabBar setBackgroundColor:[UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1]];
    [self.view addSubview:self.ibViewTabBar];
    
    CGFloat widthOfButton = CGRectGetWidth([UIScreen mainScreen].bounds)/3.0;
    CGFloat posX = 0.0;
    
    NSArray *arrayMarriage = [myLanguage(@"kArrayTitlesTabBar") componentsSeparatedByString:@","];

    for (NSInteger num = 0; num < self.numberTabBar; num++) {
        
        TabBarItemView *tabBarItem = [[TabBarItemView alloc] initWithFrame:CGRectMake(posX, 0, widthOfButton, TAB_BAR_HEIGHT)];
        [tabBarItem initWithTitle:[arrayMarriage objectAtIndex:num]];
        tabBarItem.ibButton.tag = num;
        [tabBarItem.ibButton addTarget:self action:@selector(eventClick:) forControlEvents:UIControlEventTouchUpInside];
        
        posX += CGRectGetWidth(tabBarItem.frame);
        
        [self.arrBarItems addObject:tabBarItem];
        [self.ibViewTabBar addSubview:tabBarItem];
        
    }
    
    float maxEdge = CGRectGetMaxX(self.ibViewTabBar.frame);
    if (posX < maxEdge ) {
        float delta = (maxEdge - posX)/2;
        myLog(@"Delta: %f", delta);
        for (TabBarItemView *tabBarItem in self.arrBarItems) {
            tabBarItem.frame = CGRectOffset(tabBarItem.frame, delta, 0);
        }
    }
}

-(void)updateLanguage {
    NSInteger idx = 0;
    NSArray *arrayMarriage = [myLanguage(@"kArrayTitlesTabBar") componentsSeparatedByString:@","];
    for (TabBarItemView *tab in _arrBarItems) {
        [tab setTitle:[[arrayMarriage objectAtIndex:idx] uppercaseString]];
        idx++;
    }
}


//init view controller
-(void)initViewControllerForTabBar{
    if ([self.viewControllers count]) {
        self.viewControllers = nil;
    }
    
    _makeController = [self.storyboard instantiateViewControllerWithIdentifier:kMakeViewController];
    [_makeController viewDidLoad];
    _clipController = [self.storyboard instantiateViewControllerWithIdentifier:kClipsViewController];
    _moreController = [self.storyboard instantiateViewControllerWithIdentifier:kMoreViewController];
    
    self.viewControllers = @[_clipController, _makeController , _moreController];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
