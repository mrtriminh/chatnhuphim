//
//  MakeViewController.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StartApp/StartApp.h>

@interface MakeViewController : UIViewController<UIAlertViewDelegate,UIVideoEditorControllerDelegate>
{
    STAStartAppAd* startAppAd;  //Ads
}

-(void)addClipToMakeWithData:(id)data;
-(void)updateLanguage;
-(void)clearAllMakeClipsForClearCache;

@property(nonatomic, copy) NSString *editVideoPath;
@property (weak, nonatomic) IBOutlet UIButton *ibPlayAll;
@property (assign) BOOL madeVideo;

@end
