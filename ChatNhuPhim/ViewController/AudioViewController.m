//
//  AudioViewController.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 1/18/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import "AudioViewController.h"
#import "UIView+Utils.h"
#import "UIApplication+Utils.h"
#import "MakeClipsBusinessManager.h"
#import "NSString+Utils.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSUserDefaults+Utils.h"
#import "AudioToolbox/AudioToolbox.h"

@interface AudioViewController () {
    AVPlayer *player;
    BOOL ready;
    NSTimer *playTimer;
    int duration;
    int currentPlayTime;
}
@property (weak, nonatomic) IBOutlet UILabel *labelDubTip;
@property (weak, nonatomic) IBOutlet UIView *ibOverlayView;

@end


@implementation AudioViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //player.muted = YES;
    //_ibRecordPauseButton.hidden = YES;
    _ibRecordPauseButton.selected = NO;
    ready = NO;
    _labelDubTip.text = myLanguage(@"kAudioTip");
    _ibOverlayView.hidden = NO;
    _labelDubTip.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAudio) name:kPushCloseAudio object:nil];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:_URLVideo];
    
    duration = (CMTimeGetSeconds(playerItem.asset.duration)*10);//((playerItem.duration.value*1000)/playerItem.duration.timescale);
    NSLog(@"duration %d",duration);//CMTimeGetSeconds(playerItem.duration));
    
    // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopRecord) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    
    player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    
    AVPlayerLayer *layer = [AVPlayerLayer layer];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    NSLog(@"w %f",screenSize.width);
    CGFloat height = screenSize.width*480/854;
    CGFloat y = (250 - height)/2;
    //_ibPlayerView.bounds.size.width = [screenSize.width];
    [layer setPlayer:player];
    [layer setFrame:CGRectMake(_ibPlayerView.bounds.origin.x,y,screenSize.width,height)];
    //[layer setPosition:_ibPlayerView.center];
    [layer setBackgroundColor:[UIColor blackColor].CGColor];
    [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [self.ibPlayerView.layer addSublayer:layer];
    player.actionAtItemEnd = AVPlayerActionAtItemEndPause;

    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"userRecordAudio.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    NSError *setCategoryError = nil;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&setCategoryError];
    
    //detect route change
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(routeChange:)
                                                 name: AVAudioSessionRouteChangeNotification
                                               object: session];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    _recorder.delegate = self;
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
//    player.muted = YES;
//    [player play];
//   // [MBProgressHUD showHUDAddedTo:_ibPlayerView animated:YES];
}

- (IBAction)recordPauseTapped:(id)sender {
    _ibOverlayView.hidden = YES;
    _labelDubTip.hidden = YES;
    _ibRecordPauseButton.selected = YES;
    //PlayTime tracking
    currentPlayTime = 0;
    playTimer = [NSTimer scheduledTimerWithTimeInterval:0.05
                                                         target:self
                                                       selector:@selector(updatePlayTime)
                                                       userInfo:nil
                                                        repeats:YES];
    [playTimer fire];
    // Stop the audio player before recording
    if (!_recorder.recording) {
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
      //  [player pause];
        // Start recording
        [_ibRecordPauseButton setTitle:@"Cancel" forState:UIControlStateNormal];
        // Play after
        
        [_recorder record];
        player.muted = YES;
        [player seekToTime:kCMTimeZero];
        [player play];
        ready = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  });
    //    [_moviePlayerController play];
        
    } else {
        // Stop before
         [playTimer invalidate];
        _ibRecordPauseButton.selected = NO;
        _ibOverlayView.hidden = NO;
        _labelDubTip.hidden = NO;
        [player pause];
        [player seekToTime:kCMTimeZero];
          // Pause recording
        [_recorder stop];
        [_ibRecordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
        NSError *setCategoryError = nil;
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:&setCategoryError];
        //[MakeClipsBusinessManager mergerUserAudio:_recorder.url withClip:_URLVideo showHUD:YES isFromScriptClips:NO completion:nil];
        [playTimer invalidate];

    }
}

-(void)stopRecord{
    [playTimer invalidate];
    _ibRecordPauseButton.selected = NO;
    _ibOverlayView.hidden = NO;
    _labelDubTip.hidden = NO;
    myLog(@"Finish Playback");
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//                                                  object:_moviePlayerController];
//    if ((_ibRecordPauseButton.hidden == NO)&&(ready == YES)) {
    [_recorder stop];
    [_ibRecordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    NSError *setCategoryError = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    [audioSession setActive:NO error:&setCategoryError];
    [MakeClipsBusinessManager mergerUserAudio:_recorder.url withClip:_URLVideo showHUD:YES isFromScriptClips:NO completion:nil];
    
}

- (void)routeChange:(NSNotification*)notification {
    //[_recorder record];
//    [MBProgressHUD hideHUDForView:_ibPlayerView animated:YES];
//    [player pause];
//    [player seekToTime:kCMTimeZero];
//    _ibRecordPauseButton.hidden = NO;
//    NSDictionary *interuptionDict = notification.userInfo;
//    
//    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
//    
//    switch (routeChangeReason) {
//        case AVAudioSessionRouteChangeReasonUnknown:
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonUnknown");
//            break;
//            
//        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
//            // a headset was added or removed
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNewDeviceAvailable");
//            break;
//            
//        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
//            // a headset was added or removed
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
//            break;
//            
//        case AVAudioSessionRouteChangeReasonCategoryChange:
//            // called at start - also when other audio wants to play
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonCategoryChange");//AVAudioSessionRouteChangeReasonCategoryChange
//            break;
//            
//        case AVAudioSessionRouteChangeReasonOverride:
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOverride");
//            break;
//            
//        case AVAudioSessionRouteChangeReasonWakeFromSleep:
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonWakeFromSleep");
//            break;
//            
//        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
//            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory");
//            break;
//            
//        default:
//            break;
//    }
}

-(void)updatePlayTime {
    if (currentPlayTime <= (duration*2)) {
    currentPlayTime += 1;
    NSLog(@"current %d",currentPlayTime);
    _ibDownloadProgress.progress = currentPlayTime/(float)(duration*2);
    NSLog(@"play %f",_ibDownloadProgress.progress);
    } else {
        [playTimer invalidate];
        currentPlayTime = 0;
    }
}

-(IBAction)closeAudioViewController:(id)sender {
    [self closeAudio];
}

-(void)closeAudio {
    [player pause];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
