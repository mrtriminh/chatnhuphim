//
//  ClipsViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/29/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "ClipsViewController.h"
#import "UIColor+Utils.h"
#import "BaseClipsView.h"
#import "ExploreClipsView.h"
#import "SearchClipsView.h"
#import "FavoriteClipsView.h"
#import "UIView+Utils.h"
#import "ClipsBusinessManager.h"
#import "NSString+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "PreviewPlayViewController.h"
#import "ScriptEntity.h"
#import "WYPopoverController.h"
#import "CategoriesViewController.h"
#import <StartApp/StartApp.h>

@interface ClipsViewController ()<ExploreClipsViewDelegate, SearchClipsViewDelegate, UIActionSheetDelegate, CategoriesViewDelegate, WYPopoverControllerDelegate, STADelegateProtocol>
@property (strong, nonatomic) IBOutletCollection(BaseClipsView) NSArray *arrIBBaseClipsView;
@property (strong, nonatomic) WYPopoverController *popoverController;
@property (assign, nonatomic) GenderTypeIcon genderType;
@property (strong, nonatomic) NSString *categoryNameSelected;
@property (strong, nonatomic) STAStartAppNativeAd *startAppNativeAd;
@end

@implementation ClipsViewController
@synthesize startAppNativeAd;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Ads
    startAppNativeAd = [[STAStartAppNativeAd alloc] init];
    STANativeAdPreferences *pref = [[STANativeAdPreferences alloc]init];
    pref.adsNumber = kAmountAds; // Select ads number
    pref.bitmapSize = SIZE_150X150;     //Select image quality
    pref.autoBitmapDownload = YES;    // When set to NO no images will be downloaded by the SDK
    
    [startAppNativeAd loadAdWithDelegate:self withNativeAdPreferences:pref];
    NSLog(@"Count Ads: %ld", (unsigned long)[startAppNativeAd.adsDetails count]);
    
    
   //UIView
    ExploreClipsView *exploreView = (id)[UIView viewWithNibName:@"ExploreClipsView"];
    [exploreView setUpTopLayout];
    exploreView.startAppNativeAd = startAppNativeAd;
    exploreView.delegate = self;
    exploreView.hidden = NO;
    SearchClipsView *searchView = (id)[UIView viewWithNibName:@"SearchClipsView"];
    searchView.hidden = YES;
    searchView.delegate = self;
    [searchView.ibCategoryName setTitle:myLanguage(@"kDefaultCategory") forState:UIControlStateNormal];
    [searchView.ibCategoryName setTitle:myLanguage(@"kDefaultCategory") forState:UIControlStateSelected];
    FavoriteClipsView *favoriteView = (id)[UIView viewWithNibName:@"FavoriteClipsView"];
    favoriteView.hidden = YES;
    favoriteView.topLabel.text = myLanguage(@"FavTopLabelContent");
    _arrIBBaseClipsView = @[exploreView, searchView, favoriteView];
    [_ibViewContents addSubviewWithFullSize:exploreView];
    [_ibViewContents addSubviewWithFullSize:searchView];
    [_ibViewContents addSubviewWithFullSize:favoriteView];
    
    
    //Set default Explore button is selected
    for (UIButton *btn in self.arrIBButtons) {
        btn.selected = NO;
    }
    
    UIButton *exploreButton = [self.arrIBButtons firstObject];
    exploreButton.selected = YES;
    
    
    [ClipsBusinessManager getAllSequences:nil completion:^(BOOL success, id data, NSString *error) {
        if (data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ExploreClipsView *exploreView = [_arrIBBaseClipsView firstObject];
                //exploreView.startAppNativeAd = startAppNativeAd;
                [exploreView setArrSuggestion:data];
            });
        }
    }];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)changeFunctionClick:(UIButton*)sender {
    [self.view endEditing:YES];
    NSInteger idxView = 0;
    for (UIButton *btn in self.arrIBButtons) {
        if([btn isEqual:sender]){
            for (NSInteger i = 0; i < [_arrIBBaseClipsView count]; i++) {
                BaseClipsView *baseView = [_arrIBBaseClipsView objectAtIndex:i];
                baseView.hidden = (i == idxView)?NO:YES;
                if (i == 1 && idxView == 1) {
                    [self keyboardOn];
                }
                if (i == 2 && idxView == 2) {
                    [self getAllFavoriteVideo];
                }
            }
            btn.selected = YES;
        }else{
            btn.selected = NO;
        }
        idxView++;
    }
}


#pragma mark - ExploreClipsView's Delegate
-(void)showAdsAt:(NSInteger)position {
    STANativeAdDetails *adDetails = [startAppNativeAd.adsDetails objectAtIndex:position];
    [adDetails sendClick];
}

-(void)refreshNewData {
    [ClipsBusinessManager getAllSequences:nil completion:^(BOOL success, id data, NSString *error) {
        if (data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ExploreClipsView *exploreView = [_arrIBBaseClipsView firstObject];
                [exploreView setArrSuggestion:data];
                [exploreView.refreshControl endRefreshing];
            });
        }
    }];
}

-(void)selectedExploreClipsWithScriptEntity:(ScriptEntity *)scriptEntity {
   
    NSString *slideshowID = [NSString stringWithFormat:@"%ld",(long)scriptEntity.ID];
    NSLog(@"id %@",slideshowID);
    [ClipsBusinessManager listSequencesSuggestion:slideshowID completion:^(BOOL success, id data, NSString *error) {
            NSLog(@"Data: %@", data);
            
            PreviewPlayViewController *preview = [self.storyboard instantiateViewControllerWithIdentifier:kPreviewPlayViewController];
            preview.scriptTitle = scriptEntity.message;
            [preview setArrScripts:data];
            [self.navigationController pushViewController:preview animated:YES];
        }];
}
-(void)selectedExploreClipsWithCategoryName:(NSString *)categoryName {

    [ClipsBusinessManager getClipsWithCategoryName:categoryName completion:^(BOOL success, id data, NSString *error) {
        myLog(@"Data :%@", data);
        if (success) {
            ExploreClipsView *exploreView = [_arrIBBaseClipsView firstObject];
            [exploreView setUpDataInfo:data];
        }
    }];
}

-(void)getAllFavoriteVideo {
    [ClipsBusinessManager getFavoriteClipsWithUUID:[NSUserDefaults getDeviceToken] completion:^(BOOL success, id data, NSString *error) {
        FavoriteClipsView *favoriteView = [_arrIBBaseClipsView lastObject];
        [favoriteView setUpDataInfo:data];
        favoriteView.topLabel.text = myLanguage(@"FavTopLabelContent");
    }];
}

-(void)keyboardOn {
    SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
    [searchView.ibSearchField becomeFirstResponder];
}

#pragma mark - SearchClipsView's Delegate
-(void)willSearchVideoWithName:(NSString *)aName {

    [ClipsBusinessManager searchClipsWithKeyword:aName categoryName:_categoryNameSelected comletion:^(BOOL success, id data, NSString *error) {
        myLog(@"%@", data);
        SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
        [searchView setUpDataInfo:data autoPlayBackFirstVideo:NO];
    }];
}

-(void)willSearchAndPlayVideoFirstWithName:(NSString *)aName {
#pragma mark - [Not use current version 2.5]
//    [ClipsBusinessManager searchClipsWithKeyword:aName genderVoice:_genderType comletion:^(BOOL success, id data, NSString *error) {
//        myLog(@"%@", data);
//        SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
//        [searchView setUpDataInfo:data autoPlayBackFirstVideo:YES];
//    }];
    [ClipsBusinessManager searchClipsWithKeyword:aName categoryName:_categoryNameSelected comletion:^(BOOL success, id data, NSString *error) {
        myLog(@"%@", data);
        SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
        [searchView setUpDataInfo:data autoPlayBackFirstVideo:NO];

    }];
}

//-(void)updateGenderIcon:(GenderTypeIcon)genderType {
//    _genderType = genderType;
//    SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
//    [searchView changeVoiceIconWithGenderType:genderType];
//}
//
//-(void)willChangeGenderIcon {
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose voice" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Any" otherButtonTitles:@"Male", @"Female", nil];
//    [actionSheet showInView:self.view];
//    
//}
//
//#pragma mark - UIActionSheet's Delegate
//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    myLog(@"Number: %ld", (long)buttonIndex);
//    switch (buttonIndex) {
//        case 0: {
//            [self updateGenderIcon:GenderTypeIconNone];
//        }
//            break;
//        case 1: {
//            [self updateGenderIcon:GenderTypeIconMale];
//        }
//            break;
//        case 2: {
//            [self updateGenderIcon:GenderTypeIconFemale];
//        }
//            break;
//        default:
//            break;
//    }
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Clip View
-(void)selectTabSearchTabSearchWithKeyWord:(NSString*)keyword {
    UIButton *searchButton = [_arrIBButtons objectAtIndex:1];
    [self changeFunctionClick:searchButton];
    
    [ClipsBusinessManager searchClipsWithKeyword:keyword genderVoice:_genderType comletion:^(BOOL success, id data, NSString *error) {
        myLog(@"%@", data);
        SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
        [searchView setUpDataInfo:data autoPlayBackFirstVideo:YES];
        searchView.ibSearchField.text = keyword;
    }];
}

-(void)selectTabSearchTabSearch {
    UIButton *searchButton = [_arrIBButtons objectAtIndex:1];
    [self changeFunctionClick:searchButton];
}

-(void)willShowCategoriesWithSender:(UIButton *)sender {
    [self.view endEditing:YES];
    
    CategoriesViewController *categoriesController = [self.storyboard instantiateViewControllerWithIdentifier:kCategoriesViewController];
    categoriesController.preferredContentSize = CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), 200);
    categoriesController.delegate = self;
    _popoverController = [[WYPopoverController alloc] initWithContentViewController:categoriesController];
    _popoverController.delegate = self;
    
    _popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    _popoverController.wantsDefaultContentAppearance = NO;
    
    _popoverController.theme.arrowBase =  0;
    _popoverController.theme.arrowHeight = 0.0;
    
    
//    [_popoverController presentPopoverAsDialogAnimated:YES completion:nil];
    [_popoverController presentPopoverFromRect:sender.bounds
                                        inView:sender
                      permittedArrowDirections:WYPopoverArrowDirectionUp
                                      animated:YES
                                       options:WYPopoverAnimationOptionFadeWithScale];

}

#pragma mark - WYPopoverControllerDelegate

- (void)popoverControllerDidPresentPopover:(WYPopoverController *)controller {
    
    NSLog(@"popoverControllerDidPresentPopover");
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller {
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller {
    if (controller == _popoverController) {
        _popoverController.delegate = nil;
        _popoverController = nil;
    }
    
}

- (BOOL)popoverControllerShouldIgnoreKeyboardBounds:(WYPopoverController *)popoverController {
    return YES;
}

- (void)popoverController:(WYPopoverController *)popoverController willTranslatePopoverWithYOffset:(float *)value
{
    // keyboard is shown and the popover will be moved up by 163 pixels for example ( *value = 163 )
    *value = 0; // set value to 0 if you want to avoid the popover to be moved
}

-(void)willSearchWithCategoryName:(NSString *)title {
    _categoryNameSelected = title;
    SearchClipsView *searchView = [_arrIBBaseClipsView objectAtIndex:1];
    [searchView.ibCategoryName setTitle:title forState:UIControlStateNormal];
    [searchView.ibCategoryName setTitle:title forState:UIControlStateSelected];
    [_popoverController dismissPopoverAnimated:YES];
    _popoverController.delegate = nil;
    _popoverController = nil;
}

@end
