//
//  PreviewPlayViewController.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/27/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "PreviewPlayViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreMedia/CoreMedia.h>
#import <QuartzCore/QuartzCore.h>
#import "UIView+Utils.h"
#import "APIManager.h"
#import "PreviewClips.h"
#import "MBProgressHUD.h"
#import "LLSimpleCamera.h"
#import "MakeClipsBusinessManager.h"
#import "ProgressEntity.h"
#import "RecordEntity.h"

@import MediaPlayer;


#define     kTimeExactRun  0.01
#define     kTotalTimeRecord 10.0
#define     kASecondByMiniSeconds 1000.0


@interface PreviewPlayViewController ()<UIGestureRecognizerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    BOOL CNP_IsFirstPlayed;
    BOOL CNP_IsCurrentRecordVideo;
    NSInteger CNP_NumberRecordVideo;
    CGFloat CNP_MinLength;
}
@property (weak, nonatomic) IBOutlet UILabel *ibMessageLabel;
@property (assign, nonatomic) NSInteger curIndexIntoScript;
@property (assign, nonatomic) BOOL isUsedFullTime;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (weak, nonatomic) IBOutlet UIView *ibPlayerView;
@property (weak, nonatomic) IBOutlet UILabel *ibSuggestLabel;
@property (strong, nonatomic) NSTimer *timerDownload;
@property (strong, nonatomic) NSTimer *timerHUD;
@property (strong, nonatomic) NSTimer *timerRecordVideo;
@property (weak, nonatomic) IBOutlet UIView *ibOverlayCamera;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (weak, nonatomic) IBOutlet UIButton *ibNextButton;
@property (strong, nonatomic) LLSimpleCamera *camera;
@property (weak, nonatomic) IBOutlet UIButton *myButton;
@property (weak, nonatomic) IBOutlet UIView *ibRecordTempView;
@property (weak, nonatomic) IBOutlet UIView *cropViewTop;
@property (weak, nonatomic) IBOutlet UIView *cropViewBot;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cropViewTopHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cropViewBotHeight;
@property (weak, nonatomic) IBOutlet UIButton *ibSwitchCameraButton;
@property (weak, nonatomic) MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet UIButton *ibDeleteRecord;
@property (weak, nonatomic) IBOutlet UILabel *labelTapHold;

@end

@implementation PreviewPlayViewController
- (IBAction)swicthCamera:(id)sender {
    [_camera togglePosition];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _ibTitleLabel.text = @"";
    _labelTapHold.text = myLanguage(@"kLabelTapHold");
    _ibNextButton.hidden = YES;
    _curIndexIntoScript = 0;
    _ibParentProgressView.hidden = YES;
    _ibSuggestLabel.hidden = YES;
    _ibRecordTempView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closePreviewVideo) name:kPushClosePreviewVideo object:nil];
    //set button
    [_myButton addTarget:self action:@selector(holdRecord:) forControlEvents:UIControlEventTouchDown];
    [_myButton addTarget:self action:@selector(releaseRecord:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    if (_moviePlayerController == nil) {
        
        _moviePlayerController = [[MPMoviePlayerController alloc] init];
        [_moviePlayerController prepareToPlay];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayerController];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pauseVideo) name:kPushStopPlayVideo object:nil];
        _moviePlayerController.view.frame = _ibPlayerView.frame;
        _moviePlayerController.movieSourceType = MPMovieSourceTypeFile;
        _moviePlayerController.repeatMode = MPMovieRepeatModeNone;
        _moviePlayerController.controlStyle = MPMovieControlStyleNone;
        _moviePlayerController.view.backgroundColor = [UIColor lightGrayColor];
        _moviePlayerController.shouldAutoplay = NO;
        
        [_ibPlayerView addSubviewWithFullSize:_moviePlayerController.view];
        //Add Tap gesture for Movie Player *no need*
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadPlayVideo)];
//        tap.delegate = self;
//        tap.numberOfTapsRequired = 1;
//        [_moviePlayerController.view addGestureRecognizer:tap];
    }
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.labelText = myLanguage(@"kPreviewLoadingMessage");
    _hud.minShowTime =1.5;
    
    //init array for progress view
    
    
    
    _arrProgressViews = [NSMutableArray arrayWithCapacity:10];
    CNP_IsCurrentRecordVideo = NO;
    CNP_NumberRecordVideo = 0;
    CGFloat widthScreen = CGRectGetWidth([UIScreen mainScreen].bounds);
    CNP_MinLength = widthScreen / ((kTotalTimeRecord / (kASecondByMiniSeconds * kTimeExactRun)) * kASecondByMiniSeconds);
    
    //Crop view
    _cropViewTop.hidden = YES;
    _cropViewBot.hidden = YES;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    _cropViewTopHeight.constant = (125 - ((screenWidth/1.779)/2));
    _cropViewBotHeight.constant = (125 - ((screenWidth/1.779)/2));
    //NSLog(@"screen width: %f cropView: %f",screenWidth,_cropViewTopHeight.constant);
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _ibSuggestLabel.text = @"Xem kỹ, đợi tới lượt diễn nha";
    _ibSuggestLabel.hidden = YES;
    _ibRecordTempView.hidden = YES;
}

-(void)finishedDownload {
    BOOL isDownloadComplete = [self allVideosAreDownloadCompleted];
    if (isDownloadComplete) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [_timerDownload invalidate];
        _ibMessageLabel.hidden = YES;
        if ([_arrScripts count] && !CNP_IsFirstPlayed) {
            PreviewClips *firstReview = [_arrScripts objectAtIndex:_curIndexIntoScript];
            _moviePlayerController.contentURL = firstReview.localVideoURL;
            [_moviePlayerController play];
        }else {
            CNP_IsFirstPlayed = YES;
        }
    }else {
        
       // [self performSelector:@selector(changeMessage) withObject:nil afterDelay:2.0];
    }
}

-(void)changeMessageLoad {
    NSArray *array = [myLanguage(@"kArrayTitleRandom") componentsSeparatedByString:@","];
    int count = (int)[array count];
    NSInteger rand = (int)arc4random_uniform(count);
    _hud.labelText = [array objectAtIndex:rand];
}

-(void)changeMessageMerge {
    NSArray *array = [myLanguage(@"kArrayTitleRandomForMerge") componentsSeparatedByString:@","];
    int count = (int)[array count];
    NSInteger rand = (int)arc4random_uniform(count);
    _hud.labelText = [array objectAtIndex:rand];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pauseVideo {
    [_moviePlayerController pause];
}

-(void)moviePlayBackDidFinish:(NSNotification*)aNotify{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    _curIndexIntoScript++;
    
    if (_curIndexIntoScript < [_arrScripts count]) {
        
        PreviewClips *nextVideo = [_arrScripts objectAtIndex:_curIndexIntoScript];
        
        if ([nextVideo.type isEqualToString:kDictTypeSelect]) {
            _ibSuggestLabel.hidden = NO;
            _ibOverlayCamera.hidden = YES;
            _ibRecordTempView.hidden = YES;
            _cropViewTop.hidden = YES;
            _cropViewBot.hidden = YES;
            _ibSuggestLabel.text = @"Xem kỹ, đợi tới lượt diễn nha";
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(moviePlayBackDidFinish:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:_moviePlayerController];
            [_moviePlayerController prepareToPlay];
            _moviePlayerController.contentURL = nextVideo.localVideoURL;
            [_moviePlayerController play];
        }else {
            myLog(@"Is User type");
            _ibSuggestLabel.hidden = NO;
            _ibOverlayCamera.hidden = NO;
           // _ibRecordTempView.hidden = NO;
            _ibParentProgressView.hidden = NO;
            _cropViewTop.hidden = NO;
            _cropViewBot.hidden = NO;
            _ibSuggestLabel.text = [NSString stringWithFormat:(@"'%@'"),nextVideo.suggestionWords];
            
            
            if (!self.camera) {
                if ([LLSimpleCamera isFrontCameraAvailable]) {
                    self.camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                                 position:LLCameraPositionFront
                                                             videoEnabled:YES];
                }else {
                    self.camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                                 position:LLCameraPositionRear
                                                             videoEnabled:YES];
                }
                [self.ibOverlayCamera addSubviewWithFullSize:self.camera.view];
                [self.ibOverlayCamera bringSubviewToFront:_ibSwitchCameraButton];
            }
            
            if (self.camera) {
                [self.camera start];
                 _ibRecordTempView.hidden = NO;
                 _ibDeleteRecord.hidden = YES;
            }
        }
    }
}

-(void)reloadPlayVideo {
    [_moviePlayerController stop];
    [_moviePlayerController play];
}

- (IBAction)closeView:(id)sender {
    [_moviePlayerController stop];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextVideo:(id)sender {
    
    _ibNextButton.hidden = YES;
    _curIndexIntoScript++;
    _ibNextButton.userInteractionEnabled = NO;
    if (self.camera) {//stop camera record
        [self.camera stop];
    }
    
    //remove progress view
    for (NSInteger idx = 0; idx < [_arrProgressViews count]; idx++) {
        ProgressEntity *entity = [_arrProgressViews objectAtIndex:idx];
        [entity.progressView removeFromSuperview];
    }
    //Clear Progress View cached
    [_arrProgressViews removeAllObjects];
    CNP_IsCurrentRecordVideo = NO;
    _isUsedFullTime = NO;
    CNP_NumberRecordVideo = 0;
    
    BOOL hasUserType = [self checkNextScriptHasTypeForUser];
    if (hasUserType) {
        //Merga record clips before merge all (prevent more than 16 clips)
        NSMutableArray *arrayTemp = [NSMutableArray arrayWithCapacity:100];
        PreviewClips *clips = [_arrScripts objectAtIndex:(_curIndexIntoScript-1)];
        for (RecordEntity *entity in clips.arrRecordFiles) {
            NSTimeInterval interval = [entity.recordEnd timeIntervalSinceDate:entity.recordBegin];
            if (interval > 0.7) {//Condition to video can export
                PreviewClips *obj = [[PreviewClips alloc] init];
                obj.localVideoURL = entity.recordURL;
                [arrayTemp addObject:obj];
            }
        }
        [MakeClipsBusinessManager mergerUserClipWithAudio:arrayTemp audioURL:clips.localAudioURL showHUD:NO isFromScriptClips:YES completion:^(BOOL success,NSURL *url){
            clips.type = kDictTypeSelect;
            clips.localVideoURL = url;
            NSLog(@"merge user clips done");
        }];
        
        if (_curIndexIntoScript < [_arrScripts count]) {
            PreviewClips *nextVideo = [_arrScripts objectAtIndex:_curIndexIntoScript];
            if ([nextVideo.type isEqualToString:kDictTypeSelect]) {
                _ibSuggestLabel.hidden = NO;
                _ibOverlayCamera.hidden = YES;
                _ibRecordTempView.hidden = YES;
                _ibSuggestLabel.text = @"Xem kỹ, đợi tới lượt diễn nha";
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(moviePlayBackDidFinish:)
                                                             name:MPMoviePlayerPlaybackDidFinishNotification
                                                           object:_moviePlayerController];
                [_moviePlayerController prepareToPlay];
                _moviePlayerController.contentURL = nextVideo.localVideoURL;
                [_moviePlayerController play];
            }else {
                myLog(@"Is User type");
                _ibSuggestLabel.hidden = NO;
                _ibOverlayCamera.hidden = NO;
                _ibRecordTempView.hidden = NO;
                _ibSuggestLabel.text = [NSString stringWithFormat:(@"'%@'"),nextVideo.suggestionWords];
                if (self.camera) {
                    [self.camera start];
                }
            }
        }
    }else if (!hasUserType || _curIndexIntoScript >= [_arrScripts count]) {
        NSMutableArray *arrayFinal = [NSMutableArray arrayWithCapacity:100];
        
        //Merga record clips before merge all (prevent more than 16 clips)
        NSMutableArray *arrayTemp = [NSMutableArray arrayWithCapacity:100];
        PreviewClips *clips = [_arrScripts objectAtIndex:(_curIndexIntoScript-1)];
        for (RecordEntity *entity in clips.arrRecordFiles) {
            NSTimeInterval interval = [entity.recordEnd timeIntervalSinceDate:entity.recordBegin];
            if (interval > 0.7) {//Condition to video can export
                PreviewClips *obj = [[PreviewClips alloc] init];
                obj.localVideoURL = entity.recordURL;
                [arrayTemp addObject:obj];
            }
        }
        
        [MakeClipsBusinessManager mergerUserClipWithAudio:arrayTemp audioURL:clips.localAudioURL showHUD:YES isFromScriptClips:YES completion:^(BOOL success,NSURL *url){
            clips.type = kDictTypeSelect;
            clips.localVideoURL = url;
          
            for (PreviewClips *clips in _arrScripts) {
                if ([clips.type isEqualToString:kDictTypeSelect]) {
                    [arrayFinal addObject:clips];
                }else {
                    NSInteger count = [clips.arrRecordFiles count];
                    if (count > 0) {//Check User has record video or not
                        //Possibly add merge audio function here
                        for (RecordEntity *entity in clips.arrRecordFiles) {
                            NSTimeInterval interval = [entity.recordEnd timeIntervalSinceDate:entity.recordBegin];
                            if (interval > 0.7) {//Condition to video can export
                                PreviewClips *obj = [[PreviewClips alloc] init];
                                obj.localVideoURL = entity.recordURL;
                                [arrayFinal addObject:obj];
                            }
                        }
                    }
                }
            }
            [_moviePlayerController stop];
            //export all video
            [MakeClipsBusinessManager mergerAndSaveClipsToPhoto:arrayFinal presentPlayBackController:YES isFromScriptClips:YES completion:nil];
        }];
        
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _ibNextButton.userInteractionEnabled =  YES;
    });
}

#pragma mark - gesture delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kPushStopPlayVideo
                                                  object:nil];
    [_moviePlayerController.view setHidden:YES];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
}

-(void)setArrScripts:(NSMutableArray *)arrScripts {
    if (arrScripts) {
        _arrScripts = [NSMutableArray arrayWithArray:arrScripts];
        for (PreviewClips *clips in self.arrScripts) {
            if ([clips.type isEqualToString:kDictTypeSelect]) {
                [[APIManager shared] downloadFileWithURL:clips.videoURL informationDownload:clips isFromClips:NO];
            }
            else if ([clips.type isEqualToString:kDictTypeUser]) {
                 [[APIManager shared] downloadFileWithURL:clips.audioURL informationDownload:clips isFromClips:NO];
            }
        }
        self.timerDownload = [NSTimer timerWithTimeInterval:0.7
                                                     target:self
                                                   selector:@selector(finishedDownload)
                                                   userInfo:nil
                                                    repeats:YES];
        
        self.timerHUD = [NSTimer timerWithTimeInterval:1.5
                                                     target:self
                                                   selector:@selector(changeMessageLoad)
                                                   userInfo:nil
                                                    repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:self.timerDownload forMode:NSDefaultRunLoopMode];
        [self.timerDownload fire];
        
        [[NSRunLoop currentRunLoop] addTimer:self.timerHUD forMode:NSDefaultRunLoopMode];
        [self.timerHUD fire];
    }
}

//check all video are download complete to Local app or not?
-(BOOL)allVideosAreDownloadCompleted {
    for (PreviewClips *clips in _arrScripts) {
        if (!clips.downloadIsCompleted && [clips.type isEqualToString:kDictTypeSelect]) {
            return NO;
            break;
        }
    }
    return YES;
}

- (IBAction)releaseRecord:(id)sender {
    _ibDeleteRecord.hidden = NO;
    [self.timerRecordVideo invalidate];
    [_camera stopRecording:^(LLSimpleCamera *camera, NSURL *outputFileUrl, NSError *error) {
        
    }];
    CNP_IsCurrentRecordVideo = NO;
    
    NSInteger videoRecordIndex = CNP_NumberRecordVideo - 1;
    NSDate *endTime = [NSDate date];
    ProgressEntity *entity;
    if ([_arrProgressViews count]) {
        entity = [_arrProgressViews objectAtIndex:videoRecordIndex];
        entity.end = endTime;
    }
    
    
    NSTimeInterval totalTimeRecord = [entity.end timeIntervalSinceDate:entity.begin];
    
    if (totalTimeRecord < 0.7) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kRecordMoreThan1") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        //Cannot save record that less than 1s
         PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
        if ([_arrProgressViews count]) {
            [clips.arrRecordFiles removeObjectAtIndex:videoRecordIndex];
            entity.progressView.backgroundColor = [UIColor clearColor];
            [entity.progressView removeFromSuperview];
            [_arrProgressViews removeObjectAtIndex:videoRecordIndex];
            CNP_NumberRecordVideo -= 1;
        }else{
            CNP_NumberRecordVideo -= 1;
        }
        
        return;
    }else {
        _ibNextButton.hidden = NO;
        //Update end record time
        PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
        RecordEntity *recordEnity = [clips.arrRecordFiles objectAtIndex:videoRecordIndex];
        recordEnity.recordEnd = endTime;
    }
    
}

//Begin record a video with camera
- (IBAction)holdRecord:(id)sender {
    
    if (_isUsedFullTime) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kRecordLessThan10") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    CNP_IsCurrentRecordVideo = YES;
    CNP_NumberRecordVideo++;
    //re-update color for progress view
    for (ProgressEntity * progress in _arrProgressViews) {
        progress.progressView.backgroundColor = [UIColor yellowColor];
        progress.state = ProgressStateNone;
    }
    
   
    NSArray *arrayPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *fileName = [NSString stringWithFormat:@"%@.mov", [NSDate date]];
    NSString *fileWriterPath = [[arrayPath firstObject] stringByAppendingPathComponent:fileName];
    fileWriterPath = [fileWriterPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *recordURL = [NSURL fileURLWithPath:fileWriterPath];
    
    PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
    RecordEntity *recordEnity = [[RecordEntity alloc] init];
    
    if (![clips.arrRecordFiles count]) {
        clips.arrRecordFiles = [NSMutableArray array];
    }
    recordEnity.recordBegin = [NSDate date];
    recordEnity.recordURL = recordURL;
    [clips.arrRecordFiles addObject:recordEnity];
    
    //Begin record file
    [_camera startRecordingWithOutputUrl:recordURL];
    
    //create a scheduler to update progress view
    self.timerRecordVideo = [NSTimer timerWithTimeInterval:kTimeExactRun
                                                 target:self
                                               selector:@selector(updateProgressVideo)
                                               userInfo:nil
                                                repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:self.timerRecordVideo forMode:NSDefaultRunLoopMode];
    [self.timerRecordVideo fire];
}

//Delete record video
- (IBAction)cancelRecord:(id)sender {
    
    if ([_arrProgressViews count]) {
        
        ProgressEntity *entity = [_arrProgressViews objectAtIndex:([_arrProgressViews count] - 1)];
        
        if (entity.state == ProgressStateNone) {
            entity.state = ProgressStatePrepareForDelete;
            entity.progressView.backgroundColor = [UIColor redColor];
        }else {
            PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
            NSInteger lastDelete = [clips.arrRecordFiles count];
            if (lastDelete) {
                [clips.arrRecordFiles removeObjectAtIndex:(lastDelete - 1)];
                [_arrProgressViews removeObjectAtIndex:(lastDelete - 1)];
                [entity.progressView removeFromSuperview];
                CNP_NumberRecordVideo -=1;
                _isUsedFullTime = NO;
            }
        }
    }
}

//check the next script has clips for user. Return YES if has and otherwise.
-(BOOL)checkNextScriptHasTypeForUser {
    
    if (_curIndexIntoScript < [_arrScripts count]) {
        for (NSInteger idx = _curIndexIntoScript; idx < [_arrScripts count]; idx++) {
            PreviewClips *clips = [_arrScripts objectAtIndex:idx];
            if ([clips.type isEqualToString:kDictTypeUser]) {
                return YES;
                break;
            }
        }
    }
    return NO;
}


//Update progress view when recording
-(void)updateProgressVideo {
    NSInteger videoRecordIndex = CNP_NumberRecordVideo - 1;
    
    NSTimeInterval total = [self totalTimeIsUsed:videoRecordIndex];
    if(total > kTotalTimeRecord) {
        [self.timerRecordVideo invalidate];
        CNP_IsCurrentRecordVideo = NO;
        _ibNextButton.hidden = NO;
        _isUsedFullTime = YES;
        //stop record video
        [_camera stopRecording:^(LLSimpleCamera *camera, NSURL *outputFileUrl, NSError *error) {
            
        }];
        
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kRecordLessThan10") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        //Update time end record for Progress Entity
        NSDate *endTime = [NSDate date];
        ProgressEntity *entity = [_arrProgressViews objectAtIndex:videoRecordIndex];
        entity.end = endTime;
        entity.progressView.width = entity.progressView.width + CNP_MinLength;
        //Update end record time
        PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
        RecordEntity *recordEnity = [clips.arrRecordFiles objectAtIndex:videoRecordIndex];
        recordEnity.recordEnd = endTime;
        return;
    }
    
    if (videoRecordIndex < [_arrProgressViews count]) {
        //Get a progress segment to update UI when recording
        ProgressEntity *entity = [_arrProgressViews objectAtIndex:([_arrProgressViews count]-1)];
        entity.progressView.width = entity.progressView.width + CNP_MinLength;
    }else {
        //Create a new progress view
        UIView *view;
        if (videoRecordIndex <= [_arrProgressViews count] && videoRecordIndex != 0) {
            ProgressEntity *entity = [_arrProgressViews objectAtIndex:[_arrProgressViews count]-1];
            UIView *previousView = entity.progressView;
            CGFloat nextPos = CGRectGetWidth(previousView.bounds) + previousView.left + 0.5;
            view = [[UIView alloc] initWithFrame:CGRectMake(nextPos, 0, 0, _ibParentProgressView.height)];
        }else {
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, _ibParentProgressView.height)];
        }
        view.backgroundColor = [UIColor yellowColor];
        //init a new progress segment
        ProgressEntity *entity = [[ProgressEntity alloc] init];
        entity.progressView = view;
        entity.state = ProgressStateNone;
        
        PreviewClips *clips = [_arrScripts objectAtIndex:_curIndexIntoScript];
        RecordEntity *recordEnity = [clips.arrRecordFiles objectAtIndex:videoRecordIndex];
        entity.begin = recordEnity.recordBegin;
        
        //save progress segment to array
        [_arrProgressViews addObject:entity];
        //add progress segment to UI
        [_ibParentProgressView addSubview:entity.progressView];
        entity.progressView.width = entity.progressView.width + CNP_MinLength;
        
    }
    
}

//Get total time did use
-(NSTimeInterval)totalTimeIsUsed:(NSInteger)currentIndex {
    NSTimeInterval total = 0.0;
    //Calculate all time did use
    for (NSInteger idx = 0; idx < currentIndex + 1; idx++) {
        if (idx < [_arrProgressViews count]) {
            ProgressEntity *en =[_arrProgressViews objectAtIndex:idx];
            NSDate *begin = en.begin;
            NSDate *end = en.end;
            if (end == nil) {
                end = [NSDate date];
            }
            NSTimeInterval interval = [end timeIntervalSinceDate:begin];
            total += interval;
        }
    }
    return total;
}

-(void)closePreviewVideo {
    [_moviePlayerController stop];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
