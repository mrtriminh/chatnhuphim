//
//  CheckInternet.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/5/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckInternet : NSObject

+ (BOOL) isInternetConnectionAvailable;

@end

