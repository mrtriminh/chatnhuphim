//
//  CheckInternetManager.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/5/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;

@interface CheckInternetManager : NSObject

@property (strong, nonatomic) Reachability *reachability;

#pragma mark -
#pragma mark Shared Manager
+ (MTReachabilityManager *)sharedManager;

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end