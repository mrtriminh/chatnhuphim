//
//  ViewController.h
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/2/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController : UIViewController <UITextFieldDelegate, NSURLSessionDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource> {
@private
    NSMutableData *receivedURL;
    NSMutableData *receivedMovie;
    NSString *strData;
    NSString *prepareTxt;
    NSURL *saveURL;
    NSURL *movieURL;
    NSURL *assetURL;
    NSUInteger words;
    NSString *voice;
    NSString *linkNewTip;
    NSString *newTipDetect;
    NSArray *trend;
    NSArray *trendinput;
}

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property BOOL saved;
@property BOOL loaded;
@property (nonatomic, retain) NSString *voice;
//- (IBAction)play:(id)sender;

@end

