//
//  HotCell.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 8/11/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import "HotCell.h"

@implementation HotCell

@synthesize descriptionLabel = _descriptionLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 300, 30)];
        self.descriptionLabel.textColor = [UIColor blackColor];
        self.descriptionLabel.font = [UIFont fontWithName:@"Helvetica Neue Medium 24.0" size:12.0f];
        
        [self addSubview:self.descriptionLabel];
    }
    return self;
}

@end
