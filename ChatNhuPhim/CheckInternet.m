//
//  CheckInternet.m
//  ChatNhuPhim
//
//  Created by Tri Minh on 6/5/15.
//  Copyright (c) 2015 Tri Minh. All rights reserved.
//

#import "CheckInternet.h"
#import "Reachability.h"

@implementation CheckInternet

+ (BOOL) isInternetConnectionAvailable
{
    Reachability *internet = [Reachability reachabilityWithHostName: @"www.google.com"];
    NetworkStatus netStatus = [internet currentReachabilityStatus];
    bool netConnection = false;
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"Access Not Available");
            netConnection = false;
            break;
        }
        case ReachableViaWWAN:
        {
            netConnection = true;
            break;
        }
        case ReachableViaWiFi:
        {
            netConnection = true;
            break;
        }
    }
    return netConnection;
}

@end

