//
//  ErrorMessageConstants.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 11/2/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//
#import <Foundation/Foundation.h>
#ifndef ErrorMessageConstants_h
#define ErrorMessageConstants_h

static NSString *const kErrorMessageNotAccessPhoto = @"Cannot access your photo albumn. Please make changes in Setting.";
static NSString *const kErrorMessageNotUseCamera = @"Cannot use your camera to record";
static NSString *const kErrorNetWorkNotAvaiable = @"No Internet Connection";
#endif /* ErrorMessageConstants_h */
