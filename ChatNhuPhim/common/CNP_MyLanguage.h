//
//  CNP_MyLanguage.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/13/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CNP_MyLanguage : NSObject
+(instancetype)shared;

-(NSString*)localizedStringForKey:(NSString*)aKey value:(NSString*)aValue tableName:(NSString*)aTableName;

-(NSString*)localizedStringForKey:(NSString*)aKey value:(NSString*)aValue;

-(NSString*)localizedStringForKey:(NSString*)aKey;

// sets the language
- (void) setLanguage:(NSString*) language;

- (NSString *)language;
@end
