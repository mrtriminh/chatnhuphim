//
//  CNP_MyLanguage.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 1/13/16.
//  Copyright © 2016 Tri Minh. All rights reserved.
//

#import "CNP_MyLanguage.h"
#import "NSString+Utils.h"

@interface CNP_MyLanguage()
@property (nonatomic, strong) NSArray /* <NSBundle> */ *bundles;
@end

@implementation CNP_MyLanguage

+(instancetype)shared {
    static CNP_MyLanguage *obj = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        obj = [[CNP_MyLanguage alloc] initWithKlikSetup];
    });
    return obj;
}

-(id)initWithKlikSetup {
    self = [super init];
    if (self) {
        NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:@"CNPLanguage"];
        
        if ([NSString isEmptyString:language]) {
            [self resetDefaultLocalization];
        }else {
            [self setLanguage:language];
        }
    }
    return self;
}

-(void)setLanguage:(NSString*)aLanguage {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableArray *appleLangs = [NSMutableArray arrayWithArray:[userDefault objectForKey:@"AppleLanguages"]];
    
    //remove the aLanguage
    [appleLangs removeObject:aLanguage];
    [appleLangs insertObject:aLanguage atIndex:0];
    
    [userDefault setObject:appleLangs forKey:@"AppleLanguages"];
    [userDefault synchronize];
    
    [userDefault setObject:aLanguage forKey:@"CNPLanguage"];
    [userDefault synchronize];
    
    NSMutableArray *languages = [NSMutableArray array];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:aLanguage ofType:@"lproj"];
    
    if (![NSString isEmptyString:path]) {
        [languages addObject:[NSBundle bundleWithPath:path]];
    }
    
    if ([aLanguage rangeOfString:@"-"].location != NSNotFound) {//get the neutral cultrure bundle
        aLanguage = [[aLanguage componentsSeparatedByString:@"-"] firstObject];
        path = [[NSBundle mainBundle] pathForResource:aLanguage ofType:@"lproj"];
        
        if (![NSString isEmptyString:path]) {
            [languages addObject:[NSBundle bundleWithPath:path]];
        }
    }
    _bundles = languages;
}

- (NSString *)language {
    
    NSString *lang = [[NSUserDefaults standardUserDefaults]
                      objectForKey:@"CNPLanguage"];
    if( nil == lang){
        
        NSMutableArray *appleLangs = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]];
        
        lang = [appleLangs objectAtIndex:0];
    }
    
    return lang;
}


-(NSString*)localizedStringForKey:(NSString*)aKey value:(NSString*)aValue tableName:(NSString*)aTableName {
    NSString *result = nil;
    
    for (NSBundle *bundle in _bundles) {
        result = [bundle localizedStringForKey:aKey value:aValue table:aTableName];
        if (![result isEqualToString:aKey] && ![result isEqualToString:aValue]) {
            return result;
        }
    }
    return result;
    
}

-(NSString *)localizedStringForKey:(NSString *)aKey value:(NSString *)aValue {
    return [self localizedStringForKey:aKey value:aValue tableName:nil];
}

-(NSString *)localizedStringForKey:(NSString *)aKey {
    return [self localizedStringForKey:aKey value:@"" tableName:nil];
}


//reset to default localization
-(void)resetDefaultLocalization {
    _bundles = @[[NSBundle mainBundle]];
}

@end