//
//  APIManager.m
//  klik
//
//  Created by Minh Dat Giap on 9/24/15.
//  Copyright © 2015 Hoat Ha Van. All rights reserved.
//

#import "APIManager.h"
#import "AFNetworking.h"
#import "NSString+Utils.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "NSMutableDictionary+Clips_Utils.h"
#import "AppDelegate.h"
#import "PreviewClips.h"

@implementation APIManager

+(instancetype)shared{
    static APIManager *obj = nil;
    static dispatch_once_t tocken;
    dispatch_once(&tocken, ^{
        obj = [[APIManager alloc] init];
    });
    return obj;
}

-(void)checkNetWork:(NetworkResponse)completion {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {

        myLog(@"There IS NO internet connection");
        completion(NO, nil, myLanguage(@"kErrorNetWorkNotAvaiable"));
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:nil message:myLanguage(@"kErrorNetWorkNotAvaiable") delegate:nil cancelButtonTitle:kMessageAgreed otherButtonTitles:nil, nil] show];
        });
    } else {

        myLog(@"There IS internet connection");
        completion(YES, nil, myLanguage(@"kErrorNetWorkNotAvaiable"));
    }
}

-(void)methodGETdataWithParams:(id)param apiName:(NSString *)apiName completion:(NetworkResponse)completion {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __weak __typeof(UIView*)weakView = delegate.window.rootViewController.view;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:weakView animated:YES];
    hud.labelText = myLanguage(@"kApiLoadingMessage");
    hud.minShowTime =0.2;
    
    [self checkNetWork:^(BOOL success, id data, NSString *error) {
        __strong __typeof(weakView)strongView = weakView;
        if (success) {
            AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
            NSString *requestURL = [NSString stringWithFormat:@"%@%@", kServerAddress, apiName];
            NSLog(@"Request URL: %@%@", requestURL,param);
            [requestOperationManager GET:requestURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                completion(YES, responseObject, nil);
                 [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
                NSDictionary *dictError = [error userInfo];
                NSString *stringError = [dictError valueForKey:@"NSLocalizedDescription"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:nil message:stringError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                });
                completion(NO, nil, nil);
            }];
        }else {
             [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
            completion(NO, nil, myLanguage(@"kErrorNetWorkNotAvaiable"));
        }
    }];
}

#pragma mark - UIAlertDelegate


-(void)methodPOSTdataWithParams:(id)param apiName:(NSString *)apiName completion:(NetworkResponse)completion {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    __weak __typeof(UIView*)weakView = delegate.window.rootViewController.view;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:weakView animated:YES];
    hud.labelText = myLanguage(@"kApiLoadingMessage");
    hud.minShowTime =0.5;
    
    [self checkNetWork:^(BOOL success, id data, NSString *error) {
        __strong __typeof(weakView)strongView = weakView;
        if (success) {
            AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
            NSString *requestURL = [NSString stringWithFormat:@"%@%@", kServerAddress, apiName];
            myLog(@"Request URL: %@", requestURL);
            [requestOperationManager POST:requestURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                completion(YES, responseObject, nil);
                [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
                NSDictionary *dictError = [error userInfo];
                NSString *stringError = [dictError valueForKey:@"NSLocalizedDescription"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (stringError) {
                        [[[UIAlertView alloc] initWithTitle:nil message:stringError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    }
                });
                completion(NO, nil, nil);
            }];
            
        }else {
            [MBProgressHUD hideAllHUDsForView:strongView animated:YES];
            completion(NO, nil, myLanguage(@"kErrorNetWorkNotAvaiable"));
        }
    }];
}

-(void)downloadFileWithURL:(NSURL *)fileURL informationDownload:(id)informationDownload isFromClips:(BOOL)flag{
    if (fileURL == nil) {
        myLog(@"Cannot found a file url to download");
        return;
    }
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:fileURL];
    AFURLConnectionOperation *connectionOperation = [[AFHTTPRequestOperation alloc] initWithRequest:requestURL];
    
    NSArray *arrayPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  
    NSString *fileName = [[fileURL path] lastPathComponent];
    __block NSString *fileWriterPath = [[arrayPath firstObject] stringByAppendingPathComponent:fileName];
    
    myLog(@"Write file at path: %@", fileWriterPath);
    
    
    connectionOperation.outputStream = [NSOutputStream outputStreamToFileAtPath:fileWriterPath append:NO];
    
    if ([informationDownload isKindOfClass:[PreviewClips class]]) {
        if ([((PreviewClips*)informationDownload).type isEqualToString:kDictTypeSelect]) {
            ((PreviewClips*)informationDownload).localVideoURL = [NSURL fileURLWithPath:fileWriterPath];
        } else {
            ((PreviewClips*)informationDownload).localAudioURL = [NSURL fileURLWithPath:fileWriterPath];
        }
    }else {
        [informationDownload setURLPath:[NSURL fileURLWithPath:fileWriterPath]];
    }
    
    //download process
    [connectionOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        myLog(@"Recieved bytes: %lld/%lld", totalBytesRead, totalBytesExpectedToRead);
        [informationDownload setTotalBytesRead:totalBytesRead];
        [informationDownload setTotalBytesExpectedToRead:totalBytesExpectedToRead];
    }];
    
    //completeDownload
    [connectionOperation setCompletionBlock:^{
        myLog(@"Complete DownLoad file");
        
        [self addSkipBackupAttributeToItemAtPath:fileWriterPath];
        
        BOOL isPreviewClips = [informationDownload isKindOfClass:[PreviewClips class]]?YES:NO;
        
        if (isPreviewClips) {
            myLog(@"Com");
            ((PreviewClips*)informationDownload).downloadIsCompleted = YES;
        }else {
            [informationDownload setDownloadIsCompleted:YES];
        }
    
        [[NSNotificationCenter defaultCenter] postNotificationName:kPushDownloadCompleted object:informationDownload];
        if (flag) {//Download from clips
             [[NSNotificationCenter defaultCenter] postNotificationName:kPushShareClips object:informationDownload];
        }
        
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        delegate.homeController.makeController.ibPlayAll.enabled = YES;
       
    }];
    
    [connectionOperation start];
    
}

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString {
    
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        myLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
@end
