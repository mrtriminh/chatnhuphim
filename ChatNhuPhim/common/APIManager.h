//
//  APIManager.h
//  klik
//
//  Created by Minh Dat Giap on 9/24/15.
//  Copyright © 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface APIManager : NSObject<UIAlertViewDelegate>

typedef void(^NetworkResponse)(BOOL success, id data, NSString*error);

-(void)checkNetWork:(NetworkResponse)completion;

+(instancetype)shared;

-(void)methodGETdataWithParams:(id)param apiName:(NSString*)apiName completion:(NetworkResponse)completion;

-(void)methodPOSTdataWithParams:(id)param apiName:(NSString*)apiName completion:(NetworkResponse)completion;

-(void)downloadFileWithURL:(NSURL*)fileURL informationDownload:(id)informationDownload isFromClips:(BOOL)flag;

@end
