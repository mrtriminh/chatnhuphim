//
//  Constants.h
//  ChatNhuPhim
//
//  Created by Minh Đạt Giáp on 29/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//
#import <Foundation/Foundation.h>

#ifndef klik_Constants_h
#define klik_Constants_h

#define kArrayTitlesTabBar @[@"library", @"make", @"more"]
#define kFontTabBarItem [UIFont fontWithName:@"Helvetica-Medium" size:13.0];
#define kDEBUG 1

//NSLog
#if kDEBUG
# define myLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define myLog(...)
#endif

#define kArraySettings @[@"FACEBOOK LOGIN", @"ADD MORE CLIPS", @"CLEAR CACHE", @"TERMS & CONDITIONS", @"SAY HELLO/GÓP Ý", @"ABOUT"]
#define kArrayUpdateSettings @[@"FACEBOOK LOGIN", @"ADD MORE CLIPS", @"CLEAR CACHE", @"TERMS & CONDITIONS", @"SAY HELLO/GÓP Ý", @"ABOUT", @"NEW UPDATE!"]


#define kArrayTitleRandom  @[@"đang viết kịch bản", @"liên hệ đạo diễn", @"tuyển diễn viên quần chúng", @"âm thanh, ánh sáng, máy chạy!"]

//-Ads define
#define kAmountAds  2000
#define kBeginAdsAfterPosition 4
#define kRe_CreateAdsAfterSteps 4
//------Languages Macro
#define myLanguage(key) [[CNP_MyLanguage shared] localizedStringForKey:key]


//--------------ViewController name
//Clips View Controller
static NSString *const kClipsViewController = @"ClipsViewController";
//Make View Controller
static NSString *const kMakeViewController = @"MakeViewController";
//More View Controller
static NSString *const kMoreViewController = @"MoreViewController";
//Home View Controller
static NSString *const kHomeViewController = @"HomeViewController";
//VideoPlayViewController
static NSString *const kVideoPlayViewController = @"VideoPlayViewController";
//AudioViewController
static NSString *const kAudioViewController = @"AudioViewController";
//WebViewController
static NSString *const kWebViewController = @"WebViewController";
//SuggestionViewController
static NSString *const kSuggestionViewController = @"SuggestionViewController";
//Preview
static NSString *const kPreviewPlayViewController = @"PreviewPlayViewController";
//CategoriesViewController
static NSString *const kCategoriesViewController = @"CategoriesViewController";


//UIView Name
static NSString *const kQAViewTypeA = @"QAViewTypeA";

//UITableViewCell
static NSString *const kClipsHeaderTableViewCell = @"ClipsHeaderTableViewCell";
static NSString *const kClipsContentTableViewCell = @"ClipsContentTableViewCell";
static NSString *const kMakeClipsTableViewCell = @"MakeClipsTableViewCell";

//---------------Hard Code string---------------------------
static NSString *const kStringTerms = @"Điều khoản sử dụng";
static NSString *const kStringConditions = @"Chính sách bảo mật";
static NSString *const kChosenLang = @"chosenLanguage";

//--------------Server Info--------------
static NSString *const kServerAddress = @"http://chatnhuphim.com/api";
//------------API Name-------------------------------
static NSString *const kAPIRandom = @"/random";
static NSString *const kAPIAllCategory = @"/category/all";
static NSString *const kAPIExplore = @"/explore";
static NSString *const kAPICategory = @"/category";
static NSString *const kAPIAddFavorite = @"/favorite/add";
static NSString *const kAPIGetFavorite = @"/favorite/get";
static NSString *const kAPIDeleteFavorite = @"/favorite/delete";
static NSString *const kAPIExploreCategory = @"/explore?category";
static NSString *const kAPISearchKeyword = @"/search";
static NSString *const kAPIAllSequences = @"/slideshow/all";
static NSString *const kAPISlideshowDetail = @"/slideshow/detail";
//Message Name
static NSString *const kMessageCheckedTermsAndCondition = @"Vui lòng chọn đồng ý với các điều khoản.";
static NSString *const kMessageAgreed = @"OK";
//-------------Key Dictionary----------------
static NSString *const kDictUUID = @"uuid";
static NSString *const kDictUserGender = @"userGender";
static NSString *const kDictHeaderInfo = @"headerInfo";
static NSString *const kDictContentInfo = @"contentInfo";
static NSString *const kDictIsOpen = @"isOpen";
static NSString *const kDictHeaderHeight = @"headerHeight";
static NSString *const kDictContentHeight = @"contentHeight";
static NSString *const kDictData = @"data";
static NSString *const kDictDataNumber = @"dataNumber";
static NSString *const kDictFileName = @"fileName";
static NSString *const kDictThumbnailImage = @"thumbnailImage";
static NSString *const kDictURLPath = @"urlPath";
static NSString *const kDictVideoName = @"name";
static NSString *const kDictVideoURL = @"url";
static NSString *const kDictVideoLocalURL = @"localURL";
static NSString *const kDictThumbnailURL = @"thumbnail";
static NSString *const kDictName = @"name";
static NSString *const kDictIsLocal = @"clipsIsLocal";
static NSString *const kDictTotalBytesRead = @"totalBytesRead";
static NSString *const kDictTotalBytesExpectedToRead = @"totalBytesExpectedToRead";
static NSString *const kDictDownloadIsCompleted = @"downloadIsCompleted";
static NSString *const kDictCategory = @"category";
static NSString *const kDictKeyword = @"keyword";
static NSString *const kDictVoice = @"voice";
static NSString *const kDictSuggestion = @"suggestion";
static NSString *const kDictItemID = @"slideshowid";
static NSString *const kDictID = @"id";
static NSString *const kDictItems = @"items";
static NSString *const kDictFile = @"file";
static NSString *const kDictType = @"type";
static NSString *const kDictTypeUser = @"user";
static NSString *const kDictTypeSelect = @"select";
static NSString *const kDictMessage = @"message";
static NSString *const kDictAudioURL = @"audio";
static NSString *const kDictVolume = @"volume";
static NSString *const kDictFilter = @"filter";
//Push Notification Name------------------------------------
static NSString *const kPushStopPlayVideo = @"kPushStopPlayVideo";
static NSString *const kPushAddClipToMake = @"kPushAddClipToMake";
static NSString *const kPushScriptsClipToMake = @"kPushScriptsClipToMake";
static NSString *const kPushRecordFileToMake = @"kPushRecordFileToMake";
static NSString *const kPushDownloadCompleted = @"kDownloadCompleted";
static NSString *const kPushClosePreviewVideo = @"kPushClosePreviewVideo";
static NSString *const kPushCloseAudio = @"kPushCloseAudio";
static NSString *const kPushShareClips = @"kShareClips";
static NSString *const kPushChangeLanguage = @"kUpdateChangeLanguage";
//Color
static NSString *const kColorSettingsViewController = @"#1E1E1E";
static NSString *const kColorVangCam = @"#fec00f";
static NSString *const kColorXam = @"#282828";
static NSString *const kColorXamDam = @"#242424";
static NSString *const kColorDoSang = @"#fd2b2b";
static NSString *const kColorDoXam = @"#d94a39";
static NSString *const kColorXanhCyan = @"#1cbcb4";
static NSString *const kColorBorder = @"#a1a1a1";
static NSString *const kColorXanhChuDao = @"#58BEBD";
//ENUM
typedef NS_ENUM(NSInteger, GenderTypeIcon) {
    GenderTypeIconNone = 0,
    GenderTypeIconMale,
    GenderTypeIconFemale
};

typedef NS_ENUM(NSInteger, ProgressState) {
    ProgressStateNone = 0,
    ProgressStatePrepareForDelete
};
#endif
