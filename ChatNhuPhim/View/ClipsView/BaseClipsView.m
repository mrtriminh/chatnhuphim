//
//  BaseClipsView.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "BaseClipsView.h"
#import "NSMutableDictionary+Clips_Utils.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "MakeClipsBusinessManager.h"
#import "AppDelegate.h"
#import "ClipsBusinessManager.h"
#import "NSUserDefaults+Utils.h"
#import "FavoriteClipsView.h"
#import "FacebookManager.h"
#import "MBProgressHUD.h"
#import "NSString+Utils.h"

@implementation BaseClipsView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self setUpClipsViewInfo];
    [self setUpDataInfo:nil];
}

-(void)setUpClipsViewInfo{
    [_ibClipsTableView registerNib:[UINib nibWithNibName:kClipsHeaderTableViewCell bundle:nil] forCellReuseIdentifier:kClipsHeaderTableViewCell];
    [_ibClipsTableView registerNib:[UINib nibWithNibName:kClipsContentTableViewCell bundle:nil] forCellReuseIdentifier:kClipsContentTableViewCell];
    _ibClipsTableView.tableFooterView = [UIView new];
    _ibClipsTableView.separatorColor = [UIColor clearColor];
    _ibClipsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - UITableView's Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrDatas count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSMutableDictionary *clipsInfo = [_arrDatas objectAtIndex:indexPath.section];
    BOOL isOpen = [clipsInfo getIsOpen];
    if (isOpen) {
        if (indexPath.row == 0) {
            return [clipsInfo getHeaderHeight];
        }
        return [clipsInfo getContentHeight];
    }
    return [clipsInfo getHeaderHeight];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableDictionary *clipsInfo = [_arrDatas objectAtIndex:section];
    return [clipsInfo getIsOpen]?2:1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary *clipsInfo = [_arrDatas objectAtIndex:indexPath.section];
    UITableViewCell *cell;
    BOOL isOpen = [clipsInfo getIsOpen];
    if (isOpen && indexPath.row != 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:kClipsContentTableViewCell];
        [(ClipsContentTableViewCell*)cell setCellInfo:clipsInfo];
        [(ClipsContentTableViewCell*)cell setDelegate:self];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:kClipsHeaderTableViewCell];
        [(ClipsHeaderTableViewCell*)cell setCellInfo:clipsInfo];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(id)loadNibCell:(NSString*)identifier{
    return [[[NSBundle mainBundle] loadNibNamed:identifier owner:nil options:nil] firstObject];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row ==0) {//select is a header cell
        NSMutableDictionary *clipsInfo = [_arrDatas objectAtIndex:indexPath.section];
        for (NSMutableDictionary *dict in _arrDatas) {
            if (![dict isEqual:clipsInfo]) {
                if ([dict getIsOpen]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
                }
                [dict setIsOpen:NO];
            }
        }
        myLog(@"Open cell info:%@", clipsInfo);
        BOOL isOpen = [clipsInfo getIsOpen];
        [clipsInfo setIsOpen:!isOpen];
        [_ibClipsTableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!isOpen) {
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:1 inSection:indexPath.section];
                [_ibClipsTableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
            }
        });
    }
}

-(void)setUpDataInfo:(id)dataInfo{
    
    if (_arrDatas) {
        [_arrDatas removeAllObjects];
    }
    _arrDatas = [NSMutableArray arrayWithCapacity:[dataInfo count]];
    
    for (NSMutableDictionary *dict in dataInfo) {
        NSMutableDictionary *clipsInfo = [NSMutableDictionary clipsDictionaryWithHeaderInfo:dict contentInfo:nil isOpen:NO];
        [clipsInfo setHeaderHeight:@56.0];
        [clipsInfo setContentHeight:@180.0];
        [_arrDatas addObject:clipsInfo];
    }
    
    [_ibClipsTableView reloadData];
}

-(void)setUpDataInfo:(id)dataInfo autoPlayBackFirstVideo:(BOOL)flag {
    
    if (_arrDatas) {
        [_arrDatas removeAllObjects];
    }
    _arrDatas = [NSMutableArray arrayWithCapacity:[dataInfo count]];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    
    for (NSMutableDictionary *dict in dataInfo) {
        NSMutableDictionary *clipsInfo = [NSMutableDictionary clipsDictionaryWithHeaderInfo:dict contentInfo:nil isOpen:NO];
        [clipsInfo setHeaderHeight:@56.0];
        [clipsInfo setContentHeight:@180.0];
        [_arrDatas addObject:clipsInfo];
    }

    if (flag) {
        NSMutableDictionary *clipsInfo = [_arrDatas firstObject];
        for (NSMutableDictionary *dict in _arrDatas) {
            if (![dict isEqual:clipsInfo]) {
                [dict setIsOpen:NO];
            }
        }
        [clipsInfo setIsOpen:YES];
    }else {
        
        for (NSMutableDictionary *dict in _arrDatas) {
            [dict setIsOpen:NO];
        }
       
    }
    [_ibClipsTableView reloadData];
}

-(void)collapseCell {
    if ([_arrDatas count]) {
        for (NSMutableDictionary *dict in _arrDatas) {
            [dict setIsOpen:NO];
        }
        [_ibClipsTableView reloadData];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self endEditing:YES];
}

#pragma mark - ClipsContentVideo
-(void)shareClipsContentWithInfo:(id)info {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefault valueForKey:kDictUUID];
    
    if ([NSString isEmptyString:token]) {
        //Clear FB Cache when re-login
        
        
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
        NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"http://login.facebook.com"]];
        for (NSHTTPCookie* cookie in facebookCookies) {
            [cookies deleteCookie:cookie];
        }
        
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        __block UIView *rootView = delegate.window.rootViewController.view;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
        hud.labelText = myLanguage(@"kFBConnect");
        hud.minShowTime =0.5;
        
        [[FacebookManager sharedInstance] logInWithReadPermissions: @[@"email", @"public_profile"] viewController:nil completion:^(BOOL success, NSString *error) {
            
            [MBProgressHUD hideHUDForView:rootView animated:YES];
            
            if (success) {
                //Show popup
                [self showSharePopup:info];
                [self getFacebookProfileInfos];
            }else{
                if (![NSString isEmptyString:error]) {
                    [[[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:kMessageAgreed otherButtonTitles:nil, nil] show];
                }
            }
        }];
    }else {
        [self showSharePopup:info];
    }
}

-(void)showSharePopup:(id)info {
    _tempInfo = info;
    _customController = [[CustomActionSheetViewController alloc] init];
    [_customController viewWillAppear:YES];
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    [delegate.window.rootViewController.view addSubview:_customController.view];
    [delegate.window.rootViewController addChildViewController:_customController];
    [_customController didMoveToParentViewController:delegate.window.rootViewController];
    [_customController viewWillAppear:YES];
    _customController.delegate = self;
    
}

-(void)getFacebookProfileInfos {
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:nil];
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if(result)
        {
            if ([result objectForKey:@"gender"]) {
                [NSUserDefaults saveUserGender:[result objectForKey:@"gender"]];
                myLog(@"gender: %@",[result objectForKey:@"gender"]);
            }
        }
    }];
    
    [connection start];
}

#pragma mark - CustomActionSheetViewController
-(void)customerActionSheetSelectType:(CustomActionSheetType)sheetType {
    switch (sheetType) {
        case CustomActionSheetNone: {
            [_customController removeFromParentViewController];
            _customController = nil;
        }
            break;
        case CustomActionSheetFB: {
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            [MakeClipsBusinessManager sharetoFBWithURL:[_tempInfo getURLPath] viewController:delegate.window.rootViewController];
            
        }
            break;
        case CustomActionSheetSaveFile: {
            [MakeClipsBusinessManager saveClipsFileToPhotos:[_tempInfo getURLPath]];
        }
            break;
        case CustomActionSheetMessenger: {
            [MakeClipsBusinessManager shareToMessengerWithURL:[_tempInfo getURLPath]];
        }
            break;
        default:
            break;
    }
}

-(void)favoriteClipsContentWithInfo:(id)info{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefault valueForKey:kDictUUID];
    
    if ([NSString isEmptyString:token]) {
        //Clear FB Cache when re-login
        
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
        NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"http://login.facebook.com"]];
        for (NSHTTPCookie* cookie in facebookCookies) {
            [cookies deleteCookie:cookie];
        }
        
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        __block UIView *rootView = delegate.window.rootViewController.view;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:rootView animated:YES];
        hud.labelText = myLanguage(@"kFBLogin");
        hud.minShowTime =0.5;
        
        [[FacebookManager sharedInstance] logInWithReadPermissions: @[@"email", @"public_profile"] viewController:nil completion:^(BOOL success, NSString *error) {
            
            [MBProgressHUD hideHUDForView:rootView animated:YES];
            
            if (success) {
                [self getFacebookProfileInfos];
                //Show popup
                if ([self isKindOfClass:[FavoriteClipsView class]]) {
                    [ClipsBusinessManager deleteClipToFavoriteWithUUID:[NSUserDefaults getDeviceToken] urlVideo:info completion:^(BOOL success, id data, NSString *error) {
                        if (success) {
                            myLog("Un-Favorite success");
                            [self resetFavorite];
                        }else {
                            myLog("Un-Favorite un success");
                        }
                    }];
                }else {
                    [ClipsBusinessManager addClipToFavoriteWithUUID:[NSUserDefaults getDeviceToken] urlVideo:info completion:^(BOOL success, id data, NSString *error) {
                        if (success) {
                            myLog("Favorite success");
                        }else {
                            myLog("Favorite un success");
                        }
                    }];
                }

                
                
            }else{
                if (![NSString isEmptyString:error]) {
                    [[[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:kMessageAgreed otherButtonTitles:nil, nil] show];
                }
            }
        }];
    }else {
        if ([self isKindOfClass:[FavoriteClipsView class]]) {
            [ClipsBusinessManager deleteClipToFavoriteWithUUID:[NSUserDefaults getDeviceToken] urlVideo:info completion:^(BOOL success, id data, NSString *error) {
                if (success) {
                    myLog("Un-Favorite success");
                    [self resetFavorite];
                }else {
                    myLog("Un-Favorite un success");
                }
            }];
        }else {
            [ClipsBusinessManager addClipToFavoriteWithUUID:[NSUserDefaults getDeviceToken] urlVideo:info completion:^(BOOL success, id data, NSString *error) {
                if (success) {
                    myLog("Favorite success");
                }else {
                    myLog("Favorite un success");
                }
            }];
        }

    }
}


-(void)resetFavorite {
    [ClipsBusinessManager getFavoriteClipsWithUUID:[NSUserDefaults getDeviceToken] completion:^(BOOL success, id data, NSString *error) {
        if (success) {
            [self setUpDataInfo:data];
        }
    }];
}
@end
