//
//  ExploreClipsView.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "BaseClipsView.h"
#import <StartApp/StartApp.h>

@class ScriptEntity;
@protocol ExploreClipsViewDelegate <NSObject>
@required
-(void)selectedExploreClipsWithCategoryName:(NSString*)categoryName;
-(void)selectedExploreClipsWithScriptEntity:(ScriptEntity*)scriptEntity;
-(void)refreshNewData;
-(void)showAdsAt:(NSInteger)position;
@end

@interface ExploreClipsView : BaseClipsView <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSArray *arrCategories;
}
@property (strong, nonatomic) IBOutletCollection(UIButton) NSMutableArray *ibArrButtons;
@property (weak, nonatomic) IBOutlet UIScrollView *ibScrollViewTop;
@property (weak, nonatomic) id<ExploreClipsViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *ibRetryButton;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
- (IBAction)retry:(id)sender;
-(void)setUpTopLayout;
-(void)reloadCellData;
@property (weak, nonatomic) IBOutlet UICollectionView *ibSuggestionCollectionView;
@property (strong, nonatomic) NSMutableArray *arrSuggestion;
@property (strong, nonatomic) NSMutableArray *arrAds;
@property (strong, nonatomic) STAStartAppNativeAd *startAppNativeAd;
@property (weak, nonatomic) IBOutlet UILabel *ibMessageTitleLabel;
@end
