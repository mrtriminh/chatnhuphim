//
//  FavoriteClipsView.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "BaseClipsView.h"

@interface FavoriteClipsView : BaseClipsView
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@end
