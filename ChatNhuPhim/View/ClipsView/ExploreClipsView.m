//
//  ExploreClipsView.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "ExploreClipsView.h"
#import "ClipsBusinessManager.h"
#import "UIView+Utils.h"
#import "UIColor+Utils.h"
#import "SuggestionCollectionViewCell.h"
#import "ScriptEntity.h"
#import "AdsEntity.h"


#define kCOLLECTIONC_CELL_WIDTH 130.0
#define kCOLLECTIONC_CELL_TOP 0.0

static NSString *const kSuggestionCollectionViewCell = @"SuggestionCollectionViewCell";

@implementation ExploreClipsView
@synthesize startAppNativeAd;
-(void)awakeFromNib {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLanguage) name:kPushChangeLanguage object:nil];
    [_ibSuggestionCollectionView registerNib:[UINib nibWithNibName:kSuggestionCollectionViewCell bundle:nil] forCellWithReuseIdentifier:kSuggestionCollectionViewCell];
    if (!_refreshControl) {
        _refreshControl= [[UIRefreshControl alloc] init];
        _refreshControl.tintColor = [UIColor whiteColor];
        [_refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
        [_ibSuggestionCollectionView addSubview:_refreshControl];
        _ibSuggestionCollectionView.alwaysBounceVertical = YES;
    }
    _arrAds = [NSMutableArray array];
    
    [self updateLanguage];
}

-(void)updateLanguage {
    _ibMessageTitleLabel.text = myLanguage(@"kExploreTitleMessage");
}
-(void)setUpTopLayout {
    [ClipsBusinessManager getAllCategoryComplete:^(BOOL success, id data, NSString *error) {
        if (success) {
#pragma mark - [Not use current version 2.5]
//            _ibArrButtons = [NSMutableArray arrayWithCapacity:[data count]];
            arrCategories = [NSArray arrayWithArray:data];
#pragma mark - [Not use current version 2.5]
//            [self addCategoiesToView];
            
        }
    }];
}

-(void)addCategoiesToView {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat axisX = 0;
        NSInteger tag = 0;
        for (NSString *title in arrCategories) {
            NSString *titleButton = [NSString stringWithFormat:@" %@ /", title];
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(axisX, 0, 100, 30)];
            button.titleLabel.textAlignment = NSTextAlignmentLeft;
            button.titleLabel.font = kFontTabBarItem;
            [button setTitle:titleButton forState:UIControlStateNormal];
            [button setTitle:titleButton forState:UIControlStateSelected];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [button sizeToFit];
            [button addTarget:self action:@selector(changeCategory:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = tag++;
            [_ibScrollViewTop addSubview:button];
            [_ibArrButtons addObject:button];
            axisX += CGRectGetWidth(button.frame) + 10;
        }
        _ibScrollViewTop.contentSize = CGSizeMake(axisX, CGRectGetHeight(_ibScrollViewTop.frame));
        _ibScrollViewTop.backgroundColor = [UIColor colorWithHexString:@"#454545"];
        
        _ibScrollViewTop.showsHorizontalScrollIndicator = NO;
        //selected at first
        UIButton *firstButton = [_ibArrButtons firstObject];
        [self highlightedButton:firstButton];
        
        [self setNeedsDisplay];
        _ibRetryButton.hidden = YES;
    });
}

-(IBAction)retry:(id)sender {
#pragma mark - [Not use current version 2.5]
//    [self setUpTopLayout];
}

-(IBAction)changeCategory:(UIButton*)sender {
    myLog(@"Hello");
    [self highlightedButton:sender];
}

-(void)highlightedButton:(UIButton*)sender {
    for (UIButton *button in _ibArrButtons) {
        if ([button isEqual:sender]) {
            if (button.selected== NO){
                button.selected = YES;
                
                //scroll to current item
                [_ibScrollViewTop scrollRectToVisible:button.frame animated:YES];
                
                if (_delegate && [_delegate respondsToSelector:@selector(selectedExploreClipsWithCategoryName:)]) {
                    [_delegate selectedExploreClipsWithCategoryName:[arrCategories objectAtIndex:button.tag]];
                }
            }
        }else{
            button.selected = NO;
        }
    }
}

#pragma mark - UICollectionView Data source and Delegate
-(void)reloadCellData {
    [_ibSuggestionCollectionView reloadData];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_arrSuggestion count];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SuggestionCollectionViewCell *cell = (SuggestionCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:kSuggestionCollectionViewCell forIndexPath:indexPath];
    
    id object = [_arrSuggestion objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[AdsEntity class]]) {
        NSLog(@"ads pos %ld",(long)indexPath.row);
        NSLog(@"ads loaded %lu",(unsigned long)[startAppNativeAd.adsDetails count]);
        int randomAds = arc4random_uniform([startAppNativeAd.adsDetails count]);
        STANativeAdDetails *adDetails = [startAppNativeAd.adsDetails objectAtIndex:randomAds];
        cell.ibTitleLabel.text=[[startAppNativeAd.adsDetails objectAtIndex:randomAds] title];
        cell.ibImageButton.image=[[startAppNativeAd.adsDetails objectAtIndex:randomAds] imageBitmap];
        [adDetails sendImpression];
        //adDetails = nil;
        ((AdsEntity *)object).positionAds =randomAds;
    }else {
        [cell setCellInfo:object];
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    id object = [_arrSuggestion objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[AdsEntity class]]) {
        if (_delegate && [_delegate respondsToSelector:@selector(showAdsAt:)]) {
            NSInteger pos = ((AdsEntity *)object).positionAds;
            [_delegate showAdsAt:pos];
        }
    }else {
        if (_delegate && [_delegate respondsToSelector:@selector(selectedExploreClipsWithScriptEntity:)]) {
            SuggestionCollectionViewCell *cell = (id)[_ibSuggestionCollectionView cellForItemAtIndexPath:indexPath];
            [cell setCellInfo:[_arrSuggestion objectAtIndex:indexPath.row]];
            
            ScriptEntity *script = [_arrSuggestion objectAtIndex:indexPath.row];
            [_delegate selectedExploreClipsWithScriptEntity:script];
        }
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(kCOLLECTIONC_CELL_TOP, 0, 0, 0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds)/2.0, CGRectGetHeight([UIScreen mainScreen].bounds)/5.0);
}

-(void)setArrSuggestion:(NSMutableArray *)arrSuggestion {
#pragma mark - not use
    if ([_arrSuggestion count]) {
        
        [_arrSuggestion removeAllObjects];
    }
    _arrSuggestion = [NSMutableArray array];
    for(NSInteger idx = 0; idx < [arrSuggestion count]; idx++) {
        id data = [arrSuggestion objectAtIndex:idx];
        
        if (idx != 0 && idx % 8 == 0) {
            //this is Ads
            AdsEntity *ads = [[AdsEntity alloc] init];
            [_arrSuggestion addObject:ads];
        }
        [_arrSuggestion addObject:data];
    }
    
    //random position Ads
    for(int idx = 0; idx < [_arrSuggestion count]; idx++) {
        if (idx % 8 == 0 && idx != 0) {
            int rand = arc4random_uniform(idx) + 7;
            if (rand < [_arrSuggestion count]) {
                [_arrSuggestion exchangeObjectAtIndex:idx withObjectAtIndex:rand];
            }
        }
    }
    
    [_ibSuggestionCollectionView reloadData];
}

#pragma mark - UIRefresh Control
-(void)refershControlAction {
    if (_delegate && [_delegate respondsToSelector:@selector(refreshNewData)] ) {
        [_delegate refreshNewData];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
