//
//  SearchClipsView.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "SearchClipsView.h"
#import "NSString+Utils.h"

@implementation SearchClipsView

-(void)awakeFromNib {
    [super awakeFromNib];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLanguage) name:kPushChangeLanguage object:nil];
    _ibSearchField.keyboardAppearance = UIKeyboardAppearanceLight;
    _ibSearchField.keyboardType = UIKeyboardTypeWebSearch;
    _ibSearchField.delegate = self;
    _ibSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:myLanguage(@"kSearchTextHolder") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(void)updateLanguage {
    [_ibCategoryName setTitle:myLanguage(@"kDefaultCategory") forState:UIControlStateNormal];
    [_ibCategoryName setTitle:myLanguage(@"kDefaultCategory") forState:UIControlStateSelected];
     _ibSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:myLanguage(@"kSearchTextHolder") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

- (IBAction)changeGenderVoice:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(willChangeGenderIcon)]) {
        [_delegate willChangeGenderIcon];
    }
}

- (IBAction)searchAndPlayVideo:(id)sender {
    if ([NSString isEmptyString:_ibSearchField.text]) {
        return;
    }
    //turn off keyboard
    [self endEditing:YES];
    [self hideSuggestButton];
    [self collapseCell];
    if (_delegate && [_delegate respondsToSelector:@selector(willSearchVideoWithName:)]) {
        [_delegate willSearchVideoWithName:_ibSearchField.text];
    }
}

// Suggest some search phrases to 1st time user.
- (IBAction)searchSuggest:(id)sender {
    NSString *suggestContent = [(UIButton *)sender currentTitle];
    //turn off keyboard
    [self endEditing:YES];
    [self hideSuggestButton];
    [self collapseCell];
    if (_delegate && [_delegate respondsToSelector:@selector(willSearchAndPlayVideoFirstWithName:)]) {
        [_delegate willSearchAndPlayVideoFirstWithName:suggestContent];
    }
    
}

-(void)hideSuggestButton {
    _searchEx1.hidden = YES;
    _searchEx2.hidden = YES;
    _searchEx3.hidden = YES;
    _searchEx4.hidden = YES;
    _searchEx5.hidden = YES;
}

#pragma mark - UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    myLog(@"Hello World");
    if ([NSString isEmptyString:textField.text]) {
        return NO;
    }else {
        //turn off keyboard
        [self hideSuggestButton];
        [self endEditing:YES];
        [self collapseCell];
        if (_delegate && [_delegate respondsToSelector:@selector(willSearchVideoWithName:)]) {
            [_delegate willSearchVideoWithName:textField.text];
        }
    }
    return YES;
}

-(void)changeVoiceIconWithGenderType:(GenderTypeIcon)type {
    switch (type) {
        case GenderTypeIconFemale: {
            [_ibGenderVoiceButton setImage:[UIImage imageNamed:@"voice-nu"] forState:UIControlStateNormal];
        }
            break;
        case GenderTypeIconMale: {
            [_ibGenderVoiceButton setImage:[UIImage imageNamed:@"voice-nam"] forState:UIControlStateNormal];
        }
            break;
        default: {
            [_ibGenderVoiceButton setImage:[UIImage imageNamed:@"voice-tuy"] forState:UIControlStateNormal];
        }
            break;
    }
}

- (IBAction)changeCategories:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(willShowCategoriesWithSender:)]) {
        [_delegate willShowCategoriesWithSender:sender];
    }
}


@end
