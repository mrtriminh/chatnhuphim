//
//  SearchClipsView.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "BaseClipsView.h"
#import "ClipsBusinessManager.h"

@protocol SearchClipsViewDelegate <NSObject>
@required
-(void)willChangeGenderIcon;
-(void)willSearchAndPlayVideoFirstWithName:(NSString*)aName;
-(void)willSearchVideoWithName:(NSString*)aName;
-(void)willShowCategoriesWithSender:(UIButton*)sender;
@end

@interface SearchClipsView : BaseClipsView <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *ibCategoryName;
@property (weak, nonatomic) IBOutlet UITextField *ibSearchField;
@property (weak, nonatomic) id<SearchClipsViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *ibGenderVoiceButton;
- (IBAction)changeGenderVoice:(id)sender;
- (IBAction)searchAndPlayVideo:(id)sender;
- (void)changeVoiceIconWithGenderType:(GenderTypeIcon)type;
@property (weak, nonatomic) IBOutlet UIButton *searchEx1;
@property (weak, nonatomic) IBOutlet UIButton *searchEx2;
@property (weak, nonatomic) IBOutlet UIButton *searchEx3;
@property (weak, nonatomic) IBOutlet UIButton *searchEx4;
@property (weak, nonatomic) IBOutlet UIButton *searchEx5;
@end
