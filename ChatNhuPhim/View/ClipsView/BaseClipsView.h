//
//  BaseClipsView.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/30/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClipsHeaderTableViewCell.h"
#import "ClipsContentTableViewCell.h"
#import "CustomActionSheetViewController.h"

@interface BaseClipsView : UIView<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ClipsContentDelegate, CustomActionSheetDelegate> {
    //id tempInfo;
}
@property (weak, nonatomic) IBOutlet UITableView *ibClipsTableView;
@property (strong, nonatomic) NSMutableArray *arrDatas;
@property (strong, nonatomic) CustomActionSheetViewController *customController;
@property (weak, nonatomic) id tempInfo;
@property (assign, nonatomic) NSInteger identifierNumber;
-(void)setUpClipsViewInfo;

-(void)setUpDataInfo:(id)dataInfo;

-(void)setUpDataInfo:(id)dataInfo autoPlayBackFirstVideo:(BOOL)flag;

-(void)collapseCell;
@end
