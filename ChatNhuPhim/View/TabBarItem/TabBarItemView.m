//
//  TabBarItemView.m
//  Dan
//
//  Created by Minh Dat Giap on 9/12/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "TabBarItemView.h"
#import "NSString+Utils.h"
#import "UIImage+Utils.h"
#import "Constants.h"
#import "UIColor+Utils.h"


@implementation TabBarItemView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        [self setBackgroundColor:[UIColor clearColor]];
         self.ibButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
        [self addSubview:self.ibButton];
        
    }
    return self;
}

-(void)initWithTitle:(NSString*)title{
    title = [title uppercaseString];
    [self.ibButton setTitle:title forState:UIControlStateNormal];
    [self.ibButton setTitle:title forState:UIControlStateSelected];
    [self.ibButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.ibButton setTitleColor:[UIColor colorWithHexString:@"#F4E60A"] forState:UIControlStateSelected];
    
//    self.ibButton.layer.borderWidth = 0.3;
//    self.ibButton.clipsToBounds = YES;
//    self.ibButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)setTitle:(NSString*)title {
    [self.ibButton setTitle:title forState:UIControlStateNormal];
    [self.ibButton setTitle:title forState:UIControlStateSelected];
}


@end
