//
//  TabBarItemView.h
//  Dan
//
//  Created by Minh Dat Giap on 9/12/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarItemView : UIView
@property (strong, nonatomic)  UIButton *ibButton;
@property (strong, nonatomic)  UILabel *ibLabel;
@property (assign, nonatomic) BOOL hasTitle;

-(void)initWithTitle:(NSString*)title;

-(void)setTitle:(NSString*)title;
@end
