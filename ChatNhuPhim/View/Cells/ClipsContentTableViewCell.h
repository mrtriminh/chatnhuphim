//
//  ClipsContentTableViewCell.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
@protocol ClipsContentDelegate <NSObject>
-(void)shareClipsContentWithInfo:(id)info;
-(void)favoriteClipsContentWithInfo:(id)info;
@end

@interface ClipsContentTableViewCell : UITableViewCell <UIAlertViewDelegate> {
    id CP_cellInfo;
}
-(void)setCellInfo:(id)cellInfo;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (weak, nonatomic) IBOutlet UIView *ibPlayerView;
@property (weak, nonatomic) IBOutlet UIView *ibOverlayView;
@property (weak, nonatomic) IBOutlet UIImageView *ibThumbnailClip;
@property (weak, nonatomic) id <ClipsContentDelegate> delegate;
@end
