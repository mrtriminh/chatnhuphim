//
//  MakeClipsTableViewCell.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>

@class MakeClipsTableViewCell;
@protocol MakeClipsTableViewCellDelegate <NSObject>
-(void)shareClipsContentWithInfo:(id)info;

@required
-(void)makeClipseTableViewCellAddMoreCell:(MakeClipsTableViewCell*)cell;
-(void)makeClipseTableViewCellAddClips:(MakeClipsTableViewCell*)cell;
-(void)makeClipseTableViewCellRecord:(MakeClipsTableViewCell*)cell;
-(void)makeClipseTableViewCellAlbum:(MakeClipsTableViewCell*)cell;
-(void)makeClipseTableViewCellSearch:(MakeClipsTableViewCell*)cell;
-(void)makeClipseTableViewCellEditClips:(MakeClipsTableViewCell*)cell passVideoURL:(NSString*)url;
-(void)makeClipseTableViewCellEditAudio:(MakeClipsTableViewCell*)cell passVideoURL:(NSString*)url;
-(void)makeClipseTableViewCellAddRandomClips:(MakeClipsTableViewCell*)cell;
-(void)deleteMakeClipsOfMakeClipsTableViewCell:(MakeClipsTableViewCell*)cell;

@end

@interface MakeClipsTableViewCell : UITableViewCell <UIGestureRecognizerDelegate>{
    id CP_cellInfo;
}
@property (weak, nonatomic) id <MakeClipsTableViewCellDelegate> delegate;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (weak, nonatomic) IBOutlet UIView *ibPlayerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageThumbnail;
@property (weak, nonatomic) IBOutlet UIButton *ibRecord;
@property (weak, nonatomic) IBOutlet UIButton *ibAlbum;
@property (weak, nonatomic) IBOutlet UIButton *ibSearch;
@property (weak, nonatomic) IBOutlet UIButton *ibRandom;
@property (weak, nonatomic) IBOutlet UIButton *ibSend;
@property (weak, nonatomic) IBOutlet UIButton *ibRemake;
@property (weak, nonatomic) IBOutlet UIButton *ibEdit;
@property (weak, nonatomic) IBOutlet UIButton *ibDelete;
@property (weak, nonatomic) IBOutlet UIButton *ibFilterOff;
@property (weak, nonatomic) IBOutlet UIButton *ibPlay;
@property (weak, nonatomic) IBOutlet UILabel *ibTempLabel;
@property (weak, nonatomic) IBOutlet UIView *ibImageTempView;
@property (weak, nonatomic) IBOutlet UIProgressView *ibDownloadProgress;
@property (weak, nonatomic) IBOutlet UILabel *ibNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *ibAddCellBtn;
@property (weak, nonatomic) IBOutlet UIButton *ibAudioBtn;
@property (weak, nonatomic) IBOutlet UILabel *labelTipToMoveCell;

@property (weak, nonatomic) NSString *editVideoPath;

-(void)setCellInfo:(id)cellInfo;
-(void)setCellInfo:(id)cellInfo indexPath:(NSIndexPath*)indexPath;

@end
