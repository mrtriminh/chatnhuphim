//
//  ClipsHeaderTableViewCell.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "ClipsHeaderTableViewCell.h"
#import "NSMutableDictionary+Clips_Utils.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+Utils.h"

@implementation ClipsHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellInfo:(id)cellInfo{
    BOOL isOpen = [cellInfo getIsOpen];
    _ibClipsTitleLabel.textColor = isOpen?[UIColor colorWithHexString:kColorVangCam]:[UIColor whiteColor];
    
    id headerInfo = [cellInfo getHeaderInfo];
    _ibClipsTitleLabel.text = [headerInfo getVideoName];
    NSString *thumbnailURL = [headerInfo getVideoThumbnailURL];
    thumbnailURL = [thumbnailURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [_ibThumbnailVideo setImageWithURL:[NSURL URLWithString:thumbnailURL] placeholderImage:nil];
}


-(void)dealloc{
    myLog(@"Clips Header Dealloc");
}
@end
