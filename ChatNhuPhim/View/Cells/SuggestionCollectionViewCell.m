//
//  SuggestionCollectionViewCell.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/27/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "SuggestionCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ScriptEntity.h"

@implementation SuggestionCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setCellInfo:(ScriptEntity*)info {
    if (info.thumbnailURL) {
        [_ibImageButton setImageWithURL:info.thumbnailURL placeholderImage:nil];
    }
    _ibTitleLabel.text = info.message;
    _ibTitleLabel.backgroundColor = [UIColor yellowColor];
    
}

-(UIColor*)colorWithRandom {
    NSInteger rand = (int)arc4random_uniform(3)+1;
    switch (rand) {
        case 0:
            return [UIColor brownColor];
            break;
        case 1:
            return [UIColor purpleColor];
            break;
        case 2:
            return [UIColor redColor];
            break;
        default:
            break;
    }
    return [UIColor redColor];
}
@end
