//
//  SuggestionCollectionViewCell.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 12/27/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScriptEntity;

@interface SuggestionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ibImageButton;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
-(void)setCellInfo:(ScriptEntity*)info;
@end
