//
//  ClipsContentTableViewCell.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "ClipsContentTableViewCell.h"
#import "NSMutableDictionary+Clips_Utils.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "UIView+Utils.h"
#import "UIImageView+AFNetworking.h"
#import "VideoBusinessManager.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "UIApplication+Utils.h"
#import "ClipsBusinessManager.h"
#import "NSUserDefaults+Utils.h"
#import "NSString+Utils.h"

@implementation ClipsContentTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlay) name:kPushStopPlayVideo object:nil];
     myLog(@"awake");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _ibOverlayView.hidden = NO;
     myLog(@"set select info");
    // Configure the view for the selected state
}

-(void)setCellInfo:(id)cellInfo{
    
    id headerInfo = [cellInfo getHeaderInfo];
    CP_cellInfo = headerInfo;
    
    NSString *videoAddress = [headerInfo getVideoURL];
    videoAddress = [videoAddress stringByAddingPercentEscapesUsingEncoding:
                    NSUTF8StringEncoding];
    
    if (_moviePlayerController == nil) {
        [self createMovieClipWithURL:[NSURL URLWithString:videoAddress]];
    }
    NSString *thumbnailURL = [headerInfo getVideoThumbnailURL];
    thumbnailURL = [thumbnailURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [_ibThumbnailClip setImageWithURL:[NSURL URLWithString:thumbnailURL] placeholderImage:[UIImage imageNamed:@"bg-logo"]];
    [self bringSubviewToFront:_ibOverlayView];//_ibOverlayView.hidden = NO;
    myLog(@"set cell info");
}

-(void)pauseVideo{
    
    [_moviePlayerController stop];//Hoat Ha
    _moviePlayerController.view.hidden = YES;
   [self setNeedsDisplay];
}

-(void)stopVideoPlay{
    if (_moviePlayerController) {
        [_moviePlayerController stop];
        _moviePlayerController = nil;
       myLog(@"stop video");
    }
    myLog(@"stop video");
}

- (IBAction)favoriteClip:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefault valueForKey:kDictUUID];
    
    if ([NSString isEmptyString:token]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:myLanguage(@"kFavFBLogin")
                                                        message:myLanguage(@"kFavFBLoginMessage")
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    
    } else if (_delegate && [_delegate respondsToSelector:@selector(favoriteClipsContentWithInfo:)]) {
        [_delegate favoriteClipsContentWithInfo:[CP_cellInfo getVideoURL]];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        if (_delegate && [_delegate respondsToSelector:@selector(favoriteClipsContentWithInfo:)]) {
            [_delegate favoriteClipsContentWithInfo:[CP_cellInfo getVideoURL]];
    }}
}

- (IBAction)playClip:(id)sender {
    
    if (_moviePlayerController == nil) {
        NSString *videoAddress = [CP_cellInfo getVideoURL];
        videoAddress = [videoAddress stringByAddingPercentEscapesUsingEncoding:
                        NSUTF8StringEncoding];
        
        [self createMovieClipWithURL:[NSURL URLWithString:videoAddress]];
    }
     _moviePlayerController.view.hidden = NO;
    [_moviePlayerController play];
    _ibOverlayView.hidden = YES;
}

-(void)createMovieClipWithURL:(NSURL*)urlVideo {
    
    //hide control button
    //_ibOverlayView.hidden = YES;
    
    if (_moviePlayerController) {
        _moviePlayerController.view.hidden = YES;
        [_moviePlayerController.view removeFromSuperview];
        _moviePlayerController = nil;
    }
    
    _moviePlayerController = [[MPMoviePlayerController alloc] init];
    //Add Tap gesture for Movie Player
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pauseVideo)];
    tap.delegate = self;
    tap.numberOfTapsRequired = 1;
    [_moviePlayerController.view addGestureRecognizer:tap];
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus:) name:MPMoviePlayerLoadStateDidChangeNotification object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseVideo) name:kPushStopPlayVideo object:nil];

    _moviePlayerController.view.frame = _ibPlayerView.frame;
    _moviePlayerController.movieSourceType = MPMovieSourceTypeStreaming;
    _moviePlayerController.repeatMode = MPMovieRepeatModeNone;
    _moviePlayerController.controlStyle = MPMovieControlStyleNone;
    _moviePlayerController.shouldAutoplay = NO;
    _moviePlayerController.contentURL = urlVideo;
    [self addSubviewWithFullSize:_moviePlayerController.view];
    //[_moviePlayerController prepareToPlay];
    [_moviePlayerController play];
    [self setNeedsDisplay];
}

- (IBAction)sendClip:(id)sender {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareClips:) name:kPushShareClips object:nil];
    
    BOOL isDownload = [CP_cellInfo getDownloadIsCompleted];
    if (isDownload == NO) {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:delegate.window.rootViewController.view animated:YES];
        hud.labelText = myLanguage(@"kMergeClip");
        hud.minShowTime =0.5;
        
        [CP_cellInfo setTotalBytesRead:0];
        [CP_cellInfo setTotalBytesExpectedToRead:0];
        NSString *stringURL = [CP_cellInfo getVideoURL];
        stringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[VideoBusinessManager manager] downloadVideoClips:[NSURL URLWithString:stringURL] informationDownload:CP_cellInfo isFromClips:YES];
        
        //observer when changed bytes are downloaded
        [CP_cellInfo addObserver:self forKeyPath:kDictTotalBytesRead options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    }else {
        if (_delegate && [_delegate respondsToSelector:@selector(shareClipsContentWithInfo:)]) {
            [_delegate shareClipsContentWithInfo:CP_cellInfo];
        }
    }
}
-(void)shareClips:(NSNotification*)aNotif {
    if ([[aNotif object] isEqual:CP_cellInfo]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushShareClips object:nil];
        
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [MBProgressHUD hideAllHUDsForView:delegate.window.rootViewController.view animated:YES];
        
        
        if (_delegate && [_delegate respondsToSelector:@selector(shareClipsContentWithInfo:)]) {
            [_delegate shareClipsContentWithInfo:CP_cellInfo];
        }
    }
}

- (IBAction)touchMoreClip:(id)sender {
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.homeController selectTabBarItem:0];//move to Clips tab
    [delegate.homeController.clipController selectTabSearchTabSearchWithKeyWord:[CP_cellInfo getVideoName]];
}

- (IBAction)gotoMakeClip:(UIButton*)sender {
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.homeController.makeController addClipToMakeWithData:CP_cellInfo];
    [delegate.homeController selectTabBarItem:1];
    delegate.homeController.makeController.ibPlayAll.enabled = NO;
}

-(void)moviePlayBackDidFinish:(NSNotification*)aNotify{
    
    myLog(@"Finish Playback");
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] removeObserver:kPushStopPlayVideo];
    
    _moviePlayerController.view.hidden = YES;
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
    
    _ibOverlayView.hidden = NO;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:kDictTotalBytesRead]) {
        myLog(@"");
        long long totalBytesRead = [CP_cellInfo getTotalBytesRead];
        long long totalBytesExpectedToRead = [CP_cellInfo getTotalBytesExpectedToRead];
        
        if (totalBytesRead == totalBytesExpectedToRead && totalBytesRead != 0) {
            myLog(@"Remove observer");
            
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            
            [MBProgressHUD hideAllHUDsForView:delegate.window.rootViewController.view animated:YES];
            
//            if (_delegate && [_delegate respondsToSelector:@selector(shareClipsContentWithInfo:)]) {
//                [_delegate shareClipsContentWithInfo:CP_cellInfo];
//            }
            [CP_cellInfo removeObserver:self forKeyPath:kDictTotalBytesRead];
        }
    }
}

#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)changeStatus:(NSNotification*)aNotif {
    myLog(@"%@", [aNotif object]);
}

-(void)dealloc{
    myLog(@"Clips Content Dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
