//
//  MakeClipsTableViewCell.m
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import "MakeClipsTableViewCell.h"
#import "NSMutableDictionary+Make_Utils.h"
#import "NSMutableDictionary+VideoClips_Utils.h"
#import "UIImageView+AFNetworking.h"
#import "VideoBusinessManager.h"
#import "UIView+Utils.h"
#import "AppDelegate.h"

@implementation MakeClipsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _ibRecord.layer.borderWidth = 0.3;
    _ibRecord.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibAlbum.layer.borderWidth = 0.3;
    _ibAlbum.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibSearch.layer.borderWidth = 0.3;
    _ibSearch.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibRandom.layer.borderWidth = 0.3;
    _ibRandom.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibRemake.layer.borderWidth = 0.3;
    _ibRemake.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibEdit.layer.borderWidth = 0.3;
    _ibEdit.layer.borderColor = [UIColor whiteColor].CGColor;
    _ibAudioBtn.layer.borderWidth = 0.3;
    _ibAudioBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLanguageMakeCell) name:kPushChangeLanguage object:nil];
    [self updateLanguageMakeCell];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateLanguageMakeCell{
    _labelTipToMoveCell.text = myLanguage(@"kMakeTipMoveCel");
    [_ibRecord setTitle:myLanguage(@"kMakeRecord") forState:UIControlStateNormal];
    [_ibRecord setTitle:myLanguage(@"kMakeRecord") forState:UIControlStateSelected];
    [_ibAlbum setTitle:myLanguage(@"kMakeAlbum") forState:UIControlStateNormal];
    [_ibSearch setTitle:myLanguage(@"kMakeSearch") forState:UIControlStateNormal];
    [_ibRandom setTitle:myLanguage(@"kMakeRandom") forState:UIControlStateNormal];
    [_ibEdit setTitle:myLanguage(@"kMakeEdit") forState:UIControlStateNormal];
    [_ibAudioBtn setTitle:myLanguage(@"kMakeAudio") forState:UIControlStateNormal];
    [_ibRemake setTitle:myLanguage(@"kMakeRemake") forState:UIControlStateNormal];
//    [_ibDuplicate setTitle:myLanguage(@"kMake") forState:UIControlStateNormal];
}

-(void)resetCell {
    _imageThumbnail.image = nil;
    _ibDownloadProgress.hidden = YES;
    [self showHideBasicControlToMakeClips:YES];
    [_ibPlayerView setHidden:YES];
}

-(void)setCellInfo:(id)cellInfo indexPath:(NSIndexPath*)indexPath {
    [self setCellInfo:cellInfo];
    //number of cell
    _ibNumberLabel.text = [NSString stringWithFormat:@"%d", indexPath.row +1];

}

-(void)setCellInfo:(id)cellInfo {
    
    [self resetCell];
    
    CP_cellInfo = cellInfo;
    
    BOOL isLocal = [cellInfo getMakeClipsIsLocal];
    if (isLocal) {
        if ([cellInfo getThumbnailImage]) {
            _imageThumbnail.image = [cellInfo getThumbnailImage];
            [self showHideBasicControlToMakeClips:NO];
            [_ibPlayerView setHidden:NO];
        }
    }else {
        myLog(@"URL: %@", [cellInfo getMakeClipsThumbnailURL]);
        NSString *stringURL = [cellInfo getMakeClipsThumbnailURL];
        stringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [_imageThumbnail setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"bg-logo"]];
        
        [self showHideBasicControlToMakeClips:NO];
        
        _ibDownloadProgress.hidden = NO;
        
        if ([CP_cellInfo getDownloadIsCompleted] == NO) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadCompleted:) name:kPushDownloadCompleted object:nil];
            //downloadVideo
            [CP_cellInfo setTotalBytesRead:0];
            [CP_cellInfo setTotalBytesExpectedToRead:0];
             NSURL *videoURL = [NSURL URLWithString:[cellInfo getMakeClipVideoURL]];
            [[VideoBusinessManager manager] downloadVideoClips:videoURL informationDownload:CP_cellInfo isFromClips:NO];
            [CP_cellInfo addObserver:self forKeyPath:kDictTotalBytesRead options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionInitial context:nil];
            _ibDownloadProgress.progress = 0.0;
            //_ibAddCellBtn.enabled = NO;
        }else{
            [_ibPlayerView setHidden:NO];
            _ibDownloadProgress.progress = 1.0;
            _ibAddCellBtn.enabled = YES;
        }
        
    }

    if (_moviePlayerController && _moviePlayerController.playbackState == MPMoviePlaybackStatePlaying) {
        [self showHiddenMoviewPlayerController:NO];
        [_moviePlayerController.view setNeedsDisplay];
    }else {
        [self showHiddenMoviewPlayerController:YES];
    }
    myLog(@"url %@",[CP_cellInfo getVideoURL]);
}

-(void)showHideBasicControlToMakeClips:(BOOL)flag {
    _ibRecord.hidden = !flag;
    _ibAlbum.hidden = !flag;
    _ibSearch.hidden = !flag;
    _ibRandom.hidden = !flag;
}

- (IBAction)addMoreCell:(id)sender {
    [self addCell];
}

-(void)addCell{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    if (delegate.homeController.makeController.ibPlayAll.enabled == YES) {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddMoreCell:)]) {
        [_delegate makeClipseTableViewCellAddMoreCell:self];
    }
    }
}

- (IBAction)addClips:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddClips:)]) {
        [_delegate makeClipseTableViewCellAddClips:self];
    }
}

- (IBAction)record:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddClips:)]) {
        [_delegate makeClipseTableViewCellRecord:self];
    }
}

- (IBAction)album:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddClips:)]) {
        [_delegate makeClipseTableViewCellAlbum:self];
    }
}

- (IBAction)search:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddClips:)]) {
        [_delegate makeClipseTableViewCellSearch:self];
    }
}

- (IBAction)addRandomClips:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddRandomClips:)]) {
        [_delegate makeClipseTableViewCellAddRandomClips:self];
         _ibAddCellBtn.enabled = NO;
    }
}

- (IBAction)sendVideo:(id)sender {
    
}

-(void)shareClips:(NSNotification*)aNotif {
    if ([[aNotif object] isEqual:CP_cellInfo]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushShareClips object:nil];
        
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [MBProgressHUD hideAllHUDsForView:delegate.window.rootViewController.view animated:YES];
        
        
        if (_delegate && [_delegate respondsToSelector:@selector(shareClipsContentWithInfo:)]) {
            [_delegate shareClipsContentWithInfo:CP_cellInfo];
        }
    }
}

- (IBAction)remakeVideo:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellAddClips:)]) {
        [_delegate makeClipseTableViewCellAddClips:self];
    }
}

- (IBAction)deleteVideo:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(deleteMakeClipsOfMakeClipsTableViewCell:)]) {
        [_delegate deleteMakeClipsOfMakeClipsTableViewCell:self];
    }
}

- (IBAction)editVideo:(id)sender {
    _editVideoPath = [CP_cellInfo getURLPath];
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellEditClips:passVideoURL:)]) {
        [_delegate makeClipseTableViewCellEditClips:self passVideoURL:_editVideoPath];
    }
}

- (IBAction)editAudio:(id)sender {
    PermissionBlock permissionBlock = ^(BOOL granted) {
        if (granted)
        {
            [self editAudio];
        }
        else
        {
            // Warn no access to microphone
        }
    };
    
    if([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)])
    {
        [[AVAudioSession sharedInstance] performSelector:@selector(requestRecordPermission:)
                                              withObject:permissionBlock];
    }
    else
    {
       [self editAudio];
    }
}

-(void)editAudio {
    _editVideoPath = [CP_cellInfo getURLPath];
    if (_delegate && [_delegate respondsToSelector:@selector(makeClipseTableViewCellEditAudio:passVideoURL:)]) {
        [_delegate makeClipseTableViewCellEditAudio:self passVideoURL:_editVideoPath];
    }
}

- (IBAction)filterOffVideo:(id)sender {
}

- (IBAction)playVideo:(id)sender {
    
    if (_moviePlayerController == nil) {
        
        _moviePlayerController = [[MPMoviePlayerController alloc] init];//WithContentURL:[CP_cellInfo getURLPath]];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayerController];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseVideo) name:kPushStopPlayVideo object:nil];
        _moviePlayerController.view.frame = _ibPlayerView.frame;
        _moviePlayerController.movieSourceType = MPMovieSourceTypeFile;
        _moviePlayerController.repeatMode = MPMovieRepeatModeNone;
        _moviePlayerController.controlStyle = MPMovieControlStyleNone;
        _moviePlayerController.view.backgroundColor = [UIColor lightGrayColor];
        [_ibPlayerView addSubviewWithFullSize:_moviePlayerController.view];
        _moviePlayerController.shouldAutoplay = NO;
        myLog("Path: %@", [CP_cellInfo getURLPath]);
        _moviePlayerController.contentURL = [CP_cellInfo getURLPath];
        
        //Add Tap gesture for Movie Player
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pauseVideo)];
        tap.delegate = self;
        tap.numberOfTapsRequired = 1;
        [_moviePlayerController.view addGestureRecognizer:tap];
    }
    [self showHiddenMoviewPlayerController:NO];
    [_moviePlayerController play];
}

-(void)moviePlayBackDidFinish:(NSNotification*)aNotify{
    
    myLog(@"Finish Playback");
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    [self showHiddenMoviewPlayerController:YES];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
    [self setNeedsDisplay];
}

-(void)pauseVideo{
    [_moviePlayerController pause];
    [self showHiddenMoviewPlayerController:YES];
    [self setNeedsDisplay];
}

#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

//reset view when layout changed
-(void)layoutMarginsDidChange{

    if (_moviePlayerController && _moviePlayerController.playbackState == MPMoviePlaybackStatePlaying) {
        [self showHiddenMoviewPlayerController:NO];
    }else {
        [self showHiddenMoviewPlayerController:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPushStopPlayVideo object:nil];
    }
    [self setNeedsDisplay];
}

-(void)showHiddenMoviewPlayerController:(BOOL)flag{
    [_moviePlayerController.view setHidden:flag];
    [_imageThumbnail setHidden:!flag];
    _ibImageTempView.hidden = flag;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:kDictTotalBytesRead]) {
        long long totalBytesRead = [CP_cellInfo getTotalBytesRead];
        long long totalBytesExpectedToRead = [CP_cellInfo getTotalBytesExpectedToRead];
        _ibDownloadProgress.progress = totalBytesRead/(float)totalBytesExpectedToRead;
        if (totalBytesRead == totalBytesExpectedToRead && totalBytesRead != 0) {
            myLog(@"Remove observer");
            [CP_cellInfo removeObserver:self forKeyPath:kDictTotalBytesRead];
            [_ibPlayerView setHidden:NO];
        }
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kPushStopPlayVideo
                                                  object:nil];
    [_moviePlayerController.view setHidden:YES];
    [_moviePlayerController.view removeFromSuperview];
    _moviePlayerController = nil;
}

-(void)downloadCompleted:(NSNotification*)aNotif {
    if ([[aNotif object] isEqual:CP_cellInfo]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kPushDownloadCompleted object:nil];
        [_ibPlayerView setHidden:NO];
    }
}
@end
