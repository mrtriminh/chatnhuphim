//
//  ClipsHeaderTableViewCell.h
//  ChatNhuPhim
//
//  Created by Minh Dat Giap on 10/31/15.
//  Copyright © 2015 Tri Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClipsHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ibClipsTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ibThumbnailVideo;
-(void)setCellInfo:(id)cellInfo;
@end
