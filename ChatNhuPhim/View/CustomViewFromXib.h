//
//  CustomViewFromXib.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewFromXib : UIView
@property (nonatomic, strong) CustomViewFromXib *customView;
@end
